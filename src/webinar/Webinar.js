import React from 'react'
import ReactPixel from 'react-facebook-pixel'
import { Form } from './form/Form'

import './styles.scss'

import logoPNG from './media/logo.png'
import backgroundPNG from './media/background.png'


const queryString = require('query-string')


export class Webinar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showPopup: false
    }
    this.container = React.createRef()
  }

  componentDidMount() {
    const { from } = queryString.parse(window.location.search)
    if ( from ) {
      window.localStorage.setItem('from', from)
      window.location.replace('/webinar')
    } else {
      if ( window.localStorage.getItem('from') === 'blog1' ) {
        ReactPixel.init('741550959946347')
        ReactPixel.fbq('track', 'PageView')
      }
      document.getElementById('root').setAttribute('style', 'display: none')
    }
  }

  togglePopup() {
    this.setState({ showPopup: !this.state.showPopup })
  }

  render() {
    const {
      showPopup
    } = this.state
    return (
      <div className="wrapper-webinar">
        {showPopup && <Form pixelAction={() => {
          ReactPixel.fbq('track', 'Lead')
        }} toggle={this.togglePopup.bind(this)}/>}
        <div className="row-top">
          <div className="container">
            <div className="group">
              <div className="left">
                <a href="/" target="_blank" rel="noopener noreferrer">
                  <img src={logoPNG} alt="hello move"/>
                </a>
                <div className="title">
                  Вид на жительство в Евросоюзе без инвестиций!
                </div>
                <div className="title mobile">
                  Вид на жительство в Евросоюзе без инвестиций!
                </div>
                <div className="desc">
                  Мы расскажем вам об уникальной возможности получить ВНЖ по программе стартап-виза с подачей документов
                  онлайн
                </div>
                <div className="date">
                  22 августа 17:00 МСК
                </div>
                <div className="row-date">
                                    <span>
                                        22 августа<br/>17:00 МСК
                                    </span>
                </div>
                <button onClick={this.togglePopup.bind(this)} className="rect-btn">
                  Записаться на вебинар
                </button>
              </div>
              <div className="right">
                <img src={backgroundPNG} alt="cat"/>
              </div>
            </div>
          </div>
        </div>
        <div className="row-bottom">
          <div className="container">
            <div className="row-info">
              <div className="heading">
                На вебинаре<br/>вы узнаете
              </div>
              <div className="info-block">
                                <span className="number">
                                    01
                                </span>
                <span className="name">
                                    Как выбрать страну и программу для переезда
                                </span>
              </div>
              <div className="info-block">
                                <span className="number">
                                    02
                                </span>
                <span className="name">
                                    Какие документы нужны для подачи
                                </span>
              </div>
              <div className="info-block">
                                <span className="number">
                                    03
                                </span>
                <span className="name">
                                    Как все сделать своими руками
                                </span>
              </div>
              {/*<div className="info-block">*/}
              {/*    <span className="number">*/}
              {/*        04*/}
              {/*    </span>*/}
              {/*    <span className="name">*/}
              {/*        <b>БОНУС:</b> полезные шаблоны для самостоятельной подачи*/}
              {/*    </span>*/}
              {/*</div>*/}
            </div>
            <div className="row-bonus">
              {/*<div className="bonus">*/}
              {/*    <span>БОНУС:</span> полезные шаблоны для самостоятельной подачи*/}
              {/*</div>*/}
              <button onClick={this.togglePopup.bind(this)} className="rect-btn">
                Принять участие
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
