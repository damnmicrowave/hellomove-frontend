import React from 'react'
import { apiRequest } from '../../utils'
import './styles.scss'
import { ThankYou, Loading } from '../../includes'
import InputMask from 'react-input-mask'


export class Form extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      showThanksPopup: false,
      name: '',
      phone: '',
      email: ''
    }
    this.popup = React.createRef()
  }

  sendRequest() {
    const data = {
      ...this.state,
      page: window.location.pathname
    }
    this.setState({
      loading: true,
      name: '',
      email: '',
      phone: ''
    })
    apiRequest(() => {
      this.props.pixelAction()
      this.setState({
        loading: false,
        showThanksPopup: true
      })
      setTimeout(() => {
        this.setState({
          showThanksPopup: false
        })
        this.props.toggle()
      }, 5000)
    }, 'webinar_signup', { data: { ...data, from: window.localStorage.getItem('from') }, method: 'POST' })
  }

  validate() {
    const { name, email, phone } = this.state
    const emailRegex = /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/
    return name && emailRegex.test(email.trim()) && phone.length > 10
  }

  render() {
    const { toggle } = this.props
    const {
      name,
      email,
      phone,
      loading,
      showThanksPopup
    } = this.state
    return (
      showThanksPopup ?
        <ThankYou title="Заявка отправлена!" message="Информация по вебинару придёт вам на почту"/> :
        <div onClick={e => {
          if ( e.target === this.popup.current ) {
            toggle()
          }
        }} ref={this.popup} className="popup">
          <div className="form">
            <h3>Записаться на вебинар</h3>
            <span className="close-menu" onClick={toggle}>×</span>
            <div className="fields">
              <input type="text" onChange={e => {
                this.setState({ name: e.target.value })
              }} value={name} placeholder="Имя"/>
              <InputMask mask="+7 (999) 999-99-99" maskChar="" {...{
                value: phone,
                placeholder: 'Телефон',
                className: 'input',
                onChange: e => {
                  this.setState({ phone: e.target.value })
                }
              }}/>
              <input type="text" onChange={e => {
                this.setState({ email: e.target.value })
              }} value={email} placeholder="e-mail"/>
            </div>
            <div className="agreement" style={{ textAlign: 'left' }}>
              Нажимая на кнопку “Записаться”, Вы соглашаетесь с обработкой Ваших персональных данных
            </div>
            <button onClick={this.sendRequest.bind(this)}
                    className={`round-btn${!this.validate() ? ' disabled' : ''}`}>
              <span>{loading ? <Loading color="white"/> : 'Записаться'}</span>
            </button>
          </div>
        </div>
    )
  }
}
