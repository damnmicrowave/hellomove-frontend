import React from 'react'
import './styles.scss'

export class ThankYou extends React.Component {
  render() {
    const {
      title,
      message
    } = this.props
    return (
      <div className="popup-box">
        <div className="card">
          <h4>{title}</h4>
          <div className="secondary">
            {message}
          </div>
        </div>
      </div>
    )
  }
}