import React from 'react'
import { Link } from 'react-router-dom'
import { ContactForm, EmailSubscription } from '../../../'
import './styles.scss'
import visa from './media/visa.svg'
import mastercard from './media/mastercard.svg'

export class FooterBottom extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contactFormToggled: false
    }
  }

  toggleContactForm() {
    const { contactFormToggled } = this.state
    this.setState({
      contactFormToggled: !contactFormToggled
    })
  }

  render() {
    const {
      texts,
      selectLang
    } = this.props
    const {
      _contactUs,
      _termsOfService,
      _privacyPolicy,
      _cookiePolicy,
      _refundPolicy,
      _mainPage,
      _partners,
      _faq,
      _contacts,
      _number,
      _email,
      _requisites,
      _copyright,
      _siteMadeBy,
      _switchLang
    } = texts
    return (
      <>
        {this.state.contactFormToggled && <ContactForm texts={texts} toggle={this.toggleContactForm.bind(this)}/>}
        <div className="footer-bottom">
          <div className="upper-info">
            <div className="logo-block">
              <Link className="logo" to='/'/>
              <button className="circle-btn"
                      onClick={this.toggleContactForm.bind(this)}>
                {_contactUs}
              </button>
              <button className="switch-lang" onClick={selectLang}>{_switchLang}</button>
            </div>
            <div className="info-block">
              <div className="info-col">
                <Link to='/tos/'>{_termsOfService}</Link>
                <Link to='/privacy_policy/'>{_privacyPolicy}</Link>
                <Link to='/cookie_policy/'>{_cookiePolicy}</Link>
                <Link to='/refund_policy/'>{_refundPolicy}</Link>
              </div>
              <div className="info-col small">
                <Link to='/'>{_mainPage}</Link>
                <Link to='/partners/'>{_partners}</Link>
                {/*<Link to='/services/'>{services}</Link>*/}
                <Link to='/faq/'>{_faq}</Link>
                <Link to='/contacts/'>{_contacts}</Link>
                <div className="payment-options">
                  <img src={visa} alt="visa"/>
                  <img src={mastercard} alt="mastercard"/>
                </div>
              </div>
            </div>
            <div className="info-block">
              <div className="info-col">
                <span>Hellomove B.V.</span>
                <span>1788ER, Netherlands, Julianadorp, Middelzand 3445</span>
                <a href={`tel:${_number.replace(/[+ ]/g, '')}`}>{_number}</a>
                <a href={`mailto:${_email}`}>{_email}</a>
                <Link to='/requisites/'>{_requisites}</Link>
              </div>
              <div className="info-col">
                <EmailSubscription texts={texts}/>
              </div>
            </div>
          </div>
          <div className="divider"/>
          <div className="bottom-info">
            <span>{_copyright}</span>
            <span className="right">{_siteMadeBy}</span>
          </div>
        </div>
      </>
    )
  }
}