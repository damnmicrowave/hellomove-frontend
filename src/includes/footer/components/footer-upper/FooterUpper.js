import React from 'react'
import InputMask from 'react-input-mask'
import { Link } from 'react-router-dom'
import ym from 'react-yandex-metrika'
import { apiRequest } from '../../../../utils'
import { Loading, ThankYou } from '../../../'
import './styles.scss'

export class FooterUpper extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      showThanksPopup: false,
      name: '',
      phone: '',
      email: '',
      note: ''
    }
  }

  sendRequest() {
    const data = {
      ...this.state,
      page: window.location.pathname
    }
    this.setState({
      loading: true,
      name: '',
      phone: '',
      email: '',
      note: ''
    })
    apiRequest(() => {
      ym('reachGoal', 'contactUsFooter')
      this.setState({
        loading: false,
        showThanksPopup: true
      })
      setTimeout(() => {
        this.setState({
          showThanksPopup: false
        })
      }, 5000)
    }, 'contact_us', { data: data, method: 'POST' })
  }

  validate() {
    const { name, phone, email } = this.state
    const emailRegex = /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/
    return name &&
      phone.trim().length > 8 &&
      emailRegex.test(email.trim())
  }

  render() {
    const {
      name,
      email,
      phone,
      note,
      loading,
      showThanksPopup
    } = this.state
    const { texts } = this.props
    const {
      _footerContactUsTitle,
      _name,
      _phone,
      _note,
      _send,
      _agreement,
      _privacyPolicy,
      _thankYouTitle,
      _thankYouMessage
    } = texts
    return (
      <>
        {showThanksPopup && <ThankYou title={_thankYouTitle} message={_thankYouMessage}/>}
        <div className="upper-box">
          <div className="purple-color">
            <h4 dangerouslySetInnerHTML={{ __html: _footerContactUsTitle }}/>
            <div className="contact-us">
              <input value={name} className="active" placeholder={_name} type="text"
                     onChange={e => {
                       this.setState({ name: e.target.value })
                     }}/>
              <input value={email} placeholder="e-mail" type="text"
                     onFocus={e => {
                       e.target.removeAttribute('readonly')
                     }}
                     onChange={e => {
                       this.setState({ email: e.target.value })
                     }}/>
              <InputMask mask="+9999999999999" maskChar=" " {...{
                name: 'footer_phone',
                placeholder: _phone,
                value: phone,
                autoComplete: 'new-tel',
                onChange: e => {
                  this.setState({ phone: e.target.value })
                }
              }}/>
              <input value={note} placeholder={_note} type="text"
                     onChange={e => {
                       this.setState({ note: e.target.value })
                     }}/>
              <div className="agreement" style={{ textAlign: 'left' }}>
                {_agreement.replace('{{buttonText}}', _send)}
              </div>
              <button onClick={this.sendRequest.bind(this)}
                      className={this.validate.bind(this)() ? 'round-btn thin' : 'round-btn thin disabled'}>
                <span>{loading ? <Loading color="white"/> : _send}</span>
              </button>
              <Link to='/privacy_policy/'
                    style={{ verticalAlign: loading ? 'super' : 'middle' }}>{_privacyPolicy}</Link>
            </div>
          </div>
        </div>
      </>
    )
  }
}