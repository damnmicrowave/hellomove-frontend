import React from 'react'
import { NavLink } from 'react-router-dom'
import { ContactForm, Logo } from '..'
import { getParents } from '../../utils'
import './styles.scss'
import menuSVG from './media/menu.svg'
import timesSVG from './media/times.svg'


export class NavBar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      small: false,
      contactFormToggled: false,
      sliderToggled: false
    }
    this.slider = React.createRef()
    this.sliderToggler = React.createRef()
  }

  componentDidMount() {
    window.onscroll = () => {
      const small = window.scrollY > 20
      if ( this.state.small !== small ) {
        this.setState({ small: small })
      }
    }
    window.onclick = e => {
      if (
        !getParents(e.target).includes(this.slider.current) &&
        !getParents(e.target).includes(this.sliderToggler.current) &&
        this.state.sliderToggled
      ) {
        this.toggleSlider()
      }
    }
  }

  toggleContactForm() {
    const { contactFormToggled } = this.state
    this.setState({
      contactFormToggled: !contactFormToggled
    })
  }

  toggleSlider() {
    const { sliderToggled } = this.state
    this.setState({
      sliderToggled: !sliderToggled
    })
  }

  render() {
    const { texts } = this.props
    const { contactFormToggled, sliderToggled } = this.state
    const {
      _mainPage,
      _partners,
      _faq,
      _contacts,
      _contactUs
    } = texts
    const NavLinkMobile = props => (
      <NavLink onClick={this.toggleSlider.bind(this)} {...props}/>
    )
    return (
      <>
        {contactFormToggled && <ContactForm texts={texts} toggle={this.toggleContactForm.bind(this)}/>}
        <div className={`header${this.state.small ? ' small' : ''}`}>
          <NavLink className="logo" to="/">
            <Logo/>
          </NavLink>
          <div className="links">
            {/*<b><a className="circle-btn" href="http://newyear-hellomove.online/" target="_blank">{new_year}</a></b>*/}
            <NavLink exact to="/">{_mainPage}</NavLink>
            {/*<NavLink to="/all_programs/">{all_programs}</NavLink>*/}
            <NavLink to="/partners/">{_partners}</NavLink>
            {/*<NavLink to="/services/">{services}</NavLink>*/}
            <NavLink to="/faq/">{_faq}</NavLink>
            <NavLink to="/contacts/">{_contacts}</NavLink>
            {/*<a ref={this.langChange} onClick={() => {*/}
            {/*    onLangChange(this.langChange.current.innerHTML)*/}
            {/*}}>{lang === 'EN' ? 'RU' : 'EN'}</a>*/}
            <button id='contactUs' onClick={this.toggleContactForm.bind(this)} className="circle-btn">{_contactUs}</button>
          </div>
          <button ref={this.sliderToggler} onClick={this.toggleSlider.bind(this)} className="menu-mobile">
            <img src={menuSVG} alt="menu"/>
          </button>
        </div>
        <div ref={this.slider} className={`slider${sliderToggled ? ' active' : ''}`}>
          <div className={`top-box${this.state.small ? ' small' : ''}`}>
            <div className="links">
              <NavLinkMobile exact to="/">{_mainPage}</NavLinkMobile>
              {/*<b><a className="circle-btn" href="http://newyear-hellomove.online/" target="_blank">{new_year}</a></b>*/}
              {/*<NavLink to="/all_programs/">{all_programs}</NavLink>*/}
              <NavLinkMobile to="/partners/">{_partners}</NavLinkMobile>
              {/*<NavLink to="/services/">{services}</NavLink>*/}
              <NavLinkMobile to="/faq/">{_faq}</NavLinkMobile>
              <NavLinkMobile to="/contacts/">{_contacts}</NavLinkMobile>
              {/*<a ref={this.langChange} onClick={() => {*/}
              {/*    onLangChange(this.langChange.current.innerHTML)*/}
              {/*}}>{lang === 'EN' ? 'RU' : 'EN'}</a>*/}
              <button
                onClick={() => {
                this.toggleSlider.bind(this)()
                this.toggleContactForm.bind(this)()
              }} className="circle-btn">{_contactUs}</button>
            </div>
            <button onClick={this.toggleSlider.bind(this)} className="close-menu">
              <img src={timesSVG} alt="close"/>
            </button>
          </div>
        </div>
      </>
    )
  }
}