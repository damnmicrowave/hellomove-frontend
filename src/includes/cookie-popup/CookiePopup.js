import React from 'react'
import ym from 'react-yandex-metrika'
import './styles.scss'

export class CookiePopup extends React.Component {
  constructor(props) {
    super(props)

    this.banner = React.createRef()
  }

  removePopup() {
    this.banner.current.classList.add('disappear')
    setTimeout(() => {
      this.banner.current.remove()
    }, 2000)
    window.localStorage.cookie = true
  }

  render() {
    const {
      _cookiePopupMessage,
      _cookiePopupAccept,
      _cookiePopupDecline
    } = this.props.texts
    return (
      <div ref={this.banner} className="wrapper-cookie">
        <div className="cookie-popup">
                <span>
                    {_cookiePopupMessage}
                </span>
          <div className="buttons">
            <button onClick={() => {
              this.removePopup.bind(this)()
              ym('reachGoal', 'cookieAccept')
            }}>{_cookiePopupAccept}</button>
            <button onClick={() => {
              this.removePopup.bind(this)()
              ym('reachGoal', 'cookieDecline')
            }}>{_cookiePopupDecline}</button>
          </div>
        </div>
      </div>
    )
  }
}
