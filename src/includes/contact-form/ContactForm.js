import React from 'react'
import InputMask from 'react-input-mask'
import ym from 'react-yandex-metrika'
import { apiRequest } from '../../utils'
import { Link } from 'react-router-dom'
import { Loading, ThankYou } from '..'
import './styles.scss'


export class ContactForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      showThanksPopup: false,
      name: '',
      phone: '',
      email: ''
    }
    this.popup = React.createRef()
  }

  sendRequest() {
    const data = {
      ...this.state,
      page: window.location.pathname
    }
    this.setState({
      name: '',
      phone: '',
      email: '',
      loading: true
    })
    apiRequest(() => {
      ym('reachGoal', 'contactUsHeader')
      this.setState({
        loading: false,
        showThanksPopup: true
      })
      setTimeout(() => {
        this.setState({
          showThanksPopup: false
        })
        this.props.toggle()
      }, 5000)
    }, 'contact_us', { data: data, method: 'POST' })
  }

  validate() {
    const { name, phone, email } = this.state
    const emailRegex = /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/
    return name &&
      phone.trim().length > 8 &&
      emailRegex.test(email.trim())
  }

  render() {
    const { texts, toggle } = this.props
    const {
      name,
      phone,
      email,
      loading,
      showThanksPopup
    } = this.state
    const {
      _thankYouTitle,
      _thankYouMessage,
      _popupTitle,
      _name,
      _phone,
      _agreement,
      _popupButton,
      _privacyPolicy
    } = texts
    return (
      showThanksPopup ? <ThankYou title={_thankYouTitle} message={_thankYouMessage}/> :
        <div onClick={e => {
          if ( e.target === this.popup.current ) {
            toggle()
          }
        }} ref={this.popup} className="popup">
          <div className="form">
            <h3>{_popupTitle}</h3>
            <span className="close-menu" onClick={toggle}>×</span>
            <div className="fields">
              <input type="text" onChange={e => {
                this.setState({ name: e.target.value })
              }} value={name} placeholder={_name}/>
              <input type="text" onChange={e => {
                this.setState({ email: e.target.value })
              }} value={email} placeholder="e-mail"/>
              <InputMask mask="+9999999999999" maskChar=" " {...{
                value: phone,
                placeholder: _phone,
                onChange: e => {
                  this.setState({ phone: e.target.value })
                }
              }}/>
            </div>
            <div className="agreement" style={{ textAlign: 'left' }}>
              {_agreement.replace('{{buttonText}}', _popupButton)}
            </div>
            <button onClick={this.sendRequest.bind(this)}
                    className={`round-btn${!this.validate() ? ' disabled' : ''}`}>
              <span>{loading ? <Loading color="white"/> : _popupButton}</span>
            </button>
            <Link to='/privacy_policy/' onClick={toggle}
                  style={{ verticalAlign: loading ? 'super' : 'middle' }}>{_privacyPolicy}</Link>
          </div>
        </div>
    )
  }
}
