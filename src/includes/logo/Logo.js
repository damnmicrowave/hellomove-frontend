import React from 'react'
import './styles.scss'
import logoIcon from './media/logoH.png'
import logoText from './media/logo.png'

export const Logo = () => (
  <div className="logo-box">
    <div className="logo-icon">
      <img src={logoIcon} alt="H"/>
    </div>
    <img className="logo-text" src={logoText} alt="Hello Move"/>
  </div>
)