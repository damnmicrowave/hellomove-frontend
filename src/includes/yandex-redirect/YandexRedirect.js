import React from 'react'
import { Redirect } from 'react-router'


export const YandexRedirect = props => {
  localStorage.setItem('fromYandexAds', 'true')
  return (
    <Redirect to={props.location.search.split('r=')[1]}/>
  )
}