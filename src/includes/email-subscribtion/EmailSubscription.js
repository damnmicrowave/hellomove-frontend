import React from 'react'
import ym from 'react-yandex-metrika'
import { apiRequest } from '../../utils'
import { Loading } from '..'
import './styles.scss'

export class EmailSubscription extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      loading: false,
      subscribed: false
    }
  }

  validate() {
    const regex = /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/
    return regex.test(this.state.email)
  }

  subscribe() {
    const data = {
      email: this.state.email,
      page: window.location.pathname
    }
    this.setState({
      loading: true,
      email: ''
    })
    apiRequest(() => {
      ym('reachGoal', 'newsSubscribed')
      this.setState({
        loading: false,
        subscribed: true
      })
      setTimeout(() => {
        this.setState({
          subscribed: false
        })
      }, 5000)
    }, 'subscribe', { data: data, method: 'POST' })
  }

  render() {
    const { subscribed, loading, email } = this.state
    const {
      _newsTitle,
      _subscribeButton,
      _subscribeSuccessful
    } = this.props.texts
    return (
      <div className="subscribe">
        <h5>{_newsTitle}</h5>
        {subscribed ?
          <div className="subscribed">
            {_subscribeSuccessful}
          </div> :
          <>
            <input onChange={e => {
              this.setState({ email: e.target.value })
            }} autoComplete="off" value={email} placeholder="e-mail" type="text"/>
            <button onClick={this.subscribe.bind(this)}
                    className={this.validate.bind(this)() ? 'round-btn thin' : 'round-btn thin disabled'}>
              <span>{loading ? <Loading color="white"/> : _subscribeButton}</span>
            </button>
          </>
        }
      </div>
    )
  }
}