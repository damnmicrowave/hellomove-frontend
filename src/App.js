import React from 'react'
import { YMInitializer } from 'react-yandex-metrika'
import { createBrowserHistory } from 'history'
import { Redirect, Route as DefaultRoute, Switch } from 'react-router'
import { BrowserRouter as Router } from 'react-router-dom'
import { apiRequest } from './utils'
import ReactPixel from 'react-facebook-pixel'
import {
  CookiePopup,
  FooterBottom,
  Loading,
  NavBar,
  YandexRedirect
} from './includes'
import {
  HomePage,
  Partners,
  FAQ,
  Contacts,
  PrivacyPolicy,
  TermsOfService,
  CookiePolicy,
  RefundPolicy,
  Requisites,
  Programs,
  Program,
  Agency,
  AddAgency,
  ChangeTexts,
  Questionnaire,
  ErrorPage,
  CommercialProposal,
  ProgramNew,
  Payment,
  SignUp,
  Login,
  PersonalArea,
  ProgramStep, StepsEditor
} from './pages'
import './static/css/common.scss'


export default class App extends React.Component {
  constructor(props) {
    super(props)

    if ( window.location.search.includes('lang=en') ) window.localStorage.lang = 'EN'
    this.state = {
      loaded: false,
      lang: 'lang' in window.localStorage ? window.localStorage.lang : 'RU',
      texts: {}
    }
    this.history = createBrowserHistory()
  }

  componentDidMount() {
    ReactPixel.init('259964418474908')
    // noinspection JSUnresolvedFunction
    ReactPixel.fbq('track', 'PageView')
    document.getElementById('root').setAttribute('style', 'display: block')
    apiRequest(response => {
      this.setState({
        texts: response,
        loaded: true
      })
    }, 'get_all_texts')
  }

  render() {
    const { loaded, texts, lang } = this.state
    const { common } = loaded ? texts[lang] : {}
    if ( loaded ) {
      Object.keys(texts[lang]).forEach((item) => {
        if ( item !== 'common' ) texts[lang][item] = { ...texts[lang][item], ...common }
      })
    }
    const {
      homePage,
      programs,
      program,
      programNew,
      partners,
      commercialProposal,
      faq,
      contacts,
      agency,
      addAgency,
      policies,
      changeTexts,
      questionnaire,
      qBusiness,
      qInvestments,
      qJob,
      qEdu,
      qExchange,
      paymentPage,
      personalArea,
      stepPage,
      signUpPage,
      loginPage
    } = loaded ? texts[lang] : {}
    const Route = props => (
      <DefaultRoute {...props} component={routeProps => {
        window.scrollTo(0, 0)
        return props.render(routeProps)
      }}/>
    )
    return loaded ?
      <YMInitializer accounts={[ 56306485 ]}
                     options={{
                       clickmap: true,
                       trackLinks: true,
                       accurateTrackBounce: true,
                       webvisor: true
                     }} version="2">
        <Router>
          <NavBar texts={common} lang={lang}/>
          <Switch>
            <Route exact path="/" render={() => (<HomePage texts={homePage}/>)}/>
            <Route exact path="/sign_up/" render={() => <SignUp texts={signUpPage}/>}/>
            <Route exact path="/login/" render={() => <Login texts={loginPage}/>}/>
            <Route exact path="/personal_area/" render={() => <PersonalArea texts={personalArea} />}/>
            <Route exact path="/step/" render={props => <ProgramStep texts={stepPage} history={this.history} {...props}/>}/>
            <Route exact path="/steps_editor/" render={() => <StepsEditor/>}/>
            <Route exact path="/partners/" render={() => (<Partners texts={partners}/>)}/>
            <Route exact path="/faq/" render={() => (<FAQ texts={faq} lang={lang}/>)}/>
            <Route exact path="/contacts/" render={() => (<Contacts texts={contacts}/>)}/>
            <Route exact path="/privacy_policy/" render={() => (<PrivacyPolicy texts={policies}/>)}/>
            <Route exact path="/ya/" render={props => (<YandexRedirect {...props}/>)}/>
            <Route exact path="/tos/" render={() => (<TermsOfService texts={policies}/>)}/>
            <Route exact path="/cookie_policy/" render={() => (<CookiePolicy texts={policies}/>)}/>
            <Route exact path="/refund_policy/" render={() => (<RefundPolicy texts={policies}/>)}/>
            <Route exact path="/requisites/" render={() => (<Requisites texts={policies}/>)}/>
            <Route path="/payment/" render={props => (<Payment texts={paymentPage} lang={lang} {...props}/>)}/>
            <Route exact path="/programs/:programs"
                   render={props => (<Programs texts={programs} {...props} />)}/>
            <Route exact path="/program/:id/" render={props => (<Program texts={program} {...props}/>)}/>
            <Route exact path="/p/:programSlug/" render={props => (<ProgramNew texts={programNew} {...props}/>)}/>
            <Route exact path="/agency/:id/" render={props => (<Agency texts={agency} {...props}/>)}/>
            <Route exact path="/add_agency/" render={() => (<AddAgency texts={addAgency} lang={lang}/>)}/>
            <Route exact path="/commercial_proposal/:id/"
                   render={props => (<CommercialProposal texts={commercialProposal} {...props}/>)}/>
            <Route exact path="/change_texts/"
                   render={() => (<ChangeTexts texts={changeTexts} lang={lang}/>)}/>
            <Route path="/residence/" render={() => (<Redirect to="/questionnaire/business/"/>)}/>
            <Route exact path="/questionnaire/:questions/" render={props => (
              <Questionnaire texts={{
                ...questionnaire,
                qBusiness: qBusiness,
                qInvestments: qInvestments,
                qJob: qJob,
                qEdu: qEdu,
                qExchange: qExchange
              }} lang={lang} history={this.history} {...props}/>
            )}/>
            <DefaultRoute path="/" component={() => (<ErrorPage texts={changeTexts}/>)}/>
          </Switch>
          <FooterBottom selectLang={() => {
            const l = lang === 'RU' ? 'EN' : 'RU'
            window.localStorage.lang = l
            this.setState({ lang: l })
          }
          } texts={common}/>
          {!window.localStorage.cookie && <CookiePopup texts={common}/>}
        </Router>
      </YMInitializer> :
      <Loading global/>
  }
}
