import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { Webinar } from './webinar/Webinar'
import * as serviceWorker from './serviceWorker'

import './static/css/root.scss'

if ( window.location.pathname.includes('webinar') ) {
  ReactDOM.render(<Webinar/>, document.getElementById('webinar'))
} else {
  ReactDOM.render(<App/>, document.getElementById('root'))
}

serviceWorker.unregister()
