import React from 'react'
import { apiRequest } from '../../utils'
import { Link } from 'react-router-dom'
import { FooterUpper, Loading } from '../../includes'
import './styles.scss'
import emptyPNG from './media/empty.png'
import starSVG from './media/star.svg'
import starActiveSVG from './media/star-active.svg'


export class Programs extends React.Component {
  constructor(props) {
    super(props)
    const { state } = props.location
    this.state = {
      programsCount: 6,
      programs: state ? state.programs : []
    }
  }

  componentDidMount() {
    if ( !this.state.programs.length ) {
      apiRequest(({ programs }) => {
        this.setState({ programs: programs })
      }, `get_programs/${this.props.match.params.programs}`)
    }
  }

  render() {
    const {
      programsCount,
      programs
    } = this.state
    const { texts } = this.props
    const {
      title,
      terms,
      difficulty,
      morePrograms
    } = texts
    // noinspection JSUnresolvedVariable
    return (
      <>
        <div className="wrapper-programs">
          <div className="container">
            <h3>{title}</h3>
            {programs.length ? <>
              {programs.map(
                (item, index) => (
                  index + 1 <= programsCount && <Link to={{
                    pathname: `/program/${item.id}/`,
                    state: { program: item }
                  }} key={index} className="program">
                    <div className="program-image">
                      <img src={emptyPNG} alt="not loaded"/>
                      <img className="image"
                           src={`https://storage.googleapis.com/hm-files/${item.picture}`} alt=""/>
                    </div>
                    <div className="program-props">
                      <div className="terms">
                        {terms}: {item.terms}
                      </div>
                      <div>{difficulty}:</div>
                      <div className="simplicity">
                        {[ ...Array(5).keys() ].map((_, key) => (
                          <img key={key}
                               src={key > item.difficulty ? starSVG : starActiveSVG}
                               alt="star"/>
                        ))}
                      </div>
                    </div>
                    <h5>{item.name}</h5>
                    <div className="desc">
                      {item.short_description}
                    </div>
                  </Link>
                ))}
              {programs.length > programsCount &&
              <div className="show-more">
                <button onClick={() => {
                  this.setState({ programsCount: programsCount + 3 })
                }} className="round-btn">
                  <span>{morePrograms}</span>
                </button>
              </div>
              }
            </> : <Loading/>}
          </div>
        </div>
        <FooterUpper texts={texts}/>
      </>
    )
  }
}