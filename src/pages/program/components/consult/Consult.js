import React from 'react'
import './styles.scss'


export class Consult extends React.Component {
  render() {
    const { texts, toggle } = this.props
    const {
      getFreeConsultation,
      contactAgency
    } = texts
    return (
      <div className="consult">
        <div className="purpler"/>
        <div className="container">
          <h3>{getFreeConsultation}</h3>
          <button onClick={toggle} className="round-btn filled">
            <span>{contactAgency}</span>
          </button>
        </div>
      </div>
    )
  }
}