import React from 'react'
import './styles.scss'


export class Cashback extends React.Component {
  render() {
    const { texts, toggle } = this.props
    const {
      cashbackTitle,
      cashbackDescription,
      cashbackButton
    } = texts
    return (
      <div className="cashback">
        <div className="purpler"/>
        <div className="container">
          <h3 dangerouslySetInnerHTML={{ __html: cashbackTitle }}/>
          <div className="desc">{cashbackDescription}</div>
          <button onClick={toggle} className="round-btn filled">
            <span>{cashbackButton}</span>
          </button>
        </div>
      </div>
    )
  }
}