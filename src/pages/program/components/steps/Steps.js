import React from 'react'
import './styles.scss'


export class Steps extends React.Component {
  render() {
    const { stepsTitle } = this.props.texts
    let { steps } = this.props.program
    steps = steps.split('; ')
    const rowHeight = 100 / Math.ceil(steps.length / 3)
    return (
      <div className="program-steps">
        <div className="container">
          <h3>{stepsTitle}</h3>
          <div className="steps-list">
            {[ ...Array(Math.ceil(steps.length / 3)) ].map((_, index) => (
              <div key={index} className="row" style={{ height: `${rowHeight}%` }}>
                {steps.slice(index * 3, index * 3 + 3).map((item, key) => (
                  <div key={key} className="step">
                    <span><div>{index * 3 + key + 1}</div></span>
                    <div className="desc">{item}</div>
                  </div>
                ))}
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
