import React from 'react'
import './styles.scss'

export class BottomSection extends React.Component {
  render() {
    const { texts, toggle } = this.props
    const {
      bottomSectionTitle,
      bottomSectionDesc,
      bottomSectionButton
    } = texts
    return (
      <div className="bottom-section">
        <div className="container">
          <h3 dangerouslySetInnerHTML={{ __html: bottomSectionTitle }}/>
          <div className="desc">{bottomSectionDesc}</div>
          <button onClick={toggle} className="round-btn filled">
            <span>{bottomSectionButton}</span>
          </button>
        </div>
      </div>
    )
  }
}