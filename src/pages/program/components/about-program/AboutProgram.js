import React from 'react'
import './styles.scss'


export class AboutProgram extends React.Component {
  render() {
    const { aboutProgramTitle } = this.props.texts
    const {
      description,
      secondary_picture: picture
    } = this.props.program
    return (
      <div className="about-program">
        <div className="container">
          <div className="left"
               style={picture ? { backgroundImage: `url("https://storage.googleapis.com/hm-files/${picture}")` } : null}/>
          <div className="right">
            <div className="v-aligner">
              <h4>{aboutProgramTitle}</h4>
              <div className="divider"/>
              <div className="desc" dangerouslySetInnerHTML={{ __html: description }}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
