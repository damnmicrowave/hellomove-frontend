import React from 'react'
import { apiRequest } from '../../utils'
import { FooterUpper, Loading } from '../../includes'
import {
  StartScreen,
  AboutProgram,
  Consult,
  ProgramAdvantages,
  Cashback,
  Steps,
  BottomSection,
  ContactAgencies
} from './components'
import { Redirect } from 'react-router'

export class Program extends React.Component {
  constructor(props) {
    super(props)
    const { state } = props.location
    this.state = {
      showPopup: false,
      loaded: Boolean(state),
      program: state ? state.program : {}
    }
  }

  componentDidMount() {
    const { loaded } = this.state
    if ( !loaded ) {
      apiRequest(response => {
        this.setState({
          program: response.program,
          loaded: true
        })
      }, `get_program/${this.props.match.params.id}`)
    }
  }

  togglePopup() {
    const { showPopup } = this.state
    this.setState({ showPopup: !showPopup })
  }

  render() {
    const { texts } = this.props
    const {
      loaded,
      program
    } = this.state
    return (
      <>
        {loaded ? <div className="wrapper-program">
          {program.new_page && <Redirect to={`/p/${program.new_page}/`}/>}
          {this.state.showPopup &&
          <ContactAgencies toggle={this.togglePopup.bind(this)} program={program} texts={texts}/>
          }
          <StartScreen program={program} texts={texts}/>
          <AboutProgram program={program} texts={texts}/>
          <Consult toggle={this.togglePopup.bind(this)} texts={texts}/>
          <ProgramAdvantages program={program} texts={texts}/>
          <Cashback toggle={this.togglePopup.bind(this)} texts={texts}/>
          <Steps program={program} texts={texts}/>
          <BottomSection toggle={this.togglePopup.bind(this)} texts={texts}/>
        </div> : <Loading/>}
        <FooterUpper texts={texts}/>
      </>
    )
  }
}
