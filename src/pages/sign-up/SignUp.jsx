import React, { useEffect, useState } from 'react'
import InputMask from 'react-input-mask'
import { loadStripe } from '@stripe/stripe-js'
import { apiRequest } from '../../utils'
import { Loading } from '../../includes'
import { ProgramSelectionList } from './program-selection/ProgramSelectionList'

import styles from './SignUp.module.scss'

export const SignUp = ({ texts }) => {

  const [ name, setName ] = useState('')
  const [ citizenship, setCitizenship ] = useState('')
  const [ email, setEmail ] = useState('')
  const [ phone, setPhone ] = useState('')
  const [ password, setPassowrd ] = useState('')
  const [ selectedProgram, setProgram ] = useState('')
  const [ programsExpanded, setExpanded ] = useState(false)

  const [ programs, serPrograms ] = useState(false)
  useEffect(() => {
    apiRequest(({ programs }) => {
      serPrograms(programs)
    }, 'get_all_new_programs')
  }, [])

  const [ loading, setLoading ] = useState(false)
  const proceedToPayment = async () => {
    setLoading(true)
    const stripe = await loadStripe('pk_live_HIZ16LnT5fYmVsJcafgEewk6008RfwT4gq')
    apiRequest(async ({ sessionId }) => {
        stripe.redirectToCheckout({
          sessionId
        }).then(err => alert(err.message))
      }, 'member_signup',
      {
        method: 'POST',
        data: { name, phone, email, password, selectedProgram, citizenship }
      })
  }

  const validate = () => {
    const emailRegex = /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/
    return emailRegex.test(email.trim()) && phone.length > 8 && name && programs.includes(selectedProgram) && citizenship && password.length > 7
  }

  const { title, namePlaceholder, citizenshipPlaceholder, phonePlaceholder, passwordPlaceholder, signUpButton } = texts

  return (
    <div className={styles.wrapper}>
      {programs ?
        <div className={styles.card}>
          <h3>{title}</h3>
          <div className={styles.row}>
            <div className={styles.block}>
              <input
                value={name}
                onChange={e => setName(e.target.value)}
                type="text"
                placeholder={namePlaceholder}
              />
              <ProgramSelectionList
                expanded={programsExpanded}
                toggle={setExpanded}
                programs={programs}
                selected={selectedProgram}
                select={setProgram}
                texts={texts}
              />
              <input
                value={citizenship}
                onChange={e => setCitizenship(e.target.value)}
                type="text"
                placeholder={citizenshipPlaceholder}
              />
            </div>
            <div className={styles.block}>
              <InputMask mask="+99999999999999" maskChar=" " {...{
                name: 'signup_phone',
                placeholder: phonePlaceholder,
                value: phone,
                autoComplete: 'off',
                onChange: e => {
                  setPhone(e.target.value)
                }
              }}/>
              <input
                value={email}
                onChange={e => setEmail(e.target.value)}
                type="text"
                placeholder='E-mail'
              />
              <input
                value={password}
                onChange={e => setPassowrd(e.target.value)}
                type='password'
                placeholder={passwordPlaceholder}
              />
            </div>
          </div>
          <button
            onClick={proceedToPayment}
            className={`${styles.button} ${!validate() || loading ? 'disabled' : ''}`}
          >
            {loading ? <Loading/> : <span>{signUpButton}</span>}
          </button>
        </div> : <Loading/>}
    </div>
  )
}