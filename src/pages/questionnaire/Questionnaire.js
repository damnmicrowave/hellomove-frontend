import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import ym from 'react-yandex-metrika'
import ReactPixel from 'react-facebook-pixel'
import { apiRequest } from '../../utils'
import { FooterUpper, ThankYou } from '../../includes'
import {
  LeftSection,
  RightSection
} from './components'
import './styles.scss'


export class Questionnaire extends React.Component {
  constructor(props) {
    super(props)

    this.initialState = JSON.stringify({
      loading: false,
      showNoProgramsPopup: false,
      questionnaireData: {
        business: {
          selectedDirections: [],
          selectedCountries: []
        },
        investments: {
          selectedDirections: [],
          selectedCountries: []
        },
        job: {
          selectedDirections: [],
          selectedCountries: [],
          selectedCitizenship: []
        },
        edu: {
          selectedDirections: [],
          selectedCountries: [],
          selectedCitizenship: []
        },
        exchange: {
          selectedDirections: [],
          selectedCountries: []
        }
      },
      questionnaireAnswers: {},
      crmAnswers: {}
    })
    this.state = JSON.parse(this.initialState)
  }

  setQuestionnaireData(questionnaireType, question, questionTitle, answerData, answerText) {
    const {
      questionnaireData,
      questionnaireAnswers,
      crmAnswers
    } = this.state
    this.setState({
      questionnaireData: {
        ...questionnaireData,
        [questionnaireType]: {
          ...questionnaireData[questionnaireType],
          [question]: answerData
        }
      },
      questionnaireAnswers: {
        ...questionnaireAnswers,
        [questionnaireType]: {
          ...questionnaireAnswers[questionnaireType],
          [questionTitle]: answerText
        }
      },
      crmAnswers: {
        ...crmAnswers,
        [question]: answerText
      }
    })
  }

  sendAnswers() {
    const {
      questionnaireData,
      questionnaireAnswers,
      crmAnswers
    } = this.state
    const data = {
      type: window.location.pathname,
      data: questionnaireData,
      answers: questionnaireAnswers,
      crmAnswers: crmAnswers
    }
    this.setState({ loading: true })
    apiRequest(({ programs, ids }) => {
      ym('reachGoal', 'questionnaire')
      if ( ids.length ) {
        ReactPixel.init('741550959946347')
        ReactPixel.fbq('track', 'Contact')
        this.props.history.push({
          pathname: `/programs/${ids.join('_')}/`,
          state: { programs: programs }
        })
      } else {
        this.setState({ showNoProgramsPopup: true, loading: false })
        setTimeout(() => {
          this.setState({ showNoProgramsPopup: false })
          const ru = '68_20_80_34_76_12_72_65_15_88_51_18_90_100_23_28_29_4_25_14_35_102_36_81_27_8_84_37_61_40_69_3_56_104_41_53_77_98_33_92_43_57_59_63_96_94_42_85_73'
          const en = '17_67_79_26_32_71_60_30_75_74_19_78_11_82_70_24_64_38_93_49_39_22_44_89_101_47_54_21_31_97_52_16_58_62_50_45_13_95_46_48_86_55_66_83_91_99_103_105'
          this.props.history.push(`/programs/${this.props.lang === 'RU' ? ru : en}/`)
        }, 5000)
      }
    }, `pick_programs/${this.props.lang}`, { data: data, method: 'POST' })
  }

  validate(pathname) {
    const page = pathname.split('/')[2]
    const data = this.state.questionnaireData[page]
    const emailRegex = /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/
    let requiredQuestions, listsFilled
    switch ( page ) {
      case 'business':
        requiredQuestions = [
          'businessGoal',
          // 'projectIdea',
          'haveInvestments',
          'documentTerms',
          'spousePresence',
          // 'childrenPresence',
          // 'businessExperience',
          // 'companyFounders',
          // 'budgetDetails',
          'relocationTerms'
        ]
        if ( data['projectIdea'] === 'hasProjectIdea' ) requiredQuestions.push('projectInnovative')
        if ( data['childrenPresence'] === 'withChildren' ) requiredQuestions.push('childrenCount')
        listsFilled = data.selectedDirections.length
        break
      case 'investments':
        requiredQuestions = [
          'residenceGoal',
          'documentTerms',
          'maxInvestment',
          // 'whatInvestment',
          // 'spousePresence',
          // 'childrenPresence',
          'relocationTerms'
        ]
        if ( data['childrenPresence'] === 'withChildren' ) requiredQuestions.push('childrenCount')
        listsFilled = data.selectedDirections.length
        break
      case 'job':
        requiredQuestions = [
          // 'selectedCitizenship',
          // 'professtionDetails',
          'documentTerms',
          // 'budgetDetails',
          'lowQualificationJob',
          // 'salaryDetails',
          // 'spousePresence',
          // 'childrenPresence',
          // 'familyMembersWork',
          'relocationTerms'
          // 'needResidence'
        ]
        if ( data['childrenPresence'] === 'withChildren' ) requiredQuestions.push('childrenCount')
        listsFilled = data.selectedDirections.length // && data.selectedCitizenship.length;
        break
      case 'edu':
        requiredQuestions = [
          'selectedCitizenship',
          'ageInfo',
          'educationType',
          'studiyngNow',
          'academicVacation',
          'documentTerms',
          'budgetDetails',
          'selectedDirections'
        ]
        if ( data['studiyngNow'] === 'studiyngNow' ) requiredQuestions.push('educationSpeciality', 'graduationTime')
        listsFilled = data.selectedDirections.length && data.selectedCitizenship.length
        break
      case 'exchange':
        requiredQuestions = [
          'ageInfo',
          'sex',
          'studiyngNow',
          'selectedDirections',
          'sideJob',
          'budgetDetails'
        ]
        listsFilled = data.selectedDirections.length
        break
      default:
        break
    }
    const phone = data.phone ? data.phone : ''
    const email = data.email ? data.email : ''
    return requiredQuestions.every(q => q in data) &&
      listsFilled && data.name && phone.trim().length > 8 && emailRegex.test(email.trim())
  }

  render() {
    const {
      texts,
      match
    } = this.props
    const {
      loading,
      showNoProgramsPopup,
      questionnaireData
    } = this.state
    const {
      title,
      desc,
      noProgramsTitle,
      noProgramsMessage
    } = texts
    return (
      <>
        {showNoProgramsPopup && <ThankYou title={noProgramsTitle} message={noProgramsMessage}/>}
        <div className="wrapper-questionnaire">
          <div className="container">
            <h3>{title}</h3>
            <div className="desc">
              {desc}
            </div>
            <div className="group">
              <Router>
                <LeftSection onUpdate={() => (this.forceUpdate())} texts={texts}/>
                <RightSection
                  loading={loading}
                  validate={this.validate.bind(this)}
                  questions={match.params.questions}
                  send={this.sendAnswers.bind(this)}
                  questionnaireData={questionnaireData}
                  callback={this.setQuestionnaireData.bind(this)}
                  texts={texts}
                />
              </Router>
            </div>
          </div>
        </div>
        <FooterUpper texts={texts}/>
      </>
    )
  }
}
