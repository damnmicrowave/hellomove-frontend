import React from 'react'
import { Route } from 'react-router-dom'
import { Loading } from '../../../../includes'
import {
  Business,
  Investments,
  Job,
  Education,
  Exchange
} from './components'
import './styles.scss'


export class RightSection extends React.Component {
  render() {
    const {
      loading,
      texts,
      callback,
      questionnaireData,
      send,
      validate
    } = this.props
    const { sendButton } = texts
    return (
      <div className="right-section">
        <Route path="/questionnaire/business/" render={() => (
          <Business data={questionnaireData['business']} callback={callback} texts={texts}/>
        )}/>
        <Route path="/questionnaire/investments/" render={() => (
          <Investments data={questionnaireData['investments']} callback={callback} texts={texts}/>
        )}/>
        <Route path="/questionnaire/job/" render={() => (
          <Job data={questionnaireData['job']} callback={callback} texts={texts}/>
        )}/>
        <Route path="/questionnaire/edu/" render={() => (
          <Education data={questionnaireData['edu']} callback={callback} texts={texts}/>
        )}/>
        <Route path="/questionnaire/exchange/" render={() => (
          <Exchange data={questionnaireData['exchange']} callback={callback} texts={texts}/>
        )}/>
        <button onClick={send}
                className={`round-btn${!validate(window.location.pathname) || loading ? ' disabled' : ''}`}>
          <span>{loading ? <Loading color="white"/> : sendButton}</span>
        </button>
      </div>
    )
  }
}
