import React from 'react'
import {
  InputQuestion,
  TextareaQuestion,
  NumberInput,
  SingleAnswerQuestion,
  MultipleSelectionList,
  MultipleListSearch
} from '../questions'


export class Job extends React.Component {
  constructor(props) {
    super(props)

    const { qJob } = props.texts
    let qJobTexts = {}
    Object.keys(qJob).forEach(item => {
      qJobTexts[item] = qJob[item]
      if ( item.includes('Answers') || item.includes('List') ) {
        qJobTexts[item] = qJobTexts[item].split('; ')
      }
    })
    this.state = {
      directionsExpanded: false,
      countriesExpanded: false,
      citizenshipExpanded: false,
      qJob: qJobTexts
    }
  }

  toggleDirectionsList(expanded = undefined) {
    this.setState({
      directionsExpanded: expanded !== undefined ? expanded : !this.state.directionsExpanded
    })
  }

  toggleCountriesList(expanded = undefined) {
    this.setState({
      countriesExpanded: expanded !== undefined ? expanded : !this.state.countriesExpanded
    })
  }

  toggleCitizenshipList(expanded = undefined) {
    this.setState({
      citizenshipExpanded: expanded !== undefined ? expanded : !this.state.citizenshipExpanded
    })
  }

  render() {
    const {
      texts,
      data,
      callback: defaultCallback
    } = this.props
    const callback = (q, t, d, a) => {
      defaultCallback('job', q, t, d, a)
    }
    const {
      directionsExpanded,
      countriesExpanded,
      citizenshipExpanded,
      qJob
    } = this.state
    const {
      name,
      selectedDirections,
      selectedCountries,
      selectedCitizenship,
      professtionDetails,
      relocationDetails,
      childrenCount,
      aboutYourself,
      phone,
      email
    } = data
    const {
      qYourName,
      qYourPhone,
      qYourEmail,
      answerYes,
      answerNo,
      qDirections,
      qDirectionsList,
      qDirectionsChoose,
      qChooseCountries,
      qChooseCountriesPlaceholder
    } = texts
    const {
      programJob,
      qCitizenship,
      qCitizenshipAnswers,
      qProfession,
      qDocumentTerms,
      qDocumentTermsAnswers,
      qBudget,
      qBudgetAnswers,
      qLowQualificationJob,
      qRelocationDetails,
      qWhatSalary,
      qWhatSalaryAnswers,
      qSpouse,
      qChildren,
      qChildrenCount,
      qRelocationTerms,
      qRelocationTermsAnswers,
      qNeedResidence,
      qNeedResidenceAnswers,
      qAboutYourself,
      qFamilyMembersWork,
      qFamilyMembersWorkAnswers
    } = qJob
    const questions = [
      {
        type: 'inputQuestion',
        props: {
          title: qYourName,
          onInput: e => {
            callback('name', qYourName, e.target.value, e.target.value)
          },
          value: name
        }
      },
      // {
      //     type: "multipleListSearch",
      //     props: {
      //         title: qCitizenship,
      //         searchPlaceholder: qChooseCountriesPlaceholder,
      //         expanded: citizenshipExpanded,
      //         allItems: qCitizenshipAnswers
      //             .filter((keyword, index, keywords) => keywords.lastIndexOf(keyword) === index)
      //             .sort((a, b) => a < b ? -1 : 1),
      //         selectedItems: selectedCitizenship,
      //         toggleList: this.toggleCitizenshipList.bind(this),
      //         selectItem: c => {
      //             callback("selectedCitizenship", qCitizenship, c, c)
      //         },
      //     }
      // },
      // {
      //     type: "inputQuestion",
      //     props: {
      //         title: qProfession,
      //         onInput: e => {
      //             callback('professtionDetails', qProfession, e.target.value, e.target.value)
      //         },
      //         value: professtionDetails
      //     }
      // },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qDocumentTerms,
          answers: [
            { codeName: 'urgently', answer: qDocumentTermsAnswers[0] },
            { codeName: 'twoToFourMonths', answer: qDocumentTermsAnswers[1] },
            { codeName: 'fiveToSevenMonths', answer: qDocumentTermsAnswers[2] },
            { codeName: 'eightAndMore', answer: qDocumentTermsAnswers[3] }
          ],
          questionCode: 'documentTerms'
        }
      },
      {
        type: 'multipleSelectionList',
        props: {
          title: qDirections,
          directionsPlaceholder: qDirectionsChoose,
          expanded: directionsExpanded,
          allItems: [ 'north', 'britain', 'europe', 'asia', 'arab', 'australia', 'latin' ]
            .map(
              (item, index) => ({
                codeName: item,
                item: qDirectionsList.split('; ')[index]
              })
            ),
          selectedItems: selectedDirections,
          toggleList: this.toggleDirectionsList.bind(this),
          selectItem: (d, a) => {
            callback('selectedDirections', qDirections, d, a)
          }
        }
      },
      // ...selectedDirections.length ?
      //     [{
      //         type: "multipleListSearch",
      //         props: {
      //             title: qChooseCountries,
      //             searchPlaceholder: qChooseCountriesPlaceholder,
      //             expanded: countriesExpanded,
      //             allItems: selectedDirections.map(({codeName}) => (texts[`qCountriesList__${codeName}`].split('; '))).flat()
      //                 .filter((keyword, index, keywords) => keywords.lastIndexOf(keyword) === index)
      //                 .sort((a, b) => a < b ? -1 : 1),
      //             selectedItems: selectedCountries,
      //             toggleList: this.toggleCountriesList.bind(this),
      //             selectItem: c => {
      //                 callback("selectedCountries", qChooseCountries, c, c)
      //             },
      //         }
      //     }] : [],
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qBudget,
      //         answers: [
      //             {codeName: 'budgetLow', answer: qBudgetAnswers[0]},
      //             {codeName: 'budgetTwoToThree', answer: qBudgetAnswers[1]},
      //             {codeName: 'budgetFourToSeven', answer: qBudgetAnswers[2]},
      //             {codeName: 'budgetNoLimit', answer: qBudgetAnswers[3]},
      //         ],
      //         questionCode: "budgetDetails"
      //     }
      // },
      // {
      //     type: "textareaQuestion",
      //     props: {
      //         title: qRelocationDetails,
      //         onInput: e => {
      //             callback('relocationDetails', qRelocationDetails, e.target.value, e.target.value)
      //         },
      //         value: relocationDetails
      //     }
      // },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qLowQualificationJob,
          answers: [
            { codeName: 'lowQualification', answer: answerYes },
            { codeName: 'highQualification', answer: answerNo }
          ],
          questionCode: 'lowQualificationJob'
        }
      },
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qWhatSalary,
      //         answers: [
      //             {codeName: 'salaryLow', answer: qWhatSalaryAnswers[0]},
      //             {codeName: 'salaryTwoToFour', answer: qWhatSalaryAnswers[1]},
      //             {codeName: 'salaryFourAndMore', answer: qWhatSalaryAnswers[2]},
      //         ],
      //         questionCode: "salaryDetails"
      //     }
      // },
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qSpouse,
      //         answers: [
      //             {codeName: 'withSpouse', answer: answerYes},
      //             {codeName: 'noSpouse', answer: answerNo},
      //         ],
      //         questionCode: "spousePresence"
      //     }
      // },
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qChildren,
      //         answers: [
      //             {codeName: 'withChildren', answer: answerYes},
      //             {codeName: 'noChildren', answer: answerNo},
      //         ],
      //         questionCode: "childrenPresence"
      //     }
      // },
      ...data['childrenPresence'] === 'withChildren' ?
        [ {
          type: 'numberInput',
          props: {
            title: qChildrenCount,
            onInput: e => {
              callback('childrenCount', qChildrenCount, e.target.value, e.target.value)
            },
            value: childrenCount
          }
        } ] : [],
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qFamilyMembersWork,
      //         answers: [
      //             {codeName: 'familyWorkersZero', answer: qFamilyMembersWorkAnswers[0]},
      //             {codeName: 'familyWorkersOne', answer: qFamilyMembersWorkAnswers[1]},
      //             {codeName: 'familyWorkersTwo', answer: qFamilyMembersWorkAnswers[2]},
      //         ],
      //         questionCode: "familyMembersWork"
      //     }
      // },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qRelocationTerms,
          answers: [
            { codeName: 'asap', answer: qRelocationTermsAnswers[0] },
            { codeName: 'halfYear', answer: qRelocationTermsAnswers[1] },
            { codeName: 'planning', answer: qRelocationTermsAnswers[2] }
          ],
          questionCode: 'relocationTerms'
        }
      },
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qNeedResidence,
      //         answers: [
      //             {codeName: 'wantResidence', answer: qNeedResidenceAnswers[0]},
      //             {codeName: 'returnHome', answer: qNeedResidenceAnswers[1]},
      //         ],
      //         questionCode: "needResidence"
      //     }
      // },
      // {
      //     type: "textareaQuestion",
      //     props: {
      //         title: qAboutYourself,
      //         onInput: e => {
      //             callback('aboutYourself', qAboutYourself, e.target.value, e.target.value)
      //         },
      //         value: aboutYourself
      //     }
      // },
      {
        type: 'inputQuestion',
        props: {
          title: qYourPhone,
          maskField: true,
          onInput: e => {
            callback('phone', qYourPhone, e.target.value, e.target.value)
          },
          value: phone
        }
      },
      {
        type: 'inputQuestion',
        props: {
          title: qYourEmail,
          onInput: e => {
            callback('email', qYourEmail, e.target.value, e.target.value)
          },
          value: email
        }
      }
    ]
    return (
      <>
        <h4>{programJob}</h4>
        <div className="questions">
          {questions.map(({ type, props }, index) => {
            const types = {
              inputQuestion: <InputQuestion {...props} />,
              singleAnswerQuestion:
                <SingleAnswerQuestion {...props} callback={callback} questionsData={data}/>,
              multipleSelectionList: <MultipleSelectionList {...props} />,
              multipleListSearch: <MultipleListSearch {...props} />,
              numberInput: <NumberInput {...props} />,
              textareaQuestion: <TextareaQuestion {...props} />
            }
            return type && props && <React.Fragment key={index}>{
              React.cloneElement(types[type], { number: index >= 9 ? index + 1 : `0${index + 1}` })
            }</React.Fragment>
          })}
        </div>
      </>
    )
  }
}