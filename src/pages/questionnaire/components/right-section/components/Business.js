import React from 'react'
import {
  InputQuestion,
  TextareaQuestion,
  NumberInput,
  SingleAnswerQuestion,
  MultipleSelectionList,
  MultipleListSearch
} from '../questions'


export class Business extends React.Component {
  constructor(props) {
    super(props)

    const { qBusiness } = props.texts
    let qBusinessTexts = {}
    Object.keys(qBusiness).forEach(item => {
      qBusinessTexts[item] = qBusiness[item]
      if ( item.includes('Answers') || item.includes('List') ) {
        qBusinessTexts[item] = qBusinessTexts[item].split('; ')
      }
    })
    this.state = {
      directionsExpanded: false,
      countriesExpanded: false,
      qBusiness: qBusinessTexts
    }
  }

  toggleDirectionsList(expanded = undefined) {
    this.setState({
      directionsExpanded: expanded !== undefined ? expanded : !this.state.directionsExpanded
    })
  }

  toggleCountriesList(expanded = undefined) {
    this.setState({
      countriesExpanded: expanded !== undefined ? expanded : !this.state.countriesExpanded
    })
  }

  render() {
    const {
      texts,
      data,
      callback: defaultCallback
    } = this.props
    const callback = (q, t, d, a) => {
      defaultCallback('business', q, t, d, a)
    }
    const {
      directionsExpanded,
      countriesExpanded,
      qBusiness
    } = this.state
    const {
      name,
      projectDetails,
      selectedDirections,
      selectedCountries,
      experienceDetails,
      relocationDetails,
      childrenCount,
      phone,
      email
    } = data
    const {
      qYourName,
      qYourPhone,
      qYourEmail,
      answerYes,
      answerNo,
      qDirections,
      qDirectionsList,
      qDirectionsChoose,
      qChooseCountries,
      qChooseCountriesPlaceholder
    } = texts
    const {
      programBusiness,
      qYouWant,
      qYouWantAnswers,
      qProjectIdea,
      qProjectInnovative,
      qProjectDetails,
      qHaveInvestments,
      qDocumentsTerms,
      qDocumentsTermsAnswers,
      qSpouse,
      qChildren,
      qChildrenCount,
      qExperience,
      qExperienceDetails,
      qCompanyFounders,
      qCompanyFoundersAnswers,
      qRelocationImportantDetails,
      qRelocationBudget,
      qRelocationBudgetAnswers,
      qRelocationTerms,
      qRelocationTermsAnswers
    } = qBusiness
    const questions = [
      {
        type: 'inputQuestion',
        props: {
          title: qYourName,
          onInput: e => {
            callback('name', qYourName, e.target.value, e.target.value)
          },
          value: name
        }
      },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qYouWant,
          answers: [
            { codeName: 'scaleBusiness', answer: qYouWantAnswers[0] },
            { codeName: 'startFromZero', answer: qYouWantAnswers[1] },
            { codeName: 'openLegalEntity', answer: qYouWantAnswers[2] }
          ],
          questionCode: 'businessGoal'
        }
      },
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qProjectIdea,
      //         answers: [
      //             {codeName: 'hasProjectIdea', answer: answerYes},
      //             {codeName: 'noProjectIdea', answer: answerNo},
      //         ],
      //         questionCode: "projectIdea"
      //     }
      // },
      ...data['projectIdea'] === 'hasProjectIdea' ?
        [ {
          type: 'singleAnswerQuestion',
          props: {
            title: qProjectInnovative,
            answers: [
              { codeName: 'projectInnovative', answer: answerYes },
              { codeName: 'projectNotInnovative', answer: answerNo }
            ],
            questionCode: 'projectInnovative'
          }
        } ] : [],
      ...data['projectInnovative'] === 'projectInnovative' ?
        [ {
          type: 'textareaQuestion',
          props: {
            title: qProjectDetails,
            onInput: e => {
              callback('projectDetails', qProjectDetails, e.target.value, e.target.value)
            },
            value: projectDetails
          }
        } ] : [],
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qHaveInvestments,
          answers: [
            { codeName: 'haveInvesments', answer: answerYes },
            { codeName: 'noInvestments', answer: answerNo }
          ],
          questionCode: 'haveInvestments'
        }
      },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qDocumentsTerms,
          answers: [
            { codeName: 'urgently', answer: qDocumentsTermsAnswers[0] },
            { codeName: 'twoToFourMonths', answer: qDocumentsTermsAnswers[1] },
            { codeName: 'fiveToSevenMonths', answer: qDocumentsTermsAnswers[2] },
            { codeName: 'eightAndMore', answer: qDocumentsTermsAnswers[3] }
          ],
          questionCode: 'documentTerms'
        }
      },
      {
        type: 'multipleSelectionList',
        props: {
          title: qDirections,
          directionsPlaceholder: qDirectionsChoose,
          expanded: directionsExpanded,
          allItems: [ 'north', 'britain', 'europe', 'asia', 'arab', 'australia', 'latin' ]
            .map(
              (item, index) => ({
                codeName: item,
                item: qDirectionsList.split('; ')[index]
              })
            ),
          selectedItems: selectedDirections,
          toggleList: this.toggleDirectionsList.bind(this),
          selectItem: (d, a) => {
            callback('selectedDirections', qDirections, d, a)
          }
        }
      },
      // ...selectedDirections.length ?
      //     [{
      //         type: "multipleListSearch",
      //         props: {
      //             title: qChooseCountries,
      //             searchPlaceholder: qChooseCountriesPlaceholder,
      //             expanded: countriesExpanded,
      //             allItems: selectedDirections.map(({codeName}) => (texts[`qCountriesList__${codeName}`].split('; '))).flat()
      //                 .filter((keyword, index, keywords) => keywords.lastIndexOf(keyword) === index)
      //                 .sort((a, b) => a < b ? -1 : 1),
      //             selectedItems: selectedCountries,
      //             toggleList: this.toggleCountriesList.bind(this),
      //             selectItem: c => {
      //                 callback("selectedCountries", qChooseCountries, c, c)
      //             },
      //         }
      //     }] : [],
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qSpouse,
          answers: [
            { codeName: 'withSpouse', answer: answerYes },
            { codeName: 'noSpouse', answer: answerNo }
          ],
          questionCode: 'spousePresence'
        }
      },
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qChildren,
      //         answers: [
      //             {codeName: 'withChildren', answer: answerYes},
      //             {codeName: 'noChildren', answer: answerNo},
      //         ],
      //         questionCode: "childrenPresence"
      //     }
      // },
      ...data['childrenPresence'] === 'withChildren' ?
        [ {
          type: 'numberInput',
          props: {
            title: qChildrenCount,
            onInput: e => {
              callback('childrenCount', qChildrenCount, e.target.value, e.target.value)
            },
            value: childrenCount
          }
        } ] : [],
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qExperience,
      //         answers: [
      //             {codeName: 'haveExperience', answer: answerYes},
      //             {codeName: 'noExperience', answer: answerNo},
      //         ],
      //         questionCode: "businessExperience"
      //     }
      // },
      // ...data['businessExperience'] === 'haveExperience' ?
      //     [{
      //         type: "textareaQuestion",
      //         props: {
      //             title: qExperienceDetails,
      //             onInput: e => {
      //                 callback('experienceDetails', qExperienceDetails, e.target.value, e.target.value)
      //             },
      //             value: experienceDetails
      //         }
      //     }] : [],
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qCompanyFounders,
      //         answers: [
      //             {codeName: 'oneFounder', answer: qCompanyFoundersAnswers[0]},
      //             {codeName: 'twoFounders', answer: qCompanyFoundersAnswers[1]},
      //             {codeName: 'threeFounders', answer: qCompanyFoundersAnswers[2]},
      //             {codeName: 'fourAndMore', answer: qCompanyFoundersAnswers[3]},
      //         ],
      //         questionCode: "companyFounders"
      //     }
      // },
      // {
      //     type: "textareaQuestion",
      //     props: {
      //         title: qRelocationImportantDetails,
      //         onInput: e => {
      //             callback('relocationDetails', qRelocationImportantDetails, e.target.value, e.target.value)
      //         },
      //         value: relocationDetails
      //     }
      // },
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qRelocationBudget,
      //         answers: [
      //             {codeName: 'budget__20-30', answer: qRelocationBudgetAnswers[0]},
      //             {codeName: 'budget__50-100', answer: qRelocationBudgetAnswers[1]},
      //             {codeName: 'budget__100+', answer: qRelocationBudgetAnswers[2]},
      //             {codeName: 'noBudget', answer: qRelocationBudgetAnswers[3]},
      //         ],
      //         questionCode: "budgetDetails"
      //     }
      // },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qRelocationTerms,
          answers: [
            { codeName: 'asap', answer: qRelocationTermsAnswers[0] },
            { codeName: 'halfYear', answer: qRelocationTermsAnswers[1] },
            { codeName: 'planning', answer: qRelocationTermsAnswers[2] }
          ],
          questionCode: 'relocationTerms'
        }
      },
      {
        type: 'inputQuestion',
        props: {
          title: qYourPhone,
          maskField: true,
          onInput: e => {
            callback('phone', qYourPhone, e.target.value, e.target.value)
          },
          value: phone
        }
      },
      {
        type: 'inputQuestion',
        props: {
          title: qYourEmail,
          onInput: e => {
            callback('email', qYourEmail, e.target.value, e.target.value)
          },
          value: email
        }
      }
    ]
    return (
      <>
        <h4>{programBusiness}</h4>
        <div className="questions">
          {questions.map(({ type, props }, index) => {
            const types = {
              inputQuestion: <InputQuestion {...props} />,
              singleAnswerQuestion:
                <SingleAnswerQuestion {...props} callback={callback} questionsData={data}/>,
              multipleSelectionList: <MultipleSelectionList {...props} />,
              multipleListSearch: <MultipleListSearch {...props} />,
              numberInput: <NumberInput {...props} />,
              textareaQuestion: <TextareaQuestion {...props} />
            }
            return type && props && <React.Fragment key={index}>{
              React.cloneElement(types[type], { number: index >= 9 ? index + 1 : `0${index + 1}` })
            }</React.Fragment>
          })}
        </div>
      </>
    )
  }
}