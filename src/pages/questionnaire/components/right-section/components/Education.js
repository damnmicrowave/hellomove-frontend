import React from 'react'
import {
  InputQuestion,
  TextareaQuestion,
  NumberInput,
  SingleAnswerQuestion,
  MultipleSelectionList,
  MultipleListSearch
} from '../questions'


export class Education extends React.Component {
  constructor(props) {
    super(props)

    const { qEdu } = props.texts
    let qEduTexts = {}
    Object.keys(qEdu).forEach(item => {
      qEduTexts[item] = qEdu[item]
      if ( item.includes('Answers') || item.includes('List') ) {
        qEduTexts[item] = qEduTexts[item].split('; ')
      }
    })
    this.state = {
      directionsExpanded: false,
      countriesExpanded: false,
      citizenshipExpanded: false,
      qEdu: qEduTexts
    }
  }

  toggleDirectionsList(expanded = undefined) {
    this.setState({
      directionsExpanded: expanded !== undefined ? expanded : !this.state.directionsExpanded
    })
  }

  toggleCountriesList(expanded = undefined) {
    this.setState({
      countriesExpanded: expanded !== undefined ? expanded : !this.state.countriesExpanded
    })
  }

  toggleCitizenshipList(expanded = undefined) {
    this.setState({
      citizenshipExpanded: expanded !== undefined ? expanded : !this.state.citizenshipExpanded
    })
  }

  render() {
    const {
      texts,
      data,
      callback: defaultCallback
    } = this.props
    const callback = (q, t, d, a) => {
      defaultCallback('edu', q, t, d, a)
    }
    const {
      directionsExpanded,
      countriesExpanded,
      citizenshipExpanded,
      qEdu
    } = this.state
    const {
      name,
      ageInfo,
      selectedCitizenship,
      educationSpeciality,
      graduationTime,
      selectedDirections,
      selectedCountries,
      specialityInterests,
      aboutYourself,
      phone,
      email
    } = data
    const {
      qYourName,
      qYourPhone,
      qYourEmail,
      answerYes,
      answerNo,
      qDirections,
      qDirectionsList,
      qDirectionsChoose,
      qChooseCountries,
      qChooseCountriesPlaceholder
    } = texts
    const {
      programEdu,
      qCitizenship,
      qCitizenshipAnswers,
      qAge,
      qEducationType,
      qEducationTypeAnswers,
      qStudiyngNow,
      qWhatSpeciality,
      qGraduationTime,
      qAcademicVacation,
      qAcademicVacationAnswers,
      qDocumentTerms,
      qDocumentTermsAnswers,
      qBudget,
      qBudgetAnswers,
      qSpecialityInterests,
      qAboutYourself
    } = qEdu
    const questions = [
      {
        type: 'inputQuestion',
        props: {
          title: qYourName,
          onInput: e => {
            callback('name', qYourName, e.target.value, e.target.value)
          },
          value: name
        }
      },
      {
        type: 'multipleListSearch',
        props: {
          title: qCitizenship,
          searchPlaceholder: qChooseCountriesPlaceholder,
          expanded: citizenshipExpanded,
          allItems: qCitizenshipAnswers
            .filter((keyword, index, keywords) => keywords.lastIndexOf(keyword) === index)
            .sort((a, b) => a < b ? -1 : 1),
          selectedItems: selectedCitizenship,
          toggleList: this.toggleCitizenshipList.bind(this),
          selectItem: c => {
            callback('selectedCitizenship', qCitizenship, c, c)
          }
        }
      },
      {
        type: 'numberInput',
        props: {
          title: qAge,
          onInput: e => {
            callback('ageInfo', qAge, e.target.value, e.target.value)
          },
          value: ageInfo
        }
      },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qEducationType,
          answers: [
            { codeName: 'undergraduate', answer: qEducationTypeAnswers[0] },
            { codeName: 'magistracy', answer: qEducationTypeAnswers[1] },
            { codeName: 'twoYearCources', answer: qEducationTypeAnswers[2] },
            { codeName: 'shortCources', answer: qEducationTypeAnswers[3] },
            { codeName: 'school', answer: qEducationTypeAnswers[4] }
          ],
          questionCode: 'educationType'
        }
      },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qStudiyngNow,
          answers: [
            { codeName: 'studiyngNow', answer: answerYes },
            { codeName: 'notStudiyngNow', answer: answerNo }
          ],
          questionCode: 'studiyngNow'
        }
      },
      ...data['studiyngNow'] === 'studiyngNow' ?
        [ {
          type: 'inputQuestion',
          props: {
            title: qWhatSpeciality,
            onInput: e => {
              callback('educationSpeciality', qWhatSpeciality, e.target.value, e.target.value)
            },
            value: educationSpeciality
          }
        },
          {
            type: 'inputQuestion',
            props: {
              title: qGraduationTime,
              onInput: e => {
                callback('graduationTime', qGraduationTime, e.target.value, e.target.value)
              },
              value: graduationTime
            }
          },
          {
            type: 'singleAnswerQuestion',
            props: {
              title: qAcademicVacation,
              answers: [
                { codeName: 'takeAcademicVacation', answer: qAcademicVacationAnswers[0] },
                { codeName: 'noAcademicVacation', answer: qAcademicVacationAnswers[1] },
                { codeName: 'graduateFirst', answer: qAcademicVacationAnswers[2] }
              ],
              questionCode: 'academicVacation'
            }
          } ] : [],
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qDocumentTerms,
          answers: [
            { codeName: 'urgently', answer: qDocumentTermsAnswers[0] },
            { codeName: 'twoToFourMonths', answer: qDocumentTermsAnswers[1] },
            { codeName: 'fiveToSevenMonths', answer: qDocumentTermsAnswers[2] },
            { codeName: 'eightAndMore', answer: qDocumentTermsAnswers[3] }
          ],
          questionCode: 'documentTerms'
        }
      },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qBudget,
          answers: [
            { codeName: 'budgetLow', answer: qBudgetAnswers[0] },
            { codeName: 'budgetFiveToTen', answer: qBudgetAnswers[1] },
            { codeName: 'budgetTenToFifteen', answer: qBudgetAnswers[2] },
            { codeName: 'budgetNoLimit', answer: qBudgetAnswers[3] }
          ],
          questionCode: 'budgetDetails'
        }
      },
      {
        type: 'multipleSelectionList',
        props: {
          title: qDirections,
          directionsPlaceholder: qDirectionsChoose,
          expanded: directionsExpanded,
          allItems: [ 'north', 'britain', 'europe', 'asia', 'arab', 'australia', 'latin' ]
            .map(
              (item, index) => ({
                codeName: item,
                item: qDirectionsList.split('; ')[index]
              })
            ),
          selectedItems: selectedDirections,
          toggleList: this.toggleDirectionsList.bind(this),
          selectItem: (d, a) => {
            callback('selectedDirections', qDirections, d, a)
          }
        }
      },
      ...selectedDirections.length ?
        [ {
          type: 'multipleListSearch',
          props: {
            title: qChooseCountries,
            searchPlaceholder: qChooseCountriesPlaceholder,
            expanded: countriesExpanded,
            allItems: selectedDirections.map(({ codeName }) => (texts[`qCountriesList__${codeName}`].split('; '))).flat()
              .filter((keyword, index, keywords) => keywords.lastIndexOf(keyword) === index)
              .sort((a, b) => a < b ? -1 : 1),
            selectedItems: selectedCountries,
            toggleList: this.toggleCountriesList.bind(this),
            selectItem: c => {
              callback('selectedCountries', qChooseCountries, c, c)
            }
          }
        } ] : [],
      {
        type: 'textareaQuestion',
        props: {
          title: qSpecialityInterests,
          onInput: e => {
            callback('specialityInterests', qSpecialityInterests, e.target.value, e.target.value)
          },
          value: specialityInterests
        }
      },
      {
        type: 'textareaQuestion',
        props: {
          title: qAboutYourself,
          onInput: e => {
            callback('aboutYourself', qAboutYourself, e.target.value, e.target.value)
          },
          value: aboutYourself
        }
      },
      {
        type: 'inputQuestion',
        props: {
          title: qYourPhone,
          maskField: true,
          onInput: e => {
            callback('phone', qYourPhone, e.target.value, e.target.value)
          },
          value: phone
        }
      },
      {
        type: 'inputQuestion',
        props: {
          title: qYourEmail,
          onInput: e => {
            callback('email', qYourEmail, e.target.value, e.target.value)
          },
          value: email
        }
      }
    ]
    return (
      <>
        <h4>{programEdu}</h4>
        <div className="questions">
          {questions.map(({ type, props }, index) => {
            const types = {
              inputQuestion: <InputQuestion {...props} />,
              singleAnswerQuestion:
                <SingleAnswerQuestion {...props} callback={callback} questionsData={data}/>,
              multipleSelectionList: <MultipleSelectionList {...props} />,
              multipleListSearch: <MultipleListSearch {...props} />,
              numberInput: <NumberInput {...props} />,
              textareaQuestion: <TextareaQuestion {...props} />
            }
            return type && props && <React.Fragment key={index}>{
              React.cloneElement(types[type], { number: index >= 9 ? index + 1 : `0${index + 1}` })
            }</React.Fragment>
          })}
        </div>
      </>
    )
  }
}