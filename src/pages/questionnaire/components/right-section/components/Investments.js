import React from 'react'
import {
  InputQuestion,
  TextareaQuestion,
  NumberInput,
  SingleAnswerQuestion,
  MultipleSelectionList,
  MultipleListSearch
} from '../questions'


export class Investments extends React.Component {
  constructor(props) {
    super(props)

    const { qInvestments } = props.texts
    let qInvestmentsTexts = {}
    Object.keys(qInvestments).forEach(item => {
      qInvestmentsTexts[item] = qInvestments[item]
      if ( item.includes('Answers') || item.includes('List') ) {
        qInvestmentsTexts[item] = qInvestmentsTexts[item].split('; ')
      }
    })
    this.state = {
      directionsExpanded: false,
      countriesExpanded: false,
      qInvestments: qInvestmentsTexts
    }
  }

  toggleDirectionsList(expanded = undefined) {
    this.setState({
      directionsExpanded: expanded !== undefined ? expanded : !this.state.directionsExpanded
    })
  }

  toggleCountriesList(expanded = undefined) {
    this.setState({
      countriesExpanded: expanded !== undefined ? expanded : !this.state.countriesExpanded
    })
  }

  render() {
    const {
      texts,
      data,
      callback: defaultCallback
    } = this.props
    const callback = (q, t, d, a) => {
      defaultCallback('investments', q, t, d, a)
    }
    const {
      directionsExpanded,
      countriesExpanded,
      qInvestments
    } = this.state
    const {
      name,
      selectedDirections,
      selectedCountries,
      relocationDetails,
      childrenCount,
      phone,
      email
    } = data
    const {
      qYourName,
      qYourPhone,
      qYourEmail,
      answerYes,
      answerNo,
      qDirections,
      qDirectionsList,
      qDirectionsChoose,
      qChooseCountries,
      qChooseCountriesPlaceholder
    } = texts
    const {
      programInvestments,
      qResidenceGoal,
      qResidenceGoalAnswers,
      qDocumentTerms,
      qDocumentTermsAnswers,
      qMaxInvestment,
      qMaxInvestmentAnswers,
      qWhatInvestment,
      qWhatInvestmentAnswers,
      qCountryImportantDetails,
      qSpouse,
      qChildren,
      qChildrenCount,
      qRelocationTerms,
      qRelocationTermsAnswers
    } = qInvestments
    const questions = [
      {
        type: 'inputQuestion',
        props: {
          title: qYourName,
          onInput: e => {
            callback('name', qYourName, e.target.value, e.target.value)
          },
          value: name
        }
      },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qResidenceGoal,
          answers: [
            { codeName: 'visalessTravel', answer: qResidenceGoalAnswers[0] },
            { codeName: 'changeCountry', answer: qResidenceGoalAnswers[1] },
            { codeName: 'safetyPillow', answer: qResidenceGoalAnswers[2] }
          ],
          questionCode: 'residenceGoal'
        }
      },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qDocumentTerms,
          answers: [
            { codeName: 'urgently', answer: qDocumentTermsAnswers[0] },
            { codeName: 'twoToFourMonths', answer: qDocumentTermsAnswers[1] },
            { codeName: 'fiveToSevenMonths', answer: qDocumentTermsAnswers[2] },
            { codeName: 'eightAndMore', answer: qDocumentTermsAnswers[3] }
          ],
          questionCode: 'documentTerms'
        }
      },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qMaxInvestment,
          answers: [
            { codeName: 'maxInvestment__200', answer: qMaxInvestmentAnswers[0] },
            { codeName: 'maxInvestment__500', answer: qMaxInvestmentAnswers[1] },
            { codeName: 'maxInvestment__1000', answer: qMaxInvestmentAnswers[2] },
            { codeName: 'maxInvestment__1000+', answer: qMaxInvestmentAnswers[3] }
          ],
          questionCode: 'maxInvestment'
        }
      },
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qWhatInvestment,
      //         answers: [
      //             {codeName: 'estate', answer: qWhatInvestmentAnswers[0]},
      //             {codeName: 'ownBusiness', answer: qWhatInvestmentAnswers[1]},
      //             {codeName: 'otherBusiness', answer: qWhatInvestmentAnswers[2]},
      //             {codeName: 'stocks', answer: qWhatInvestmentAnswers[3]},
      //         ],
      //         questionCode: "whatInvestment"
      //     }
      // },
      {
        type: 'multipleSelectionList',
        props: {
          title: qDirections,
          directionsPlaceholder: qDirectionsChoose,
          expanded: directionsExpanded,
          allItems: [ 'north', 'britain', 'europe', 'asia', 'arab', 'australia', 'latin' ]
            .map(
              (item, index) => ({
                codeName: item,
                item: qDirectionsList.split('; ')[index]
              })
            ),
          selectedItems: selectedDirections,
          toggleList: this.toggleDirectionsList.bind(this),
          selectItem: (d, a) => {
            callback('selectedDirections', qDirections, d, a)
          }
        }
      },
      // ...selectedDirections.length ?
      //     [{
      //         type: "multipleListSearch",
      //         props: {
      //             title: qChooseCountries,
      //             searchPlaceholder: qChooseCountriesPlaceholder,
      //             expanded: countriesExpanded,
      //             allItems: selectedDirections.map(({codeName}) => (texts[`qCountriesList__${codeName}`].split('; '))).flat()
      //                 .filter((keyword, index, keywords) => keywords.lastIndexOf(keyword) === index)
      //                 .sort((a, b) => a < b ? -1 : 1),
      //             selectedItems: selectedCountries,
      //             toggleList: this.toggleCountriesList.bind(this),
      //             selectItem: c => {
      //                 callback("selectedCountries", qChooseCountries, c, c)
      //             },
      //         }
      //     }] : [],
      // {
      //     type: "textareaQuestion",
      //     props: {
      //         title: qCountryImportantDetails,
      //         onInput: e => {
      //             callback('relocationDetails', qCountryImportantDetails, e.target.value, e.target.value)
      //         },
      //         value: relocationDetails
      //     }
      // },
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qSpouse,
      //         answers: [
      //             {codeName: 'withSpouse', answer: answerYes},
      //             {codeName: 'noSpouse', answer: answerNo},
      //         ],
      //         questionCode: "spousePresence"
      //     }
      // },
      // {
      //     type: "singleAnswerQuestion",
      //     props: {
      //         title: qChildren,
      //         answers: [
      //             {codeName: 'withChildren', answer: answerYes},
      //             {codeName: 'noChildren', answer: answerNo},
      //         ],
      //         questionCode: "childrenPresence"
      //     }
      // },
      ...data['childrenPresence'] === 'withChildren' ?
        [ {
          type: 'numberInput',
          props: {
            title: qChildrenCount,
            onInput: e => {
              callback('childrenCount', qChildrenCount, e.target.value, e.target.value)
            },
            value: childrenCount
          }
        } ] : [],
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qRelocationTerms,
          answers: [
            { codeName: 'asap', answer: qRelocationTermsAnswers[0] },
            { codeName: 'halfYear', answer: qRelocationTermsAnswers[1] },
            { codeName: 'planning', answer: qRelocationTermsAnswers[2] }
          ],
          questionCode: 'relocationTerms'
        }
      },
      {
        type: 'inputQuestion',
        props: {
          title: qYourPhone,
          maskField: true,
          onInput: e => {
            callback('phone', qYourPhone, e.target.value, e.target.value)
          },
          value: phone
        }
      },
      {
        type: 'inputQuestion',
        props: {
          title: qYourEmail,
          onInput: e => {
            callback('email', qYourEmail, e.target.value, e.target.value)
          },
          value: email
        }
      }
    ]
    return (
      <>
        <h4>{programInvestments}</h4>
        <div className="questions">
          {questions.map(({ type, props }, index) => {
            const types = {
              inputQuestion: <InputQuestion {...props} />,
              singleAnswerQuestion:
                <SingleAnswerQuestion {...props} callback={callback} questionsData={data}/>,
              multipleSelectionList: <MultipleSelectionList {...props} />,
              multipleListSearch: <MultipleListSearch {...props} />,
              numberInput: <NumberInput {...props} />,
              textareaQuestion: <TextareaQuestion {...props} />
            }
            return type && props && <React.Fragment key={index}>{
              React.cloneElement(types[type], { number: index >= 9 ? index + 1 : `0${index + 1}` })
            }</React.Fragment>
          })}
        </div>
      </>
    )
  }
}