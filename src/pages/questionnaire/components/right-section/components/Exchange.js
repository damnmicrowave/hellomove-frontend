import React from 'react'
import {
  InputQuestion,
  TextareaQuestion,
  NumberInput,
  SingleAnswerQuestion,
  MultipleSelectionList,
  MultipleListSearch
} from '../questions'


export class Exchange extends React.Component {
  constructor(props) {
    super(props)

    const { qExchange } = props.texts
    let qExchangeTexts = {}
    Object.keys(qExchange).forEach(item => {
      qExchangeTexts[item] = qExchange[item]
      if ( item.includes('Answers') || item.includes('List') ) {
        qExchangeTexts[item] = qExchangeTexts[item].split('; ')
      }
    })
    this.state = {
      directionsExpanded: false,
      countriesExpanded: false,
      qExchange: qExchangeTexts
    }
  }

  toggleDirectionsList(expanded = undefined) {
    this.setState({
      directionsExpanded: expanded !== undefined ? expanded : !this.state.directionsExpanded
    })
  }

  toggleCountriesList(expanded = undefined) {
    this.setState({
      countriesExpanded: expanded !== undefined ? expanded : !this.state.countriesExpanded
    })
  }

  render() {
    const {
      texts,
      data,
      callback: defaultCallback
    } = this.props
    const callback = (q, t, d, a) => {
      defaultCallback('exchange', q, t, d, a)
    }
    const {
      directionsExpanded,
      countriesExpanded,
      qExchange
    } = this.state
    const {
      name,
      ageInfo,
      selectedDirections,
      selectedCountries,
      aboutYourself,
      phone,
      email
    } = data
    const {
      warningMessage,
      qYourName,
      qYourPhone,
      qYourEmail,
      answerYes,
      answerNo,
      qDirections,
      qDirectionsList,
      qDirectionsChoose,
      qChooseCountries,
      qChooseCountriesPlaceholder
    } = texts
    const {
      programExchange,
      qAge,
      qSex,
      qSexAnswers,
      qStudiyngNow,
      qSideJob,
      qBudget,
      qBudgetAnswers,
      qAboutYourself
    } = qExchange
    const questions = [
      {
        type: 'inputQuestion',
        props: {
          title: qYourName,
          onInput: e => {
            callback('name', qYourName, e.target.value, e.target.value)
          },
          value: name
        }
      },
      {
        type: 'numberInput',
        props: {
          title: qAge,
          onInput: e => {
            callback('ageInfo', qAge, e.target.value, e.target.value)
          },
          value: ageInfo
        }
      },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qSex,
          answers: [
            { codeName: 'male', answer: qSexAnswers[0] },
            { codeName: 'female', answer: qSexAnswers[1] }
          ],
          questionCode: 'sex'
        }
      },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qStudiyngNow,
          answers: [
            { codeName: 'studiyngNow', answer: answerYes },
            { codeName: 'notStudiyng', answer: answerNo }
          ],
          questionCode: 'studiyngNow'
        }
      },
      {
        type: 'multipleSelectionList',
        props: {
          title: qDirections,
          directionsPlaceholder: qDirectionsChoose,
          expanded: directionsExpanded,
          allItems: [ 'north', 'britain', 'europe', 'asia', 'arab', 'australia', 'latin' ]
            .map(
              (item, index) => ({
                codeName: item,
                item: qDirectionsList.split('; ')[index]
              })
            ),
          selectedItems: selectedDirections,
          toggleList: this.toggleDirectionsList.bind(this),
          selectItem: (d, a) => {
            callback('selectedDirections', qDirections, d, a)
          }
        }
      },
      // ...selectedDirections.length ?
      //     [{
      //         type: "multipleListSearch",
      //         props: {
      //             title: qChooseCountries,
      //             searchPlaceholder: qChooseCountriesPlaceholder,
      //             expanded: countriesExpanded,
      //             allItems: selectedDirections.map(({codeName}) => (texts[`qCountriesList__${codeName}`].split('; '))).flat()
      //                 .filter((keyword, index, keywords) => keywords.lastIndexOf(keyword) === index)
      //                 .sort((a, b) => a < b ? -1 : 1),
      //             selectedItems: selectedCountries,
      //             toggleList: this.toggleCountriesList.bind(this),
      //             selectItem: c => {
      //                 callback("selectedCountries", qChooseCountries, c, c)
      //             },
      //         }
      //     }] : [],
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qSideJob,
          answers: [
            { codeName: 'needSideJob', answer: answerYes },
            { codeName: 'noSideJob', answer: answerNo }
          ],
          questionCode: 'sideJob'
        }
      },
      {
        type: 'singleAnswerQuestion',
        props: {
          title: qBudget,
          answers: [
            { codeName: 'budgetLow', answer: qBudgetAnswers[0] },
            { codeName: 'budgetOneToTwo', answer: qBudgetAnswers[1] },
            { codeName: 'budgetNoLimit', answer: qBudgetAnswers[2] }
          ],
          questionCode: 'budgetDetails'
        }
      },
      // {
      //     type: "textareaQuestion",
      //     props: {
      //         title: qAboutYourself,
      //         onInput: e => {
      //             callback('aboutYourself', qAboutYourself, e.target.value, e.target.value)
      //         },
      //         value: aboutYourself
      //     }
      // },
      {
        type: 'inputQuestion',
        props: {
          title: qYourPhone,
          maskField: true,
          onInput: e => {
            callback('phone', qYourPhone, e.target.value, e.target.value)
          },
          value: phone
        }
      },
      {
        type: 'inputQuestion',
        props: {
          title: qYourEmail,
          onInput: e => {
            callback('email', qYourEmail, e.target.value, e.target.value)
          },
          value: email
        }
      }
    ]
    return (
      <>
        <h4>{programExchange}</h4>
        {/*<div className="warning">*/}
        {/*    {warningMessage}*/}
        {/*</div>*/}
        <div className="questions">
          {questions.map(({ type, props }, index) => {
            const types = {
              inputQuestion: <InputQuestion {...props} />,
              singleAnswerQuestion:
                <SingleAnswerQuestion {...props} callback={callback} questionsData={data}/>,
              multipleSelectionList: <MultipleSelectionList {...props} />,
              multipleListSearch: <MultipleListSearch {...props} />,
              numberInput: <NumberInput {...props} />,
              textareaQuestion: <TextareaQuestion {...props} />
            }
            return type && props && <React.Fragment key={index}>{
              React.cloneElement(types[type], { number: index >= 9 ? index + 1 : `0${index + 1}` })
            }</React.Fragment>
          })}
        </div>
      </>
    )
  }
}