import React from 'react'
import './styles.scss'

export class NumberInput extends React.Component {
  render() {
    const {
      number,
      title,
      value,
      onInput
    } = this.props
    return (
      <div className="question number-input">
        <div className="title-box">
                    <span className="number">
                        {number}
                    </span>
          <span className="title">
                        {title}
                    </span>
        </div>
        <input
          onChange={onInput}
          value={value}
          type="number"
          min={1}
          className="answer"
        />
      </div>
    )
  }
}