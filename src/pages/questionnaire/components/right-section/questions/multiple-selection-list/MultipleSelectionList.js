import React from 'react'
import { CheckBox } from '../../../../../../includes'
import {
  getParents,
  removeElement
} from '../../../../../../utils'
import './styles.scss'

export class MultipleSelectionList extends React.Component {
  constructor(props) {
    super(props)

    this.selectionList = React.createRef()
  }

  componentDidMount() {
    window.onclick = e => {
      const { toggleList } = this.props
      const parents = getParents(e.target)
      if ( !parents.includes(this.selectionList.current) ) {
        toggleList(false)
      }
    }
  }

  manageSelection(codeName, item) {
    const {
      selectedItems,
      selectItem
    } = this.props
    if ( !selectedItems.map(x => (x.codeName)).includes(codeName) ) {
      selectItem(
        [ ...selectedItems, { codeName: codeName, item: item } ],
        [ ...selectedItems.map(x => (x.item)), item ]
      )
    } else {
      selectItem(
        [ ...selectedItems.filter(elem => (elem.codeName !== codeName)) ],
        [ ...removeElement(selectedItems.map(x => (x.item)), item) ]
      )
    }
  }

  render() {
    const {
      number,
      title,
      directionsPlaceholder,
      expanded,
      toggleList,
      allItems,
      selectedItems
    } = this.props
    return (
      <div className="question multiple-selection-list">
        <div className="title-box">
                    <span className="number">
                        {number}
                    </span>
          <span className="title">
                        {title}
                    </span>
        </div>
        <div ref={this.selectionList} className={`list-box${expanded ? ' expanded' : ''}`}>
          <button onClick={() => {
            toggleList()
          }} className="toggle-btn">
            {selectedItems.length ?
              selectedItems.map(x => (x.item)).join(', ') :
              directionsPlaceholder
            }
          </button>
          <div className="items">
            {allItems.map(({ codeName, item }, index) => (
              <label onClick={e => {
                // noinspection JSUnresolvedVariable
                if ( e.target.nodeName === 'INPUT' ) this.manageSelection.bind(this)(codeName, item)
              }} key={index} className="elem">
                <CheckBox checked={selectedItems.map(x => (x.codeName)).includes(codeName)}/>
                <span>{item}</span>
              </label>
            ))}
          </div>
        </div>
      </div>
    )
  }
}