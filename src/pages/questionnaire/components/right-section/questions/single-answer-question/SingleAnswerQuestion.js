import React from 'react'
import { CheckBox } from '../../../../../../includes'
import './styles.scss'

export class SingleAnswerQuestion extends React.Component {
  render() {
    const {
      number,
      title,
      questionCode,
      questionsData,
      answers,
      callback
    } = this.props
    const currentAnswer = questionsData[questionCode]
    return (
      <div className="question single-answer-question">
        <div className="title-box">
                    <span className="number">
                        {number}
                    </span>
          <span className="title">
                        {title}
                    </span>
        </div>
        <div className="answers-box">
          {answers.map(({ codeName, answer }, index) => (
            <label onClick={() => {
              callback(questionCode, title, codeName, answer)
            }} key={title + index}>
              <CheckBox checked={codeName === currentAnswer}/>
              <span className="answer">
                                {answer}
                            </span>
            </label>
          ))}
        </div>
      </div>
    )
  }
}