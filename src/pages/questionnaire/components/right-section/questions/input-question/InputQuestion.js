import React from 'react'
import './styles.scss'
import InputMask from 'react-input-mask'

export class InputQuestion extends React.Component {
  render() {
    const {
      maskField,
      number,
      title,
      value,
      onInput
    } = this.props
    return (
      <div className="question input-question">
        <div className="title-box">
                    <span className="number">
                        {number}
                    </span>
          <span className="title">
                        {title}
                    </span>
        </div>
        {maskField ?
          <InputMask
            mask="+99999999999999"
            maskChar=" "
            {...{
              name: 'questionnaire_phone',
              value: value,
              autoComplete: 'off',
              onChange: onInput
            }}
            className="answer"
          /> :
          <input
            onChange={onInput}
            value={value}
            type="text"
            className="answer"
          />}
      </div>
    )
  }
}