import React from 'react'
import './styles.scss'

export class TextareaQuestion extends React.Component {
  render() {
    const {
      number,
      title,
      value,
      onInput
    } = this.props
    return (
      <div className="question textarea-question">
        <div className="title-box">
                    <span className="number">
                        {number}
                    </span>
          <span className="title">
                        {title}
                    </span>
        </div>
        <textarea
          spellCheck={false}
          onChange={onInput}
          value={value}
          className="answer"
        />
      </div>
    )
  }
}