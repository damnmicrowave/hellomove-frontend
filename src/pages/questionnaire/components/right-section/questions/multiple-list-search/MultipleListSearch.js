import React from 'react'
import { getParents, removeElement } from '../../../../../../utils'
import { CheckBox } from '../../../../../../includes'
import './styles.scss'
import timesSVG from './media/times.svg'

export class MultipleListSearch extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      searchQuery: ''
    }

    this.searchList = React.createRef()
    this.closeButton = React.createRef()
  }

  componentDidMount() {
    window.addEventListener('click', this.onClickEvent.bind(this))
  }

  onClickEvent(e) {
    const { toggleList } = this.props
    const parents = getParents(e.target)
    if ( !parents.includes(this.searchList.current) || e.target === this.closeButton.current ) {
      toggleList(false)
    } else {
      toggleList(true)
    }
  }

  manageSelection(item) {
    const {
      selectedItems,
      selectItem
    } = this.props
    if ( !selectedItems.includes(item) ) {
      selectItem(
        [ ...selectedItems, item ]
      )
    } else {
      selectItem(
        [ ...removeElement(selectedItems, item) ]
      )
    }
  }

  render() {
    const {
      number,
      title,
      searchPlaceholder,
      expanded,
      toggleList,
      allItems,
      selectedItems
    } = this.props
    const { searchQuery } = this.state
    return (
      <div className="question multiple-list-search">
        <div className="title-box">
                    <span className="number">
                        {number}
                    </span>
          <span className="title">
                        {title}
                    </span>
        </div>
        <div className="selected">
          {selectedItems.join(', ')}
        </div>
        <div ref={this.searchList} className={`list-box${expanded ? ' expanded' : ''}`}>
          <div className="list-input">
            <input
              onFocus={() => {
                toggleList(true)
              }}
              onChange={e => {
                const { value } = e.target
                this.setState({
                  searchQuery: value
                })
              }}
              value={searchQuery}
              type="text"
              placeholder={searchPlaceholder}
            />
            <img ref={this.closeButton} onClick={() => {
              toggleList(false)
            }} src={timesSVG} alt="close list"/>
          </div>
          <div className="items">
            {allItems.map((item, index) => (
              item.toLowerCase().includes(searchQuery.toLowerCase()) &&
              <label onClick={e => {
                // noinspection JSUnresolvedVariable
                if ( e.target.nodeName === 'INPUT' ) {
                  this.setState({ searchQuery: '' })
                  this.manageSelection.bind(this)(item)
                }
              }} key={index} className="elem">
                <CheckBox checked={selectedItems.includes(item)}/>
                <span>{item}</span>
              </label>
            ))}
          </div>
        </div>
      </div>
    )
  }
}