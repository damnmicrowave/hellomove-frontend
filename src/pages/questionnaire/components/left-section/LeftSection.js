import React from 'react'
import { NavLink } from 'react-router-dom'
import { CheckBox } from '../../../../includes'
import './styles.scss'


export class LeftSection extends React.Component {
  render() {
    const {
      texts,
      onUpdate
    } = this.props
    const {
      programsTitle,
      programBusiness,
      programInvestments,
      programJob,
      programEdu,
      programExchange
    } = texts
    const options = [
      {
        link: 'business',
        text: programBusiness
      },
      {
        link: 'investments',
        text: programInvestments
      },
      {
        link: 'job',
        text: programJob
      },
      // {
      //     link: 'edu',
      //     text: programEdu
      // },
      {
        link: 'exchange',
        text: programExchange
      }
    ]
    return (
      <div className="left-section">
        <h4>{programsTitle}</h4>
        <div className="programs-choice">
          {options.map(({ link, text }, index) => (
            <NavLink onClick={onUpdate} key={index} to={`/questionnaire/${link}/`}>
              <CheckBox/>
              <span className="program-type">{text}</span>
            </NavLink>
          ))}
        </div>
      </div>
    )
  }
}
