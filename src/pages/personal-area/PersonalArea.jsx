import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { apiRequest } from '../../utils'
import { Loading } from '../../includes'

import styles from './PersonalArea.module.scss'
import { ReactComponent as CheckboxSVG } from './media/checkbox.svg'

export const PersonalArea = ({ texts }) => {

  const [ loading, setLoading ] = useState(true)
  const [ userData, setUserData ] = useState({})
  const { user, program } = userData
  useEffect(() => {
    apiRequest(({ user, program, notAuthorized }) => {
      if ( notAuthorized ) window.location.replace('/login/')
      program.steps = program.steps.sort((a, b) => a.number - b.number)
      setUserData({ user, program })
      setLoading(false)
    }, 'get_userdata')
  }, [])

  useEffect(() => {
    const tabs = Array.from(document.getElementsByClassName(styles.tab))
    tabs.forEach((tab, index) => {
      if ( tab.classList.contains(styles.active) ) {
        tabs[index - 1].style.borderBottomRightRadius = '10px'
        tabs[index + 1].style.borderTopRightRadius = '10px'
      }
    })
  })

  const { stepsCompleted, myProcess, myInfo, myDocuments, myPayments, step: stepText, from, beginStage } = texts

  return (
    <div className={styles.wrapper}>
      {loading ? <Loading/> :
        <div className={styles.background}>
          <div className={styles.panel}>
            <h3>{user.name}</h3>
            <div className={styles.progress}>
              {stepsCompleted}: {user.stepsCompleted.length} {from} {program.steps.length}
            </div>
            <div className={styles.tabs}>
              <div className={styles.tab}/>
              <div className={`${styles.tab} ${styles.active}`}>
                {myProcess}
              </div>
              <div className={`${styles.tab} ${styles.disabled}`}>
                {myInfo}
              </div>
              <div className={`${styles.tab} ${styles.disabled}`}>
                {myDocuments}
              </div>
              <div className={`${styles.tab} ${styles.disabled}`}>
                {myPayments}
              </div>
            </div>
          </div>
          <div className={styles.view}>
            <h3>{myProcess}</h3>
            <div className={styles.content}>
              {program.steps.map((step, index) => (
                user.stepsCompleted.includes(index) ?
                  <div key={index} className={`${styles.step} ${styles.completed}`}>
                    <Link to={{
                      pathname: '/step/',
                      state: { steps: program.steps, completedSteps: user.stepsCompleted, currentStep: index }
                    }} className={styles.number}>{stepText} {index + 1}. {step.name}</Link>
                    <CheckboxSVG/>
                  </div> : <div key={index} className={styles.step}>
                    <div className={styles.number}>{stepText} {index + 1}. {step.name}</div>
                    <CheckboxSVG/>
                    <Link to={{
                      pathname: '/step/',
                      state: { steps: program.steps, completedSteps: user.stepsCompleted, currentStep: index }
                    }} className={styles.beginStage}>{beginStage}</Link>
                  </div>
              ))}
            </div>
          </div>
        </div>}
    </div>
  )
}