import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { ConfirmModal } from './confirm-modal/ConfirmModal'

import styles from './ProgramStep.module.scss'
import { apiRequest } from '../../utils'
import { ThankYou } from '../../includes'

export const ProgramStep = ({ location, history, texts }) => {

  let { steps, completedSteps, currentStep } = location.state
  const { name, text, links } = steps[currentStep]
  const [ completed, setCompleted ] = useState(completedSteps.includes(currentStep))

  const [ modalShown, setModalShown ] = useState(false)
  const [ feedbackShown, setFeedbackShown ] = useState(false)
  const stepSupport = () => {
    apiRequest(({ status }) => {
      setModalShown(false)
      if ( status === 'ok' ) {
        setFeedbackShown(true)
        setTimeout(() => setFeedbackShown(false), 5000)
      } else {
        alert('Необходимо войти в аккаунт')
        history.push('/login/')
      }
    }, `step_support/${currentStep}`)
  }

  const toggleCompleted = () => {
    setCompleted(!completed)
    apiRequest(() => {
    }, `toggle_step/${currentStep}`)
  }

  const {
    popupTitle, popupMessage, backToPersonalArea, step, usefulLinks, orderConsult, previousStep,
    nextStep, markAsIncomplete, markAsComplete
  } = texts

  return (
    <>
      {feedbackShown &&
      <ThankYou
        title={popupTitle}
        message={popupMessage}
      />}
      {modalShown && <ConfirmModal toggle={setModalShown} action={stepSupport} texts={texts}/>}
      <div className={styles.wrapper}>
        <Link to='/personal_area/' className={styles.backLink}>{backToPersonalArea}</Link>
        <h3>{step} {currentStep + 1}. {name}</h3>
        <div className={styles.contents}>
          <div className={styles.text} dangerouslySetInnerHTML={{ __html: text }}/>
          {Boolean(links.length) &&
          <div className={styles.links}>
            <h5>{usefulLinks}:</h5>
            {links.map(({ text, url }, index) => (
              <a key={index} href={url} target='_blank' rel='noopener noreferrer'>{text}</a>
            ))}
          </div>}
        </div>
        <div className={styles.buttons}>
          <button onClick={() => setModalShown(true)} className={styles.button}>
            <span>{orderConsult}</span>
          </button>
          {Boolean(currentStep) &&
          <Link to={{
            pathname: '/step/',
            state: { steps, completedSteps, currentStep: currentStep - 1 }
          }} className={styles.button}>
            <span>{previousStep}</span>
          </Link>}
          <button onClick={toggleCompleted} className={styles.button}>
            <span>{completed ? markAsIncomplete : markAsComplete}</span>
          </button>
          {currentStep + 1 !== steps.length &&
          <Link to={{
            pathname: '/step/',
            state: { steps, completedSteps, currentStep: currentStep + 1 }
          }} className={styles.button}>
            <span>{nextStep}</span>
          </Link>}
        </div>
      </div>
    </>
  )
}