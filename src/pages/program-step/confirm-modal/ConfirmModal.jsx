import React from 'react'

import styles from './ConfirmModal.module.scss'

export const ConfirmModal = ({ toggle, action, texts }) => (
  <div className={styles.wrapper}>
    <div className={styles.background}/>
    <div className={styles.card}>
      <span onClick={() => toggle(false)} className={styles.close}>&times;</span>
      <h3>{texts.modalTitle}</h3>
      <div>
        {texts.modalDescription}
      </div>
      <button onClick={action}>
          <span>
            {texts.modalButton}
          </span>
      </button>
    </div>
  </div>
)