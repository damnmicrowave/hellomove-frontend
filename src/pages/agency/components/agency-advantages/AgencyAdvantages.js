import React from 'react'
import './styles.scss'


export class AgencyAdvantages extends React.Component {
  render() {
    const { advantagesTitle } = this.props.texts
    const { features, features_headings } = this.props.agency
    return (
      <div className="agency-advantages">
        <div className="container">
          <h3>{advantagesTitle}</h3>
          {features.split('; ').map((item, index) => (
            <div key={index} className="row">
              <div className={index % 2 === 1 ? 'feature right' : 'feature'}>
                <span className="number">0{index + 1}</span>
                <h4>{features_headings.split('; ')[index]}</h4>
                <div className="desc">{item}</div>
              </div>
            </div>
          ))}
        </div>
      </div>
    )
  }
}
