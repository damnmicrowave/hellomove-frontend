import React from 'react'
import { Link } from 'react-router-dom'
import './styles.scss'
import arrowSVG from './media/arrow.svg'


export class Programs extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      programsCount: 4
    }
  }

  render() {
    const {
      programSubjectsTitle,
      programSubjectsDesc,
      showMore
    } = this.props.texts
    const { programs } = this.props.agency
    const { programsCount } = this.state
    return (
      <div className="programs">
        <div className="container">
          <h3>{programSubjectsTitle}</h3>
          <div className="desc" dangerouslySetInnerHTML={{ __html: programSubjectsDesc }}/>
          <div className="programs-list">
            {programs.map(({ id, name, desc }, index) => (
              index < programsCount &&
              <Link to={`/program/${id}`} key={index} className="program">
                <h4>{name}</h4>
                <div className="propgram-desc">{desc}</div>
                <div className="arrow-icon">
                  <img src={arrowSVG} alt="arrow"/>
                </div>
              </Link>
            ))}
          </div>
          {programsCount < programs.length &&
          <button onClick={() => {
            this.setState({ programsCount: programsCount + 3 })
          }} className="round-btn">
            <span>{showMore}</span>
          </button>
          }
        </div>
      </div>
    )
  }
}
