import React from 'react'
import './styles.scss'


export class AboutAgency extends React.Component {
  render() {
    let { aboutAgency } = this.props.texts
    const { full_description, secondary_picture } = this.props.agency
    return (
      <div className="about-agency">
        <div className="container">
          <div className="left"
               style={secondary_picture ? { backgroundImage: `url("https://storage.googleapis.com/hm-files/${secondary_picture}")` } : null}/>
          <div className="right">
            <div className="v-aligner">
              <h4>{aboutAgency}</h4>
              <div className="divider"/>
              <div className="desc" dangerouslySetInnerHTML={{ __html: full_description }}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}