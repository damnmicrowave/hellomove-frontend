import React from 'react'
import './styles.scss'


export class Services extends React.Component {
  render() {
    const { servicesTitle } = this.props.texts
    const { helpings } = this.props.agency
    const services = helpings.split('; ')
    const rowHeight = 100 / Math.ceil(services.length / 3)
    return (
      <div className="agency-services">
        <div className="container">
          <h3>{servicesTitle}</h3>
          <div className="steps-list">
            {[ ...Array(Math.ceil(services.length / 3)) ].map((_, index) => (
              <div key={index} className="row" style={{ height: `${rowHeight}%` }}>
                {services.slice(index * 3, index * 3 + 3).map((item, key) => (
                  <div key={key} className="step">
                    <span><div>{index * 3 + key + 1}</div></span>
                    <div className="desc">{item}</div>
                  </div>
                ))}
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
