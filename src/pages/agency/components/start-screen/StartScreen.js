import React from 'react'
import './styles.scss'
import starSVG from './media/star.svg'
import calendarSVG from './media/calendar.svg'


export class StartScreen extends React.Component {
  render() {
    let {
      timeExisting,
      rating
    } = this.props.texts
    const {
      name,
      description,
      rating: agencyRating,
      years
    } = this.props.agency
    return (
      <div className="start-screen">
        <div className="container">
          <div className="left">
            <h4>{name}</h4>
            <div className="desc">{description}</div>
          </div>
          <div className="right">
            <div className="v-aligner">
              <div className="row">
                <img src={calendarSVG} alt="calendar"/>
                <div className="info">
                  <span className="title">{timeExisting}</span>
                  <span className="value">{years}</span>
                </div>
              </div>
              <div className="row">
                <img src={starSVG} alt="star"/>
                <div className="info">
                  <span className="title">{rating}</span>
                  <div className="stars">
                    {[ ...Array(agencyRating).keys() ].map((_, index) => (
                      <React.Fragment key={index}>★</React.Fragment>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}