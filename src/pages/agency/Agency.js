import React from 'react'
import { apiRequest } from '../../utils'
import {
  FooterUpper,
  Loading
} from '../../includes'
import {
  StartScreen,
  AboutAgency,
  AgencyAdvantages,
  Consult,
  Services,
  Cashback,
  Programs,
  ConsultBottom,
  ContactAgency
} from './components'


export class Agency extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      showPopup: false,
      loaded: false,
      agency: {}
    }
  }

  componentDidMount() {
    const { loaded } = this.state
    if ( !loaded ) {
      apiRequest(response => {
        this.setState({
          agency: response.agency,
          loaded: true
        })
      }, `get_agency/${this.props.match.params.id}`)
    }
  }

  togglePopup() {
    const { showPopup } = this.state
    this.setState({ showPopup: !showPopup })
  }

  render() {
    const { texts } = this.props
    const {
      agency,
      loaded
    } = this.state
    return (
      <>
        {loaded ? <div className="wrapper-agency">
          {this.state.showPopup &&
          <ContactAgency toggle={this.togglePopup.bind(this)} agency={agency} texts={texts}/>
          }
          <StartScreen agency={agency} texts={texts}/>
          <AboutAgency agency={agency} texts={texts}/>
          <AgencyAdvantages agency={agency} texts={texts}/>
          <Consult toggle={this.togglePopup.bind(this)} texts={texts}/>
          <Services agency={agency} texts={texts}/>
          <Cashback toggle={this.togglePopup.bind(this)} texts={texts}/>
          <Programs agency={agency} texts={texts}/>
          <ConsultBottom toggle={this.togglePopup.bind(this)} texts={texts}/>
        </div> : <Loading/>}
        <FooterUpper texts={texts}/>
      </>
    )
  }
}