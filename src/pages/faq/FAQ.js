import React from 'react'
import { apiRequest } from '../../utils'
import { FooterUpper, Loading } from './../../includes'
import './styles.scss'
import minus from './media/minus.png'
import plus from './media/plus.png'

class Question extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      toggled: false
    }
  }

  render() {
    const { toggled } = this.state
    const {
      question,
      answer
    } = this.props
    return (
      <div onClick={() => {
        this.setState({ toggled: !toggled })
      }} className={`question${toggled ? ' active' : ''}`}>
        <img src={plus} className="plus" alt="plus"/>
        <img src={minus} className="minus" alt="minus"/>
        <div className="q">
          <div>{question}</div>
        </div>
        <div className="answer">{answer}</div>
      </div>
    )
  }
}

export class FAQ extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
      questions: []
    }
  }

  componentDidMount() {
    apiRequest(response => {
      const { faqs } = response
      this.setState({
        questions: faqs,
        loaded: true
      })
    }, `get_faqs/${this.props.lang}`)
  }

  render() {
    const { texts } = this.props
    const { title } = texts
    return (
      <>
        <div className="wrapper-faq">
          <div className="container">
            <h3>{title}</h3>
            {this.state.loaded ? this.state.questions.map((item, index) => (
                <Question key={index} {...item} />
              )) :
              <Loading/>
            }
          </div>
        </div>
        <FooterUpper texts={texts}/>
      </>
    )
  }
}
