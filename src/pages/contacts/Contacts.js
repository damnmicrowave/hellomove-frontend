import React from 'react'
import { Link } from 'react-router-dom'
import InputMask from 'react-input-mask'
import ym from 'react-yandex-metrika'
import { apiRequest } from '../../utils'
import { Loading, ThankYou } from '../../includes'
import './styles.scss'
import phoneSVG from './media/phone.svg'
import mailSVG from './media/mail.svg'


export class Contacts extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      showThanksPopup: false,
      name: '',
      emailAddress: '',
      phone: '',
      note: ''
    }
  }

  sendRequest() {
    const data = {
      ...this.state,
      page: window.location.pathname
    }
    this.setState({
      loading: true,
      name: '',
      phone: '',
      email: '',
      note: ''
    })
    apiRequest(() => {
      ym('reachGoal', 'contactUsPage')
      this.setState({
        loading: false,
        showThanksPopup: true
      })
      setTimeout(() => {
        this.setState({
          showThanksPopup: false
        })
      }, 5000)
    }, 'contact_us', { data: data, method: 'POST' })
  }

  validate() {
    const { name, phone, emailAddress } = this.state
    const emailRegex = /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/
    return name &&
      phone.trim().length > 8 &&
      emailRegex.test(emailAddress.trim())
  }

  render() {
    const { texts } = this.props
    const {
      title,
      formName,
      formPhone,
      formNote,
      send,
      agreement,
      privacyPolicy,
      number,
      email,
      _thankYouTitle,
      _thankYouMessage
    } = texts
    const {
      name,
      emailAddress,
      phone,
      note,
      loading,
      showThanksPopup
    } = this.state
    return (
      <>
        {showThanksPopup && <ThankYou title={_thankYouTitle} message={_thankYouMessage}/>}
        <div className="wrapper-contacts">
          <div className="container">
            <h3 dangerouslySetInnerHTML={{ __html: title }}/>
            <div className="wrapper">
              <div className="info-block">
                <div className="row">
                  <input value={name} className="active" placeholder={formName} type="text"
                         onChange={e => {
                           this.setState({ name: e.target.value })
                         }}/>
                  <input value={emailAddress} placeholder="e-mail" type="text"
                         onChange={e => {
                           this.setState({ emailAddress: e.target.value })
                         }}/>
                  <InputMask mask="+99999999999999" maskChar=" " {...{
                    name: 'footer_phone',
                    placeholder: formPhone,
                    value: phone,
                    autoComplete: 'off',
                    onChange: e => {
                      this.setState({ phone: e.target.value })
                    }
                  }}/>
                </div>
                <div className="row">
                                    <textarea value={note} placeholder={formNote} onChange={e => {
                                      this.setState({ note: e.target.value })
                                    }}/>
                </div>
              </div>
              <div className="info-block">
                <div className="contact-info">
                  <img src={mailSVG} alt="mail"/>
                  <span>{email}</span>
                </div>
                <div className="contact-info">
                  <img src={phoneSVG} alt="phone"/>
                  <span>{number}</span>
                </div>
              </div>
            </div>
            <div className="agreement" style={{ textAlign: 'left' }}>
              {agreement.replace('{{buttonText}}', send)}
            </div>
            <button onClick={this.sendRequest.bind(this)}
                    className={this.validate.bind(this)() ? 'round-btn thin' : 'round-btn thin disabled'}>
              <span>{loading ? <Loading color="white"/> : send}</span>
            </button>
            <Link to='/privacy_policy/'
                  style={{ verticalAlign: loading ? 'super' : 'middle' }}>{privacyPolicy}</Link>
          </div>
        </div>
      </>
    )
  }
}
