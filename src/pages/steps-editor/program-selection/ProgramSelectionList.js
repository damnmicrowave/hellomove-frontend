import React from 'react'
import { CheckBox } from '../../../includes'
import { getParents } from '../../../utils'

import styles from './ProgramSelectionList.module.scss'


export class ProgramSelectionList extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      searchQuery: ''
    }

    this.programsList = React.createRef()
  }

  componentDidMount() {
    window.onclick = e => {
      const { toggle } = this.props
      const nodeParents = getParents(e.target)
      if ( !nodeParents.includes(this.programsList.current) ) {
        toggle(false)
      }
    }
  }

  render() {
    const {
      expanded,
      toggle,
      programs,
      select,
      selected
    } = this.props
    const { searchQuery } = this.state
    return (
      <>
        <div ref={this.programsList} className={`${styles.listBox} ${expanded ? styles.expanded : ''}`}>
          <div className={styles.listInput}>
            <input
              onFocus={e => {
                e.target.select()
                toggle(true)
              }}
              onChange={e => {
                const { value } = e.target
                this.setState({
                  searchQuery: value
                })
              }}
              style={expanded ? { boxShadow: 'none' } : {}}
              value={searchQuery}
              type="text"
              placeholder='Search for program'
            />
          </div>
          <div className={styles.items}>
            {programs.map((name, index) => (
              name.toLowerCase().includes(searchQuery.toLowerCase()) &&
              <label onClick={e => {
                // noinspection JSUnresolvedVariable
                if ( e.target.nodeName === 'INPUT' ) {
                  select(name)
                  this.setState({ searchQuery: name })
                  toggle(false)
                }
              }} key={index} className={styles.elem}>
                <CheckBox backgroundColor='white' checked={selected === name}/>
                <span>{name}</span>
              </label>
            ))}
          </div>
        </div>
      </>
    )
  }
}