import React, { useEffect, useState } from 'react'
import RichTextEditor from 'react-rte'
import { apiRequest } from '../../utils'
import { Loading } from '../../includes'
import { ProgramSelectionList } from './program-selection/ProgramSelectionList'

import styles from './StepsEditor.module.scss'

const emptyStep = {
  number: 0,
  name: '',
  text: '',
  links: []
}

export const StepsEditor = () => {

  const [ listExpanded, setExpanded ] = useState(false)
  const [ programs, setPrograms ] = useState(false)
  useEffect(() => {
    apiRequest(({ programs }) => {
      setPrograms(programs)
    }, 'get_all_new_programs')
  }, [])

  const [ selectedProgram, setSelectedProgram ] = useState('')
  const [ steps, setSteps ] = useState([])
  const [ selectedStep, setSelectedStep ] = useState(emptyStep)
  const [ editorState, setEditorState ] = useState(RichTextEditor.createEmptyValue())
  const setEditorText = value => {
    setEditorState(RichTextEditor.createValueFromString(value, 'html'))
  }
  useEffect(() => {
    if ( selectedProgram ) {
      apiRequest(({ status, steps }) => {
        if ( status === 'error' ) {
          alert('You have to login first')
        } else {
          const { text } = steps[0]
          setEditorText(text)
          setSelectedStep(steps[0])
          setSteps(steps)
        }
      }, `program_steps/${selectedProgram}`)
    }
  }, [ selectedProgram ])

  const { name, links } = selectedStep
  const changeName = e => {
    setSelectedStep({ ...selectedStep, name: e.target.value })
  }
  const changeText = e => {
    setEditorState(e)
    setSelectedStep({ ...selectedStep, text: e.toString('html') })
  }
  const changeLinkText = (index, newValue) => {
    links[index].text = newValue
    setSelectedStep({ ...selectedStep, links })
  }
  const changeLinkUrl = (index, newValue) => {
    links[index].url = newValue
    setSelectedStep({ ...selectedStep, links })
  }
  const addNewLink = () => {
    setSelectedStep({ ...selectedStep, links: [ ...links, { text: '', url: '' } ] })
  }
  const removeLink = index => {
    links.splice(index, 1)
    setSelectedStep({ ...selectedStep, links })
  }

  const addNewStep = () => {
    setSteps([ ...steps, { ...emptyStep, number: steps.length } ])
  }
  const saveStep = () => {
    apiRequest(({ status }) => {
        alert(status)
      }, `program_steps/${selectedProgram}`,
      {
        method: 'POST',
        data: { step: selectedStep }
      }
    )
  }

  return (
    <div className={styles.wrapper}>
      {programs ?
        <div className={styles.wrapper}>
          <div className={styles.card}>
            <h3>Choose program</h3>
            <ProgramSelectionList
              expanded={listExpanded}
              toggle={setExpanded}
              programs={programs}
              selected={selectedProgram}
              select={programName => setSelectedProgram(programName)}
            />
            {Boolean(steps.length) &&
            <div className={styles.stepsEditor}>
              <h3>Choose step</h3>
              <div className={styles.stepSelector}>
                {steps.map((step, index) => (
                  <div
                    onClick={() => {
                      setSelectedStep(step)
                      setEditorText(step.text)
                    }}
                    key={index}
                    className={`${styles.step} ${selectedStep.number === index ? styles.active : ''}`}
                  >
                    <span>Step {index + 1}</span>
                  </div>
                ))}
                <button onClick={addNewStep} className={styles.step}>
                  <span>Add new step</span>
                </button>
              </div>
              <h3>
                Change step
                <button onClick={saveStep} className={styles.inlineButton}>
                  <span>Save</span>
                </button>
              </h3>
              <div className={styles.stepValues}>
                <div className={styles.row}>
                  <div className={styles.valueName}>Name:</div>
                  <div className={styles.valueContent}>
                    <input
                      className={styles.input}
                      type='text'
                      onChange={changeName}
                      value={name}
                    />
                  </div>
                </div>
                <div className={styles.valueName}>Text:</div>
                <div className={styles.valueContent}>
                  <RichTextEditor
                    className={styles.textEditor}
                    value={editorState}
                    onChange={changeText}
                  />
                </div>
                <div className={styles.valueName}>Links:</div>
                {links.map(({ text, url }, index) => (
                  <div key={index} className={styles.linkRow}>
                    <div className={styles.linkProp}>
                      <div className={styles.valueName}>Text:</div>
                      <div className={styles.valueContent}>
                        <input
                          className={styles.input}
                          type='text'
                          onChange={e => {
                            changeLinkText(index, e.target.value)
                          }}
                          value={text}
                        />
                      </div>
                    </div>
                    <div className={styles.linkProp}>
                      <div className={styles.valueName}>URL:</div>
                      <div className={styles.valueContent}>
                        <input
                          className={styles.input}
                          type='text'
                          onChange={e => changeLinkUrl(index, e.target.value)}
                          value={url}
                        />
                      </div>
                    </div>
                    <button onClick={() => removeLink(index)} className={styles.inlineButton}>
                      <span>Remove</span>
                    </button>
                  </div>
                ))}
                <button onClick={addNewLink} className={styles.addLink}>
                  <span>Add new link</span>
                </button>
              </div>
            </div>}
          </div>
        </div>
        : <Loading/>}
    </div>
  )
}