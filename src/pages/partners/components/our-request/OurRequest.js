import React from 'react'
import './styles.scss'


export class OurRequest extends React.Component {
  render() {
    const {
      whatWeWantText
    } = this.props.texts
    return (
      <div className="our-request">
        <div className="container">
          <div className="desc">{whatWeWantText}</div>
        </div>
      </div>
    )
  }
}
