import React from 'react'
import './styles.scss'
import applicationSVG from './media/application.svg'
import crownSVG from './media/crown.svg'
import groupSVG from './media/group.svg'

export class Benefits extends React.Component {
  render() {
    const {
      whatYouBenefitTitle,
      wybSubTitle,
      wybFreeApplications,
      wybRecognitionRaise,
      wybProfitableClients
    } = this.props.texts
    return (
      <div className="benefits">
        <div className="container">
          <h3>{whatYouBenefitTitle}</h3>
          <div className="info">
            {wybSubTitle}
          </div>
          <div className="list">
            <div className="item">
              <img style={{ paddingLeft: '5px' }} src={applicationSVG} alt="application"/>
              {wybFreeApplications}
            </div>
            <div className="item">
              <img style={{ padding: '0 10px' }} src={crownSVG} alt="crown"/>
              {wybRecognitionRaise}
            </div>
            <div className="item">
              <img src={groupSVG} alt="group"/>
              {wybProfitableClients}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
