import React from 'react'
import './styles.scss'


export class HowWeWork extends React.Component {
  render() {
    const {
      howWeWorkTitle,
      hwwItems,
      hwwBottomText
    } = this.props.texts
    return (
      <div className="how-we-work">
        <div className="container">
          <h3>{howWeWorkTitle}</h3>
          <div className="list">
            {hwwItems.split('; ').map((item, index) => (
              <div key={index} className="item">
                                <span>
                                    <div>{index + 1}</div>
                                </span>
                <div className="desc">{item}</div>
              </div>
            ))}
          </div>
          <div className="info">
            {hwwBottomText}
          </div>
        </div>
      </div>
    )
  }
}
