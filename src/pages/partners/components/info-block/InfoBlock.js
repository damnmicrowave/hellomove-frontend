import React from 'react'
import './styles.scss'


export class InfoBlock extends React.Component {
  render() {
    const {
      detailedInfoUpper,
      detailedInfoBottom
    } = this.props.texts
    return (
      <div className="info-block">
        <div className="container">
          <div className="desc">{detailedInfoUpper}</div>
          <div className="desc">{detailedInfoBottom}</div>
        </div>
      </div>
    )
  }
}
