import React from 'react'
import './styles.scss'
import starSVG from './media/star.svg'
import dialogSVG from './media/dialog.svg'
import moneySVG from './media/money.svg'

export class Plan extends React.Component {
  render() {
    const {
      workPlanTitle,
      wpSubTitle,
      wpItems
    } = this.props.texts
    const icons = [ starSVG, dialogSVG, moneySVG ]
    return (
      <div className="plan">
        <div className="container">
          <h3>{workPlanTitle}</h3>
          <div className="info">
            {wpSubTitle}
          </div>
          <div className="list">
            {wpItems.split('; ').map((item, index) => (
              <div key={index} className="item">
                <img src={icons[index]} alt=""/>
                {item}
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
