import React from 'react'
import { Link } from 'react-router-dom'
import './styles.scss'

export class Bottom extends React.Component {
  render() {
    const {
      leaveRequestTitle,
      leaveRequestButton
    } = this.props.texts
    return (
      <div className="bottom">
        <div className="container">
          <div className="desc">{leaveRequestTitle}</div>
          <Link to="/add_agency/" className="round-btn filled">
            <span>{leaveRequestButton}</span>
          </Link>
        </div>
      </div>
    )
  }
}