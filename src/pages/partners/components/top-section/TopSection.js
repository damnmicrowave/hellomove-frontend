import React from 'react'
import './styles.scss'


export class TopSection extends React.Component {
  render() {
    const {
      title,
      subTitle
    } = this.props.texts
    return (
      <div className="top-section">
        <div className="purpler"/>
        <div className="container">
          <h2>{title}</h2>
          <div className="desc">{subTitle}</div>
        </div>
      </div>
    )
  }
}
