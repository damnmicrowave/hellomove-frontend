import React from 'react'
import { FooterUpper } from '../../includes'
import {
  TopSection,
  HowWeWork,
  InfoBlock,
  Benefits,
  OurRequest,
  Plan,
  Bottom
} from './components'

export class Partners extends React.Component {
  render() {
    const { texts } = this.props
    return (
      <>
        <div className="wrapper-partners">
          <TopSection texts={texts}/>
          <HowWeWork texts={texts}/>
          <InfoBlock texts={texts}/>
          <Benefits texts={texts}/>
          <OurRequest texts={texts}/>
          <Plan texts={texts}/>
          <Bottom texts={texts}/>
        </div>
        <FooterUpper texts={texts}/>
      </>
    )
  }
}
