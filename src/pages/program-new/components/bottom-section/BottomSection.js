import React from 'react'
import './styles.scss'


export class BottomSection extends React.Component {
  render() {
    const {
      texts,
      toggle
    } = this.props
    const {
      bottomSectionTitle,
      bottomSectionDesc,
      bottomSectionButton,
      bottomSectionPopupTitle
    } = texts
    return (
      <div className="bottom-section">
        <div className="purpler"/>
        <div className="container">
          <h3>{bottomSectionTitle}</h3>
          <div className="desc">{bottomSectionDesc}</div>
          <button onClick={() => {
            toggle(bottomSectionPopupTitle)
          }} className="round-btn">
            <span>{bottomSectionButton}</span>
          </button>
        </div>
      </div>
    )
  }
}