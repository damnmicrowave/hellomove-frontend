import React from 'react'
import './styles.scss'


export class Consult extends React.Component {
  render() {
    const {
      texts,
      toggle,
      program
    } = this.props
    const {
      consultTitle,
      consultButtonLeft,
      consultButtonRight,
      consultPopupTitleBR
    } = texts
    const { law } = program
    return (
      <div className="consult">
        <div className="purpler"/>
        <div className="container">
          <h3>{consultTitle}</h3>
          <div className="buttons">
            <a href={law} target="_blank" rel="noopener noreferrer" className="round-btn">
              <span>{consultButtonLeft}</span>
            </a>
            <button onClick={() => {
              toggle(consultPopupTitleBR)
            }} className="round-btn filled">
              <span>{consultButtonRight}</span>
            </button>
          </div>
        </div>
      </div>
    )
  }
}