import React from 'react'
import './styles.scss'


export class StepsDetails extends React.Component {
  render() {
    const {
      texts,
      toggle,
      program
    } = this.props
    const {
      stepsDetailsTitle,
      stepsDetailsDesc,
      stepsDetailsButton,
      stepsDetailsPopupTitle
    } = texts
    const { action } = program
    return (
      <div className="steps-details">
        <div className="purpler"/>
        <div className="container">
          <h3>{stepsDetailsTitle}</h3>
          <div className="desc">{stepsDetailsDesc.replace('{{programAction}}', action)}</div>
          <button onClick={() => {
            toggle(stepsDetailsPopupTitle)
          }} className="round-btn">
            <span>{stepsDetailsButton}</span>
          </button>
        </div>
      </div>
    )
  }
}