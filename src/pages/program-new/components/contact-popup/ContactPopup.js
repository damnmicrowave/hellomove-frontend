import React from 'react'
import InputMask from 'react-input-mask'
import ym from 'react-yandex-metrika'
import { apiRequest } from '../../../../utils'
import { Link } from 'react-router-dom'
import {
  Loading,
  ThankYou
} from '../../../../includes'
import './styles.scss'


export class ContactPopup extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      showThanksPopup: false,
      name: '',
      phone: '',
      email: '',
      note: ''
    }
    this.popup = React.createRef()
  }

  sendRequest() {
    const data = {
      ...this.state,
      page: window.location.pathname
    }
    this.setState({
      name: '',
      phone: '',
      email: '',
      note: '',
      loading: true
    })
    apiRequest(() => {
      ym('reachGoal', 'contactAgency')
      this.props.toggle()
      this.setState({
        loading: false,
        showThanksPopup: true
      })
      setTimeout(() => {
        this.setState({
          showThanksPopup: false
        })
      }, 5000)
    }, 'contact_agency', { data: data, method: 'POST' })
  }

  validate() {
    const { name, phone, email } = this.state
    const emailRegex = /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/
    return name &&
      phone.trim().length > 8 &&
      emailRegex.test(email.trim())
  }

  render() {
    const {
      texts,
      title,
      toggle,
      programName
    } = this.props
    const {
      name,
      phone,
      email,
      note,
      loading,
      showThanksPopup
    } = this.state
    const {
      applicationSentTitle,
      applicationSentMessage,
      _name,
      _phone,
      _note,
      _agreement,
      sendApplicationButton,
      _privacyPolicy
    } = texts
    return (
      <>
        {showThanksPopup && <ThankYou title={applicationSentTitle} message={applicationSentMessage}/>}
        <div onClick={e => {
          if ( e.target === this.popup.current ) {
            toggle()
          }
        }} ref={this.popup} className="program-popup">
          <div className="form">
            <h3>{title.replace('{{programName}}', programName)}</h3>
            <span className="close" onClick={toggle}>×</span>
            <div className="fields">
              <input type="text" onChange={e => {
                this.setState({ name: e.target.value })
              }} value={name} placeholder={_name}/>
              <input type="text" onChange={e => {
                this.setState({ email: e.target.value })
              }} value={email} placeholder="e-mail"/>
              <InputMask mask="+9999999999999" maskChar=" " {...{
                value: phone,
                placeholder: _phone,
                onChange: e => {
                  this.setState({ phone: e.target.value })
                }
              }}/>
              <input type="text" onChange={e => {
                this.setState({ note: e.target.value })
              }} value={note} placeholder={_note}/>
            </div>
            <div className="agreement" style={{ textAlign: 'left' }}>
              {_agreement.replace('{{buttonText}}', sendApplicationButton)}
            </div>
            <button onClick={this.sendRequest.bind(this)}
                    className={`round-btn${!this.validate() ? ' disabled' : ''}`}>
              <span>{loading ? <Loading color="white"/> : sendApplicationButton}</span>
            </button>
            <Link to='/privacy_policy/'
                  style={{ verticalAlign: loading ? 'super' : 'middle' }}>
              {_privacyPolicy}
            </Link>
          </div>
        </div>
      </>
    )
  }
}
