import React from 'react'
import './styles.scss'
import starSVG from './media/star.svg'
import calendarSVG from './media/calendar.svg'
import clockSVG from './media/clock.svg'


export class StartScreen extends React.Component {
  render() {
    const {
      texts,
      program,
      toggle
    } = this.props
    let {
      startScreenTerms,
      startScreenPeriod,
      startScreenSimplicity,
      startScreenButtonLeft,
      startScreenButtonRight,
      startScreenPopupTitleBL,
      startScreenPopupTitleBR
    } = texts
    const {
      picture,
      name,
      short_description: description,
      terms,
      residence_period: residencePeriod,
      difficulty
    } = program
    return (
      <div className="start-screen"
           style={{ backgroundImage: `url("https://storage.googleapis.com/hm-files/${picture}")` }}>
        <div className="purpler"/>
        <div className="container">
          <div className="left">
            <h4>{name}</h4>
            <div className="desc">{description}</div>
          </div>
          <div className="right">
            <div className="v-aligner">
              <div className="row">
                <img src={clockSVG} alt="calendar"/>
                <div className="info">
                  <span className="title">{startScreenTerms}</span>
                  <span className="value">{terms}</span>
                </div>
              </div>
              <div className="row">
                <img src={calendarSVG} alt="calendar"/>
                <div className="info">
                  <span className="title">{startScreenPeriod}</span>
                  <span className="value">{residencePeriod}</span>
                </div>
              </div>
              <div className="row">
                <img src={starSVG} alt="star"/>
                <div className="info">
                  <span className="title">{startScreenSimplicity}</span>
                  <div className="stars">
                    {[ ...Array(difficulty).keys() ].map((_, index) => (
                      <React.Fragment key={index}>★</React.Fragment>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="buttons">
            <button onClick={() => {
              toggle(startScreenPopupTitleBL)
            }} className="round-btn filled">
              <span>{startScreenButtonLeft}</span>
            </button>
            <button onClick={() => {
              toggle(startScreenPopupTitleBR)
            }} className="round-btn">
              <span>{startScreenButtonRight}</span>
            </button>
          </div>
        </div>
      </div>
    )
  }
}