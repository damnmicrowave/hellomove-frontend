import React from 'react'
import './styles.scss'


export class Calculations extends React.Component {
  render() {
    const {
      texts,
      toggle,
      program
    } = this.props
    const {
      calculationsTitle,
      calculationsDesc,
      calculationsButton,
      calculationsPopupTitle
    } = texts
    const { action } = program
    return (
      <div className="calculations">
        <div className="purpler"/>
        <div className="container">
          <h3>{calculationsTitle}</h3>
          <div className="desc">{calculationsDesc.replace('{{programAction}}', action)}</div>
          <button onClick={() => {
            toggle(calculationsPopupTitle)
          }} className="round-btn">
            <span>{calculationsButton}</span>
          </button>
        </div>
      </div>
    )
  }
}