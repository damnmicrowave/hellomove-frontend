import React from 'react'
import './styles.scss'
import calendarSVG from './media/calendar.svg'


export class Steps extends React.Component {
  render() {
    const {
      stepsTitle,
      stepsSubtitle,
      stepsContentsTitle
    } = this.props.texts
    let {
      action,
      steps_headings,
      steps_terms,
      steps
    } = this.props.program
    steps = steps.split('#')
    steps_headings = steps_headings.split('#')
    return (
      <div data-heading={stepsContentsTitle} className="steps">
        <div className="container">
          <h3>{stepsTitle.replace('{{programAction}}', action)}</h3>
          <div className="steps-count">{steps.length} {stepsSubtitle}</div>
          {steps_terms.split('#').map((item, index) => (
            <div key={index} className="step">
              <div className="heading">
                <span className="number">{index < 9 ? `0${index + 1}` : index + 1}</span>
                <span className="name">{steps_headings[index]}</span>
              </div>
              {item && <div className="terms">
                <img src={calendarSVG} alt="calendar"/>
                <span>{item}</span>
              </div>}
              <div className="desc" dangerouslySetInnerHTML={{ __html: steps[index] }}/>
            </div>
          ))}
        </div>
      </div>
    )
  }
}