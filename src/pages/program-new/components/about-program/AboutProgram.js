import React from 'react'
import './styles.scss'


export class AboutProgram extends React.Component {
  render() {
    const { description } = this.props.program
    const { aboutProgramTitle } = this.props.texts
    return (
      <div data-heading={aboutProgramTitle} className="about-program">
        <div className="container">
          <h3>{aboutProgramTitle}</h3>
          <div className="desc" dangerouslySetInnerHTML={{ __html: description }}/>
        </div>
      </div>
    )
  }
}