import React from 'react'
import './styles.scss'


export class ProgramAdvantages extends React.Component {
  render() {
    const { advantagesTitle } = this.props.texts
    const {
      name,
      advantages_headings,
      advantages
    } = this.props.program
    return (
      <div data-heading={advantagesTitle.replace('{{programName}}', name)} className="program-advantages">
        <div className="container">
          <h3>{advantagesTitle.replace('{{programName}}', name)}</h3>
          {advantages.split('; ').map((item, index) => (
            <div key={index} className="row">
              <div className={index % 2 === 1 ? 'advantage right' : 'advantage'}>
                <span className="number">{index < 9 ? `0${index + 1}` : index + 1}</span>
                <h4>{advantages_headings.split('; ')[index]}</h4>
                <div className="desc">{item}</div>
              </div>
            </div>
          ))}
        </div>
      </div>
    )
  }
}
