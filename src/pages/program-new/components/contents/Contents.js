import React from 'react'
import './styles.scss'
import arrowSVG from './media/arrow.svg'

export class Contents extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      show: false,
      blocks: []
    }

    this.contentsPage = React.createRef()
    this.blocks = React.createRef()
  }

  componentDidMount() {
    const nodes = this.contentsPage.current.parentNode.childNodes
    this.setState({
      blocks: Array.from(nodes).map(n => (
        n.dataset.heading ? { heading: n.dataset.heading, height: n.offsetTop } : null
      )).filter(n => n)
    })
  }

  render() {
    const {
      show,
      blocks
    } = this.state
    const { contentsTitle } = this.props.texts
    return (
      <div ref={this.contentsPage} className="contents">
        <div className="container">
          <h3 className={show ? 'active' : null} onClick={() => {
            this.setState({ show: !show })
          }}>
            {contentsTitle}
            <img src={arrowSVG} alt="arrow"/>
          </h3>
          <div ref={this.blocks} className="blocks">
            {blocks.map(({ heading, height }, index) => (
              <div key={index} onClick={() => {
                window.scrollTo(0, height + this.blocks.current.scrollHeight - 60)
              }} className="block">
                <div className="heading">{heading}</div>
                <div className="arrow-icon">
                  <img src={arrowSVG} alt="arrow"/>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}