import React from 'react'
import { NavLink } from 'react-router-dom'
import './styles.scss'
import arrowSVG from './media/arrow.svg'


export class ProgramsGroup extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      showPrograms: window.innerWidth > 640
    }
  }

  render() {
    const {
      id: programId,
      programs
    } = this.props.program
    const {
      showPrograms
    } = this.state
    return (
      <div className="programs-group">
        <div className="container">
          <h3 onClick={() => {
            this.setState({ showPrograms: !showPrograms })
          }} className={showPrograms ? 'active' : ''}>
            {this.props.texts.otherPrograms}
            <img src={arrowSVG} alt="arrow"/>
          </h3>
          {showPrograms && <div className="programs">
            {programs.map((item, index) => {
              const { id, slug, name } = item
              return <NavLink key={index} to={{
                pathname: `/p/${slug}/`,
                state: { program: { ...item, programs: programs } }
              }} className={id === programId ? 'filled' : ''}>
                <span>{name}</span>
              </NavLink>
            })}
          </div>}
        </div>
      </div>
    )
  }
}