import React from 'react'
import './styles.scss'
import chackmarkSVG from './media/checkmark.svg'


export class Requirements extends React.Component {
  render() {
    const {
      requirementsTitle,
      requirementsContentsTitle
    } = this.props.texts
    let {
      action,
      requirements_headings,
      requirements
    } = this.props.program
    requirements = requirements.split('; ')
    return (
      <div data-heading={requirementsContentsTitle} className="requirements">
        <div className="container">
          <h3>{requirementsTitle.replace('{{programAction}}', action)}</h3>
          {requirements_headings.split('; ').map((item, index) => (
            <div key={index} className="requirement">
              <div className="heading">
                <img src={chackmarkSVG} alt="checkmark"/>
                <span>{item}</span>
              </div>
              <div className="desc" dangerouslySetInnerHTML={{ __html: requirements[index] }}/>
            </div>
          ))}
        </div>
      </div>
    )
  }
}