import React from 'react'
import './styles.scss'
import minus from './media/minus.png'
import plus from './media/plus.png'

class Question extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      toggled: false
    }
  }

  render() {
    const { toggled } = this.state
    const {
      question,
      answer
    } = this.props
    return (
      <div onClick={() => {
        this.setState({ toggled: !toggled })
      }} className={`question${toggled ? ' active' : ''}`}>
        <img src={plus} className="plus" alt="plus"/>
        <img src={minus} className="minus" alt="minus"/>
        <div className="q">
          <div>{question}</div>
        </div>
        <div className="answer">{answer}</div>
      </div>
    )
  }
}

export class FAQ extends React.Component {
  render() {
    const {
      faqTitle,
      faqContentsTitle
    } = this.props.texts
    let {
      faq_headings,
      faq
    } = this.props.program
    faq = faq.split('; ')
    return (
      <div data-heading={faqContentsTitle} className="faq">
        <div className="container">
          <h3>{faqTitle}</h3>
          {faq_headings.split('; ').map((item, index) => (
            <Question key={index} question={item} answer={faq[index]}/>
          ))}
        </div>
      </div>
    )
  }
}
