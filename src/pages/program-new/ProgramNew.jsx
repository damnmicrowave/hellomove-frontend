import React from 'react'
import { apiRequest } from '../../utils'
import { FooterUpper, Loading } from '../../includes'
import {
  ContactPopup,
  StartScreen,
  ProgramsGroup,
  AboutProgram,
  Consult,
  ProgramAdvantages,
  Requirements,
  Calculations,
  Steps,
  StepsDetails,
  Articles,
  FAQ,
  BottomSection, Contents
} from './components'

export class ProgramNew extends React.Component {
  constructor(props) {
    super(props)
    const { state } = props.location
    this.state = {
      showPopup: false,
      popupTitle: '',
      loaded: Boolean(state),
      program: state ? state.program : {}
    }
    this.programPage = React.createRef()
  }

  componentDidMount() {
    const { loaded } = this.state
    const { programSlug } = this.props.match.params
    if ( !loaded ) {
      apiRequest(response => {
        this.setState({
          program: response.program,
          loaded: true
        })
      }, `get_new_program/${programSlug}`)
    }
  }

  togglePopup(title = '') {
    const { showPopup } = this.state
    this.setState({
      popupTitle: title,
      showPopup: !showPopup
    })
  }

  render() {
    const { texts } = this.props
    const {
      loaded,
      showPopup,
      popupTitle,
      program
    } = this.state
    const toggle = this.togglePopup.bind(this)
    return (
      <>
        {loaded ? <div ref={this.programPage} className="wrapper-program-new">
          {showPopup &&
          <ContactPopup programName={program.name} toggle={toggle} title={popupTitle} texts={texts}/>
          }
          <StartScreen toggle={toggle} program={program} texts={texts}/>
          <ProgramsGroup texts={texts} program={program}/>
          <Contents texts={texts}/>
          <AboutProgram program={program} texts={texts}/>
          <Consult program={program} toggle={toggle} texts={texts}/>
          <ProgramAdvantages program={program} texts={texts}/>
          <Requirements program={program} texts={texts}/>
          <Calculations toggle={toggle} program={program} texts={texts}/>
          <Steps program={program} texts={texts}/>
          <StepsDetails toggle={toggle} program={program} texts={texts}/>
          <Articles texts={texts}/>
          <FAQ program={program} texts={texts}/>
          <BottomSection toggle={toggle} texts={texts}/>
        </div> : <Loading/>}
        <FooterUpper texts={texts}/>
      </>
    )
  }
}
