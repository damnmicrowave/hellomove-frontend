import React from 'react'
import {
  TopSection,
  WhatProblemsSolve,
  WhatWeDo,
  HowItWorks,
  ForWhom,
  CalculatePercentage,
  Articles,
  Stories,
  Team,
  BottomSection
} from './components'


export class HomePage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
      questions: []
    }
  }

  render() {
    const { texts } = this.props
    return (
      <>
        <div className="wrapper-homepage">
          <TopSection texts={texts}/>
          <HowItWorks texts={texts}/>
          <WhatProblemsSolve texts={texts}/>
          <WhatWeDo texts={texts}/>
          <ForWhom texts={texts}/>
          <CalculatePercentage texts={texts}/>
          <Articles texts={texts}/>
          <Stories texts={texts}/>
          <Team texts={texts}/>
          <BottomSection texts={texts}/>
        </div>
      </>
    )
  }
}
