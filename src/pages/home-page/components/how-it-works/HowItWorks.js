import React from 'react'
import { Link } from 'react-router-dom'
import './styles.scss'
import targetSVG from './media/target.svg'
import gearSVG from './media/gear.svg'
import listSVG from './media/list.svg'
import walkingSVG from './media/walking.svg'
import arrowSVG from './media/arrow.svg'

export class HowItWorks extends React.Component {
  render() {
    const {
      howItWorksTitle,
      hiwItems,
      hiwButtonLeft,
      hiwButtonRight
    } = this.props.texts
    const icons = [ targetSVG, listSVG, gearSVG, walkingSVG ]
    return (
      <div className="how-it-works">
        <div className="container">
          <h3>{howItWorksTitle}</h3>
          <div className="row">
            {hiwItems.split('; ').map((item, index) => (
              <Link to="/questionnaire/business/" key={index} className="block">
                <img className="icon" src={icons[index]} alt="target"/>
                <div className="info-container">
                  <div className="info" dangerouslySetInnerHTML={{ __html: item }}/>
                </div>
                <div className="bottom">
                  <div className="number">
                    0{index + 1}
                  </div>
                  {index !== 3 && <img src={arrowSVG} alt="arrow" className="arrow"/>}
                </div>
              </Link>
            ))}
          </div>
          <div className="row">
            <div className="buttons">
              <a onClick={
                () => document.getElementById('contactUs').click()
              } to="/questionnaire/business/" className="round-btn">
                <span>{hiwButtonLeft}</span>
              </a>
              <Link to="/partners/" className="round-btn">
                <span>{hiwButtonRight}</span>
              </Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}