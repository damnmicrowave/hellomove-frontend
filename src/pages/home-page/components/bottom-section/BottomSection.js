import React from 'react'
import './styles.scss'
import { Link } from 'react-router-dom'

export class BottomSection extends React.Component {
  render() {
    const {
      bottomSectionText,
      bottomSectionButton
    } = this.props.texts
    return (
      <div className="bottom-section">
        <div className="purpler"/>
        <div className="container">
          <div className="desc">
            {bottomSectionText}
          </div>
          <Link className="round-btn" to="/questionnaire/business">
            <span>{bottomSectionButton}</span>
          </Link>
        </div>
      </div>
    )
  }
}