import React from 'react'
import './styles.scss'


export class WhatProblemsSolve extends React.Component {
  render() {
    const {
      problemsTitle,
      problemsUser,
      lookingForAdvisor,
      weCanSolveProblems,
      iNeedAgency,
      weHaveAgencies,
      lookingForProgram,
      weCanPickProgram,
      lowBudget,
      weHaveInstructions
    } = this.props.texts
    const rows = [
      {
        question: lookingForAdvisor,
        answer: weCanSolveProblems
      },
      {
        question: iNeedAgency,
        answer: weHaveAgencies
      },
      {
        question: lookingForProgram,
        answer: weCanPickProgram
      },
      {
        question: lowBudget,
        answer: weHaveInstructions
      }
    ]
    return (
      <div className="what-problems-solve">
        <div className="container">
          <h3>
            {problemsTitle}
          </h3>
          <div className="column">
            {rows.map(({ question, answer }, index) => (
              <div key={index} className="row">
                <div className="bubble right">
                  <div className="heading">
                    <span className="pic"/>
                    <span className="name">
                                        {problemsUser}
                                    </span>
                  </div>
                  <div className="desc">
                    {question}
                  </div>
                </div>
                <div className="bubble left">
                  <div className="heading">
                    <span className="pic"/>
                    <span className="name">
                                        HelloMove
                                    </span>
                  </div>
                  <div className="desc">
                    {answer}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
