import React from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import './styles.scss'

export class Stories extends React.Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    }
    const {
      successStoriesTitle
    } = this.props.texts
    return (
      <div className="stories">
        <div className="container">
          <h3>{successStoriesTitle}</h3>
          <div className="frame">
            <Slider {...settings}>
              <div>
                <div className="video-box">
                  <div className="video">
                    <iframe title="yt1" className="youtube"
                            src="https://www.youtube-nocookie.com/embed/f1J63X2KJ6g" frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen/>
                  </div>
                </div>
              </div>
              {/*<div>*/}
              {/*    <div className="video-box">*/}
              {/*        <div className="video">*/}
              {/*            <iframe title="yt2" className="youtube"*/}
              {/*                    src="https://www.youtube-nocookie.com/embed/dQw4w9WgXcQ" frameBorder="0"*/}
              {/*                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"*/}
              {/*                    allowFullScreen/>*/}
              {/*        </div>*/}
              {/*    </div>*/}
              {/*</div>*/}
            </Slider>
          </div>
        </div>
      </div>
    )
  }
}