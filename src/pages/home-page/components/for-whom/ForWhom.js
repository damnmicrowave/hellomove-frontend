import React from 'react'
import './styles.scss'
import busyPNG from './media/busy.jpg'
import booksPNG from './media/books.jpg'
import studyPNG from './media/study.jpg'
import movePNG from './media/move.png'
import { Link } from 'react-router-dom'

export class ForWhom extends React.Component {
  render() {
    const {
      forWhomTitle,
      fwItems
    } = this.props.texts
    const pics = {
      '/investments/': busyPNG,
      '/edu/': booksPNG,
      '/business/': studyPNG,
      '/exchange/': movePNG
    }
    return (
      <div className="for-whom">
        <div className="container">
          <h3>{forWhomTitle}</h3>
          <div className="row">
            {Object.keys(pics).map((item, index) => (
              <Link to={`/questionnaire${item}`} key={index} className="block">
                <div className="img" style={{ backgroundImage: `url("${pics[item]}")` }}/>
                <div className="desc">
                  {fwItems.split('; ')[index]}
                </div>
              </Link>
            ))}
          </div>
        </div>
      </div>
    )
  }
}