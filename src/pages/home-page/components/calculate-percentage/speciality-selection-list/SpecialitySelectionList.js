import React from 'react'
import './styles.scss'
import arrowSVG from './media/arrow.svg'
import { CheckBox } from '../../../../../includes'
import { getParents } from '../../../../../utils'

export class SpecialitySelectionList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      expanded: false
    }

    this.specialityList = React.createRef()
  }

  componentDidMount() {
    window.addEventListener('click', e => {
      const { expanded } = this.state
      if ( !getParents(e.target).includes(this.specialityList.current) && expanded ) {
        this.setState({ expanded: !expanded })
      }
    })
  }

  render() {
    const {
      cpChoose,
      cpSpecialities
    } = this.props.texts
    const {
      selectedItem,
      select
    } = this.props
    return (
      <div ref={this.specialityList} className={`speciality-selection-list${this.state.expanded ? ' active' : ''}`}>
        <button onClick={() => {
          this.setState({ expanded: !this.state.expanded })
        }}>
          <div className="arrow">
            <img src={arrowSVG} alt="arrow"/>
          </div>
          {!selectedItem ? cpChoose : selectedItem}
        </button>
        <div className="list">
          {cpSpecialities.split('; ').map((item, index) => (
            <label key={index}>
              <CheckBox toggle={() => {
                select(item)
              }} checked={selectedItem === item}/>
              <span>{item}</span>
            </label>
          ))}
        </div>
      </div>
    )
  }
}