import React from 'react'
import CountUp from 'react-countup'
import { Link } from 'react-router-dom'
import { SpecialitySelectionList } from './speciality-selection-list/SpecialitySelectionList'
import { CheckBox } from '../../../../includes'
import { removeElement } from '../../../../utils'
import './styles.scss'


export class CalculatePercentage extends React.Component {
  constructor(props) {
    super(props)

    const { cpQuestions } = props.texts
    const questions = cpQuestions.split('; ')
    this.state = {
      checkedItems: [],
      uncheckedItems: [],
      selectedSpeciality: '',
      pastFloorPercentage: 0,
      pastCeilPercentage: 0,
      questionsInfo: [
        {
          question: questions[0],
          yes: 17,
          no: 16
        },
        {
          question: questions[1],
          yes: -10,
          no: 13
        },
        {
          question: questions[2],
          yes: 21,
          no: 15
        },
        {
          question: questions[3],
          yes: 20,
          no: 21
        },
        {
          question: questions[4],
          yes: 20,
          no: 15
        }
      ]
    }
  }

  calculatePercentage() {
    const {
      checkedItems,
      uncheckedItems,
      questionsInfo,
      selectedSpeciality
    } = this.state
    const { cpSpecialities } = this.props.texts
    return 4 + questionsInfo.map(({ question, yes, no }, index) => {
      if ( checkedItems.includes(question) ) return yes
      if ( uncheckedItems.includes(question) ) return no
      if ( index === 4 && selectedSpeciality ) {
        return selectedSpeciality === cpSpecialities.split('; ')[4] ? no : yes
      }
      return 0
    }).reduce((a, b) => (a + b))
  }

  addQuestion(item) {
    const {
      checkedItems,
      uncheckedItems
    } = this.state
    const percentage = this.calculatePercentage()
    this.setState({
      pastFloorPercentage: percentage - 4,
      pastCeilPercentage: percentage + 4,
      checkedItems: !checkedItems.includes(item) ? [ ...checkedItems, item ] : checkedItems,
      uncheckedItems: uncheckedItems.includes(item) ? [ ...removeElement(uncheckedItems, item) ] : uncheckedItems
    })
  }

  removeQuestion(item) {
    const {
      checkedItems,
      uncheckedItems
    } = this.state
    const percentage = this.calculatePercentage()
    this.setState({
      pastFloorPercentage: percentage - 4,
      pastCeilPercentage: percentage + 4,
      checkedItems: checkedItems.includes(item) ? [ ...removeElement(checkedItems, item) ] : checkedItems,
      uncheckedItems: !uncheckedItems.includes(item) ? [ ...uncheckedItems, item ] : uncheckedItems
    })
  }

  chooseSpeciality(item) {
    const percentage = this.calculatePercentage()
    this.setState({
      pastFloorPercentage: percentage - 4,
      pastCeilPercentage: percentage + 4,
      selectedSpeciality: item
    })
  }

  render() {
    const {
      calculatePercentageTitle,
      cpYes,
      cpNo,
      cpProbability,
      cpButton
    } = this.props.texts
    const {
      pastFloorPercentage,
      pastCeilPercentage,
      checkedItems,
      uncheckedItems,
      selectedSpeciality,
      questionsInfo
    } = this.state
    const percentage = this.calculatePercentage()
    return (
      <div className="calculate-percentage">
        <div className="container">
          <h3>{calculatePercentageTitle}</h3>
          <div className="group">
            <div className="questions">
              {questionsInfo.map(({ question }, index) => (
                <div key={index} className="question">
                  {index + 1}. {question}
                </div>
              ))}
            </div>
            <div className="answers">
              {questionsInfo.map(({ question }, index) => (
                <div className="answer" key={index}>
                  {index !== 4 ?
                    <>
                      <label className="checkbox-label">
                        <CheckBox toggle={() => {
                          this.addQuestion.bind(this)(question)
                        }} checked={checkedItems.includes(question)}/>
                        <span>{cpYes}</span>
                      </label>
                      <label className="checkbox-label right">
                        <CheckBox toggle={() => {
                          this.removeQuestion.bind(this)(question)
                        }} checked={uncheckedItems.includes(question)}/>
                        <span>{cpNo}</span>
                      </label>
                    </> :
                    <SpecialitySelectionList
                      select={this.chooseSpeciality.bind(this)}
                      selectedItem={selectedSpeciality}
                      allItems={questionsInfo[4].question.split('; ')}
                      texts={this.props.texts}
                    />
                  }
                </div>
              ))}
            </div>
            <div className="result">
              <div className="info">
                <div className="mini-title">
                  {cpProbability}
                </div>
                <div className="percentage">
                  {percentage - 4 <= 0 ? 0 :
                    <>
                      <CountUp duration={Math.random() * 5 + 1} start={pastFloorPercentage} end={percentage - 4}/>
                      -
                      <CountUp duration={Math.random() * 5 + 1} start={pastCeilPercentage} end={percentage + 4}/>
                    </>
                  }%
                </div>
              </div>
              <Link to="/questionnaire/business/" className="round-btn">
                <span>{cpButton}</span>
              </Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}