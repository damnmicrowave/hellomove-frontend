import React from 'react'
import { Link } from 'react-router-dom'
import './styles.scss'
import instructionJPG from './media/instruction.jpg'
import agenciesPNG from './media/agencies.png'

export class WhatWeDo extends React.Component {
  render() {
    const {
      whatWeDoTitle,
      wwdInstructionsHeading,
      wwdInstructionsDesc,
      wwdInstructionsMiniTitle,
      wwdInstructionsList,
      wwdAgenciesHeading,
      wwdAgenciesDesc,
      wwdAgenciesMiniTitle,
      wwdAgenciesList,
      wwdBottomLeftButton,
      wwdBottomRightButton
    } = this.props.texts
    const infoListLeft = wwdInstructionsList.split('; ')
    const infoListRight = wwdAgenciesList.split('; ')
    return (
      <div className="what-we-do">
        <div className="container">
          <h3>
            {whatWeDoTitle}
          </h3>
          <div className="group">
            <div className="block left">
              <div className="pic" style={{ backgroundImage: `url("${instructionJPG}")` }}/>
              <div className="heading" dangerouslySetInnerHTML={{ __html: wwdInstructionsHeading }}/>
              <div className="desc">
                {wwdInstructionsDesc}
              </div>
              <h4>
                {wwdInstructionsMiniTitle}
              </h4>
              <div className="info-list">
                {infoListLeft.map((item, index) => (
                  <div key={index} className="item">
                                        <span className="number">
                                            0{index + 1}
                                        </span>
                    <span className="text">
                                            {item}
                                        </span>
                  </div>
                ))}
              </div>
              <Link to="/questionnaire/business/" className="round-btn thin">
                                <span>
                                    {wwdBottomLeftButton}
                                </span>
              </Link>
            </div>
            <div className="block right">
              <div className="pic" style={{ backgroundImage: `url("${agenciesPNG}")` }}/>
              <div className="heading" dangerouslySetInnerHTML={{ __html: wwdAgenciesHeading }}/>
              <div className="desc">
                {wwdAgenciesDesc}
              </div>
              <h4>
                {wwdAgenciesMiniTitle}
              </h4>
              <div className="info-list">
                {infoListRight.map((item, index) => (
                  <div key={index} className="item">
                                        <span className="number">
                                            0{index + 1}
                                        </span>
                    <span>
                                            {item}
                                        </span>
                  </div>
                ))}
              </div>
              <Link to="/questionnaire/business/" className="round-btn thin">
                                <span>
                                    {wwdBottomRightButton}
                                </span>
              </Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
