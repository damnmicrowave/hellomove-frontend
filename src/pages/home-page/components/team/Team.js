import React from 'react'
import './styles.scss'
import vkSVG from './media/vk.svg'
import instagramSVG from './media/instagram.svg'
import facebookSVG from './media/facebook.svg'
import youtubeSVG from './media/youtube.svg'

export class Team extends React.Component {
  render() {
    const {
      ourTeamTitle,
      otLeftHeading,
      otLeftDesc,
      otRightHeading,
      otRightDesc
    } = this.props.texts
    return (
      <div className="team">
        <div className="container">
          <h3>{ourTeamTitle}</h3>
          <div className="group">
            <div className="block">
              <div className="bg"/>
              <div className="purpler"/>
              <div className="info">
                <h4>{otLeftHeading}</h4>
                <div className="desc">
                  {otLeftDesc}
                </div>
              </div>
              <div className="icons">
                <a href="https://vk.com/vilenskiyvk" target="_blank" rel="noopener noreferrer" className="icon">
                  <img src={vkSVG} alt="vk"/>
                </a>
                <a href="https://www.youtube.com/channel/UCL73o2Ud9AaU9SnIdgJzVSQ/videos" target="_blank"
                   rel="noopener noreferrer" className="icon">
                  <img src={youtubeSVG} alt="youtube"/>
                </a>
                <a href="https://instagram.com/vilenskiy" target="_blank" rel="noopener noreferrer" className="icon">
                  <img src={instagramSVG} alt="instagram"/>
                </a>
              </div>
            </div>
            <div className="block">
              <div className="bg"/>
              <div className="purpler"/>
              <div className="info">
                <h4>{otRightHeading}</h4>
                <div className="desc">
                  {otRightDesc}
                </div>
              </div>
              <div className="icons">
                <a href="https://www.facebook.com/natasha.semina.3" target="_blank" rel="noopener noreferrer"
                   className="icon">
                  <img src={facebookSVG} alt="youtube"/>
                </a>
                <a href="https://instagram.com/nvsem" target="_blank" rel="noopener noreferrer" className="icon">
                  <img src={instagramSVG} alt="instagram"/>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}