import React from 'react'
import { Link } from 'react-router-dom'
import './styles.scss'
import pcPNG from './media/pc.png'


export class TopSection extends React.Component {
  render() {
    const {
      topSectionTitle,
      topSectionDesc,
      topSectionButton
    } = this.props.texts
    return (
      <div className="top-section">
        <div className="container">
          <div className="group">
            <div className="left">
              <h2 dangerouslySetInnerHTML={{ __html: topSectionTitle }}/>
              <div className="payback">
                {topSectionDesc}
              </div>
              <div className="button">
                {/*<Link to="/questionnaire/business/" className="round-btn">*/}
                {/*  <span>{topSectionButton}</span>*/}
                {/*</Link>*/}
                <button onClick={() => {
                  document.querySelector('#contactUs').click()
                }} className="round-btn">
                  <span>{topSectionButton}</span>
                </button>
              </div>
            </div>
            <div className="right">
              <img src={pcPNG} alt="pc"/>
            </div>
          </div>
          <div className="group-mobile">
            <h2 dangerouslySetInnerHTML={{ __html: topSectionTitle }}/>
            <img src={pcPNG} alt="pc"/>
            <div className="payback">
              {topSectionDesc}
            </div>
            {/*<Link to="/questionnaire/business/" className="round-btn">*/}
            {/*  <span>{topSectionButton}</span>*/}
            {/*</Link>*/}
            <button onClick={() => {
              document.querySelector('#contactUs').click()
            }} className="round-btn">
              <span>{topSectionButton}</span>
            </button>
          </div>
        </div>
      </div>
    )
  }
}
