import React from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import './styles.scss'
import meduzaPNG from './media/meduza.png'
import wayxarPNG from './media/wayxar.png'
import husslePNG from './media/hussle.png'
import investForsightPNG from './media/invest-forsight.png'
import vcruPNG from './media/vcru.png'
import zarubejomPNG from './media/zarubejom.png'


export class Articles extends React.Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 960,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 640,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    }
    const {
      articlesAboutUsTitle
    } = this.props.texts
    const articles = [
      {
        color: 'black',
        img: meduzaPNG,
        href: 'https://meduza.io/feature/2020/03/13/dnem-rabotal-na-dyadyu-a-nochyu-na-sebya?fbclid=IwAR0qLLmyaGprLNAX_fs3bjYP5oxUktRiARAc4W3PCwC9GZRb6LMgHOzXjEQ'
      },
      {
        img: wayxarPNG,
        href: 'https://wall.wayxar.ru/startups/kak_zapustit_startap_za_rubezhom_opyt_osnovateley_proekta_hello_move'
      },
      {
        img: husslePNG,
        href: 'https://hussle.ru/kak-v-19-let-zapustit-evropejskij-startap-pomogayushhij-migrantam-iz-rossii/'
      },
      {
        img: investForsightPNG,
        href: 'https://www.if24.ru/rossijskij-startap-menyaet-mirovoj-rynok-migratsionnyh-uslug/'
      },
      {
        img: vcruPNG,
        href: 'https://vc.ru/migrate/120319-keys-kak-poluchit-vnzh-britanii-onlayn-bez-vlozheniy-vo-vremya-karantina'
      },
      {
        color: '#ce0020',
        img: zarubejomPNG,
        href: 'https://www.zarubejom.ru/news/53724.html'
      }
    ]
    return (
      <div className="articles">
        <div className="container">
          <h3>{articlesAboutUsTitle}</h3>
          <div className="frame">
            <Slider {...settings}>
              {articles.map(({ img, href, color }, index) => (
                <div key={index}>
                  <a href={href} target="_blank" rel="noopener noreferrer"
                     className="img-box">
                    <div className="img" style={color ? { backgroundColor: color } : null}>
                      <img src={img} alt="link to the article"/>
                    </div>
                  </a>
                </div>
              ))}
            </Slider>
          </div>
        </div>
      </div>
    )
  }
}