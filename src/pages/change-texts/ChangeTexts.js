import React from 'react'
import { apiRequest, removeElement } from '../../utils'
import { FooterUpper, Loading } from '../../includes'
import './styles.scss'


export class ChangeTexts extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
      responseMessage: '',
      lang: props.lang,
      texts: {},
      newSectionName: '',
      expandedSections: []
    }
  }

  componentDidMount() {
    apiRequest(response => {
      this.setState({
        loaded: true,
        texts: response
      })
    }, `get_all_texts`)
  }

  addSection() {
    const {
      lang,
      texts,
      newSectionName
    } = this.state
    if ( newSectionName ) {
      this.setState({
        newSectionName: '',
        texts: { ...texts, ...{ [lang]: { ...texts[lang], ...{ [newSectionName]: {} } } } }
      })
    }
  }

  addVariable(sectionName) {
    const {
      lang,
      texts
    } = this.state
    this.setState({
      texts: { ...texts, ...{ [lang]: { ...texts[lang], ...{ [sectionName]: { ...texts[lang][sectionName], ...{ 'newVariable': '' } } } } } }
    })
  }

  changeText(sectionName, variableName, newValue) {
    const {
      lang,
      texts
    } = this.state
    let newState = {
      texts: texts
    }
    newState.texts[lang][sectionName][variableName] = newValue
    this.setState(newState)
  }

  changeVariable(sectionName, oldName, newName) {
    const {
      lang,
      texts
    } = this.state
    let newState = {
      texts: texts
    }
    const variableValue = newState.texts[lang][sectionName][oldName]
    delete newState.texts[lang][sectionName][oldName]
    if ( newName !== '<delete>' ) {
      newState.texts[lang][sectionName][newName] = variableValue
    }
    this.setState(newState)
  }

  changeSection(oldName, newName) {
    const {
      lang,
      texts
    } = this.state
    let newState = {
      texts: texts
    }
    const sectionData = newState.texts[lang][oldName]
    delete newState.texts[lang][oldName]
    if ( newName !== '<delete>' ) {
      newState.texts[lang][newName] = sectionData
    }
    this.setState(newState)
  }

  save() {
    apiRequest(response => {
      this.setState({
        responseMessage: response.message
      })
      setTimeout(() => {
        this.setState({
          responseMessage: ''
        })
      }, 5000)
    }, 'change_texts', { data: this.state.texts, method: 'POST' })
  }

  render() {
    const { texts: pageTexts } = this.props
    const {
      title,
      addSection,
      save,
      addVariable
    } = pageTexts
    const {
      loaded,
      responseMessage,
      lang,
      texts,
      newSectionName,
      expandedSections
    } = this.state
    let langs
    let sections
    let localTexts
    if ( loaded ) {
      localTexts = texts[lang]
      sections = Object.keys(localTexts)
      langs = Object.keys(texts).filter(l => l !== 'fromYandexAds')
    }
    return (
      <>
        <div className="wrapper-change-texts">
          <div className="container">
            {loaded ?
              <>
                <h3>{title}</h3>
                <div className="top-menu">
                  <div className="lang-choice">
                    {langs.map((item, index) => (
                      <button onClick={() => {
                        this.setState({ lang: item })
                      }} className={item === lang ? 'active' : null} key={index}>{item}</button>
                    ))}
                  </div>
                  <input
                    onChange={e => {
                      this.setState({ newSectionName: e.target.value })
                    }}
                    className="add-section"
                    type="text"
                    value={newSectionName}
                  />
                  <button onClick={this.addSection.bind(this)} className="round-btn thin">
                    <span>{addSection}</span>
                  </button>
                  <span dangerouslySetInnerHTML={{ __html: responseMessage }}/>
                  <button onClick={this.save.bind(this)} className="round-btn save">
                    <span>{save}</span>
                  </button>
                </div>
                <div className="texts">
                  {sections.map((section, index) => (
                    <div key={index}
                         className={`section ${!expandedSections.includes(section) ? ' hidden' : ''}`}>
                                            <span onClick={() => {
                                              this.setState({
                                                expandedSections: expandedSections.includes(section) ?
                                                  removeElement(expandedSections, section) :
                                                  [ ...expandedSections, section ]
                                              })
                                            }}
                                                  dangerouslySetInnerHTML={{ __html: expandedSections.includes(section) ? '&ndash;' : '+' }}
                                                  className={`toggler${expandedSections.includes(section) ? ' minus' : ''}`}
                                            />
                      <div className="section-menu">
                                                <span
                                                  onInput={e => {
                                                    const { innerText } = e.target
                                                    this.changeSection.bind(this)(section, innerText)
                                                  }}
                                                  contentEditable={true}
                                                  spellCheck={false}
                                                  suppressContentEditableWarning={true}
                                                  className="section-name">
                                                    {section}
                                                </span>
                      </div>
                      {Object.keys(localTexts[section]).map((variable, index) => (
                        <div key={index} className="row">
                          <div
                            onInput={e => {
                              const { innerText } = e.target
                              this.changeVariable.bind(this)(section, variable, innerText)
                            }}
                            contentEditable={true}
                            suppressContentEditableWarning={true}
                            spellCheck={false}
                            className="variable">
                            {variable}
                          </div>
                          <textarea
                            onChange={e => {
                              this.changeText.bind(this)(section, variable, e.target.value)
                            }}
                            value={localTexts[section][variable]}
                            spellCheck={false}
                            className="value"
                          />
                        </div>
                      ))}
                      <button onClick={() => {
                        this.addVariable.bind(this)(section)
                      }} className="round-btn thin add-var">
                        <span>{addVariable}</span>
                      </button>
                    </div>
                  ))}
                </div>
              </> : <Loading/>
            }
          </div>
        </div>
        <FooterUpper texts={pageTexts}/>
      </>
    )
  }
}
