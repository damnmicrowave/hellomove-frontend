import React from 'react'
import { FooterUpper } from '../../includes'
import './styles.scss'

export class TermsOfService extends React.Component {
  render() {
    const { texts } = this.props
    const {
      termsOfServiceTitle,
      termsOfServiceInfo
    } = texts
    return (
      <>
        <div className="wrapper-policy">
          <div className="container">
            <h3>{termsOfServiceTitle}</h3>
            <p dangerouslySetInnerHTML={{ __html: termsOfServiceInfo }}/>
          </div>
        </div>
        <FooterUpper texts={texts}/>
      </>
    )
  }
}