import React from 'react'
import { FooterUpper } from '../../includes'
import './styles.scss'

export class RefundPolicy extends React.Component {
  render() {
    const { texts } = this.props
    const {
      refundPolicyTitle,
      refundPolicyInfo
    } = texts
    return (
      <>
        <div className="wrapper-policy">
          <div className="container">
            <h3>{refundPolicyTitle}</h3>
            <p dangerouslySetInnerHTML={{ __html: refundPolicyInfo }}/>
          </div>
        </div>
        <FooterUpper texts={texts}/>
      </>
    )
  }
}