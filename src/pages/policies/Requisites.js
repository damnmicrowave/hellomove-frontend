import React from 'react'
import { FooterUpper } from '../../includes'
import './styles.scss'

export class Requisites extends React.Component {
  render() {
    const { texts } = this.props
    const { requisitesTitle } = texts
    return (
      <>
        <div className="wrapper-policy">
          <div className="container">
            <h3>{requisitesTitle}</h3>
            <p>
              <b>IBAN:</b> LT663110009547789795
            </p>
            <p>
              <b>SWIFT/BIC:</b> MNNELT21XXX
            </p>
            <p>
              <b>FIC:</b> 31100
            </p>
            <p>
              <b>Account Holder:</b> Hellomove B.V.
            </p>
            <p>
              <b>Account Holder's address:</b> Middelzand 3445, Julianadorp, 1788ER
            </p>
            <p>
              <b>Bank issuer:</b> UAB "Maneuver LT"
            </p>
            <p>
              <b>Bank issuer's address:</b> Žalgirio g. 88, LT-09303, Vilnius
            </p>
          </div>
        </div>
        <FooterUpper texts={texts}/>
      </>
    )
  }
}