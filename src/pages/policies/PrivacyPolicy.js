import React from 'react'
import { FooterUpper } from '../../includes'
import './styles.scss'

export class PrivacyPolicy extends React.Component {
  render() {
    const { texts } = this.props
    const {
      privacyPolicyTitle,
      privacyPolicyInfo
    } = texts
    return (
      <>
        <div className="wrapper-policy">
          <div className="container">
            <h3>{privacyPolicyTitle}</h3>
            <p dangerouslySetInnerHTML={{ __html: privacyPolicyInfo }}/>
          </div>
        </div>
        <FooterUpper texts={texts}/>
      </>
    )
  }
}