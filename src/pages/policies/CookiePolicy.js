import React from 'react'
import { FooterUpper } from '../../includes'
import './styles.scss'

export class CookiePolicy extends React.Component {
  render() {
    const { texts } = this.props
    const {
      cookiePolicyTitle,
      cookiePolicyInfo
    } = texts
    return (
      <>
        <div className="wrapper-policy">
          <div className="container">
            <h3>{cookiePolicyTitle}</h3>
            <p dangerouslySetInnerHTML={{ __html: cookiePolicyInfo }}/>
          </div>
        </div>
        <FooterUpper texts={texts}/>
      </>
    )
  }
}