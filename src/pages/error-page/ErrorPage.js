import React from 'react'
import './styles.scss'

export const ErrorPage = props => {
  const {
    _title404,
    _message404
  } = props.texts
  return (
    <div className="wrapper-errorpage">
      <div className="container">
        <div className="centerer">
          <h4 data-text="404">{_title404}</h4>
          <div className="desc">
            {_message404}
          </div>
        </div>
      </div>
    </div>
  )
}
