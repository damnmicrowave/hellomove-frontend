import React from 'react'
import { Link } from 'react-router-dom'
import catPNG from '../media/cat.png'

export const PaymentPage = ({ texts, programs }) => (
  <>
    <div className="row">
      <h3>{texts.title}</h3>
      <img src={catPNG} alt="cat"/>
    </div>
    <div className="divider"/>
    <div className="buttons">
      {/*<Link to={{*/}
      {/*    pathname: "/payment/instruction/",*/}
      {/*    state: {programs: programs}*/}
      {/*}} className="round-btn">*/}
      {/*    <span>{texts.buyInstructionButtoQn}</span>*/}
      {/*</Link>*/}
      <Link to={{
        pathname: '/payment/consult/',
        state: { programs: programs }
      }} className="round-btn">
        <span>{texts.buyConsultButton}</span>
      </Link>
      <Link to={{
        pathname: '/payment/fix_price/',
        state: { programs: programs }
      }} className="round-btn">
        <span>{texts.fixCost}</span>
      </Link>
    </div>
  </>
)