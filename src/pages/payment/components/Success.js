import React from 'react'
import { Link } from 'react-router-dom'

export const Success = ({ texts }) => (
  <div className="success">
    <h3>{texts.successTitle}</h3>
    <div className="desc">
      {texts.successDesc}
    </div>
    <Link className="round-btn" to="/payment/consult/">
      <span>{texts.buyConsultButton}</span>
    </Link>
  </div>
)

export const SuccessConsult = ({ texts }) => (
  <div className="success">
    <h3>{texts.successTitle}</h3>
    <div className="desc">
      {texts.consultDetails}
    </div>
    <Link className="round-btn" to="/payment/instruction/">
      <span>{texts.buyInstructionButton}</span>
    </Link>
  </div>
)

export const SuccessPrice = ({ texts }) => (
  <div className="success">
    <h3>{texts.successTitle}</h3>
    <div className="desc">
      {texts.consultDetails}
    </div>
    <Link className="round-btn" to="/payment/consult/">
      <span>{texts.buyConsultButton}</span>
    </Link>
  </div>
)

export const Fail = ({ texts }) => (
  <div className="success">
    <h3>{texts.failTitle}</h3>
    <div className="desc">
      {texts.paymentFailed}
    </div>
    <Link className="round-btn" to="/payment/">
      <span>{texts.tryAgain}</span>
    </Link>
  </div>
)