import React from 'react'
import InputMask from 'react-input-mask'
import DatePicker from 'react-date-picker'
import { Loading } from '../../../includes'
import { loadStripe } from '@stripe/stripe-js'
import { apiRequest } from '../../../utils'

export class Consultation extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      date: new Date(),
      name: '',
      email: '',
      phone: '',
      loading: false
    }
  }

  componentDidMount() {
    this.stripePromise = loadStripe('pk_live_HIZ16LnT5fYmVsJcafgEewk6008RfwT4gq')
  }

  async proceedToPayment() {
    this.setState({ loading: true })
    const {
      name,
      email,
      phone,
      date
    } = await this.state
    apiRequest(async ({ sessionId }) => {
      const stripe = await this.stripePromise
      stripe.redirectToCheckout({
        sessionId
      }).then(err => alert(err.message))
    }, 'order_consult', { method: 'POST', data: { name, phone, email, date: date.toDateString() } })
  }

  validate() {
    const {
      date,
      name,
      email,
      phone
    } = this.state
    const regex = /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/
    return regex.test(email.trim()) && phone.length > 8 && name
  }

  render() {
    const {
      email,
      phone,
      name,
      loading
    } = this.state
    const {
      consultTitle,
      consultButton,
      currency,
      _phone,
      _name
    } = this.props.texts
    return (
      <div className="consult">
        <h3>{consultTitle}</h3>
        <DatePicker
          format="dd.MM.yyyy"
          onChange={date => {
            this.setState({ date: date })
          }}
          value={this.state.date}
        />
        <input
          className="input"
          type="text"
          value={name}
          onChange={e => {
            this.setState({ name: e.target.value })
          }}
          placeholder={_name}
        />
        <InputMask mask="+7 (999) 999-99-99" maskChar="" {...{
          value: phone,
          placeholder: _phone,
          className: 'input',
          onChange: e => {
            this.setState({ phone: e.target.value })
          }
        }}/>
        <input
          className="input"
          type="text"
          value={email}
          onChange={e => {
            this.setState({ email: e.target.value })
          }}
          placeholder="e-mail"
        />
        <button onClick={this.proceedToPayment.bind(this)}
                className={`round-btn${!loading && this.validate.bind(this)() ? '' : ' disabled'}`}>
                    <span>
                        {loading ?
                          <Loading color="#4a018a"/> :
                          <>{consultButton} - 50 {currency}</>
                        }
                    </span>
        </button>
      </div>
    )
  }
}