import React from 'react'
import { loadStripe } from '@stripe/stripe-js'
import { apiRequest } from '../../../utils'
import { CheckBox, Loading } from '../../../includes'
import Select from 'react-select'
import InputMask from 'react-input-mask'

export class Instruction extends React.Component {
  constructor(props) {

    super(props)

    this.state = {
      cost: 0,
      selectedProgram: '',
      programId: 0,
      email: '',
      phone: '',
      consultation: false,
      loading: false
    }
  }

  componentDidMount() {
    this.stripePromise = loadStripe('pk_live_HIZ16LnT5fYmVsJcafgEewk6008RfwT4gq')
  }

  async proceedToPayment() {
    this.setState({ loading: true })
    const {
      programId,
      email,
      phone,
      consultation
    } = await this.state
    apiRequest(async ({ sessionId }) => {
      const stripe = await this.stripePromise
      stripe.redirectToCheckout({
        sessionId
      }).then(err => alert(err.message))
    }, 'pay', { method: 'POST', data: { programId: programId, consultation, email, phone } })
  }

  handleChange({ value, label }) {
    this.setState({ programId: value, cost: this.props.programs[value].cost, selectedProgram: { value, label } })
  };

  validate() {
    const regex = /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/
    return regex.test(this.state.email.trim()) && this.state.phone.length > 17
  }

  render() {
    const {
      cost,
      email,
      phone,
      selectedProgram,
      consultation,
      loading
    } = this.state
    const {
      programs,
      texts
    } = this.props
    const {
      instructionTitle,
      instructionButton,
      instructionInputPlaceholder,
      currency,
      _phone
    } = texts
    return (
      <div className="instruction">
        <h3>{instructionTitle}</h3>
        <Select
          className="select"
          onChange={this.handleChange.bind(this)}
          value={selectedProgram}
          options={Object.keys(programs).map(k => ({ value: k, label: programs[k].name }))}
        />
        <InputMask mask="+7 (999) 999-99-99" maskChar="" {...{
          value: phone,
          placeholder: _phone,
          className: 'input',
          onChange: e => {
            this.setState({ phone: e.target.value })
          }
        }}/>
        <input
          type="text"
          value={email}
          onChange={e => {
            this.setState({ email: e.target.value })
          }}
          placeholder={instructionInputPlaceholder}
        />
        {selectedProgram &&
        <label className="consult-checkbox">
          <CheckBox checked={consultation} toggle={() => {
            this.setState({
              consultation: !consultation,
              cost: consultation ? cost - 50 : cost + 50
            })
          }}/>
          <span>Купить консультацию дополнительно</span>
        </label>}
        <button onClick={this.proceedToPayment.bind(this)}
                className={`round-btn${cost && !loading && this.validate.bind(this)() ? '' : ' disabled'}`}>
                    <span>
                        {loading ?
                          <Loading color="#4a018a"/> :
                          <>{instructionButton}{Boolean(cost) && <> - {cost} {currency}</>}</>
                        }
                    </span>
        </button>
      </div>
    )
  }
}
