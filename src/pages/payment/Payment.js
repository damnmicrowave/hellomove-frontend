import React from 'react'
import { apiRequest } from '../../utils'
import { FooterUpper, Loading } from './../../includes'
import { Route, Switch } from 'react-router'
import {
  PaymentPage,
  Consultation,
  Instruction,
  Success,
  SuccessConsult,
  SuccessPrice,
  Fail,
  FixPrice
} from './components'
import './styles.scss'


export class Payment extends React.Component {
  constructor(props) {
    super(props)
    const { state } = props.location
    this.state = {
      loaded: Boolean(state),
      programs: state ? state.programs : {}
    }
  }

  componentDidMount() {
    if ( !this.state.loaded ) {
      apiRequest(response => {
        this.setState({
          loaded: true,
          programs: response
        })
      }, `get_instructions/${this.props.lang}`)
    }
  }

  render() {
    const { texts } = this.props
    const {
      programs,
      loaded
    } = this.state
    return (
      <>
        <div className="wrapper-payment">
          {loaded ? <div className="container">
            <Switch>
              <Route exact path="/payment/success/" render={
                () => <Success {...this.props}/>
              }/>
              <Route exact path="/payment/success_price/" render={
                () => <SuccessPrice {...this.props}/>
              }/>
              <Route exact path="/payment/fail/" render={
                () => <Fail {...this.props}/>
              }/>
              <Route exact path="/payment/success_consult/" render={
                () => <SuccessConsult {...this.props}/>
              }/>
              <Route exact path="/payment/instruction/" render={
                () => <Instruction programs={programs} {...this.props}/>
              }/>
              <Route exact path="/payment/fix_price/" render={
                () => <FixPrice programs={programs} {...this.props}/>
              }/>
              <Route exact path="/payment/consult/" render={
                () => <Consultation {...this.props}/>
              }/>
              <Route exact path="/payment/" render={
                props => <PaymentPage programs={programs} {...props} {...this.props}/>
              }/>
            </Switch>
          </div> : <Loading/>}
        </div>
        <FooterUpper texts={texts}/>
      </>
    )
  }
}
