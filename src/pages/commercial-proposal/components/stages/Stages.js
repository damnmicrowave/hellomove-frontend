import React from 'react'
import './styles.scss'


export class Stages extends React.Component {
  constructor(props) {
    super(props)

    this.stagesContainer = React.createRef()
  }

  componentDidMount() {
    this.props.onMount(this.stagesContainer.current.offsetTop)
  }

  render() {
    const {
      stages_timestamps,
      stages_names,
      stages_costs
    } = this.props.proposal
    const stages = stages_timestamps.split('; ').map((item, index) => (
      {
        time: item,
        name: stages_names.split('; ')[index],
        cost: stages_costs.split('; ')[index]
      }
    ))
    const {
      stagesTitle,
      stagesDesc
    } = this.props.texts
    return (
      <div ref={this.stagesContainer} className="stages">
        <div className="container">
          <h4>{stagesTitle}</h4>
          <div className="desc">
            {stagesDesc}
          </div>
          <div className="list">
            {stages.map(({ time, name, cost }, index) => (
              <React.Fragment key={index}>
                <div className="item">
                  <div className="info">
                    <div className="time">{time}</div>
                    <div className="name">{name}</div>
                  </div>
                  <div className="cost">{cost}</div>
                </div>
                {index + 1 !== stages.length && <div className="divider"/>}
              </React.Fragment>
            ))}
          </div>
        </div>
      </div>
    )
  }
}