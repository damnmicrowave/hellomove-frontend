import React from 'react'
import './styles.scss'


export class Possibilities extends React.Component {
  render() {
    const {
      texts,
      proposal,
      togglePopup,
      offset
    } = this.props
    const {
      item,
      cost_first,
      cost_second
    } = proposal
    const {
      possibilitiesTitle,
      possibilitiesDesc,
      leftInfo,
      leftButton,
      rightInfo,
      rightButton
    } = texts
    return (
      <div className="possibilities">
        <div className="container">
          <h4>{possibilitiesTitle.replace('{{item}}', item)}</h4>
          <div className="desc">
            {possibilitiesDesc}
          </div>
          <div className="group">
            <div className="item">
              <div className="centerer">
                <div className="cost">{cost_first}</div>
                <div className="description">{leftInfo}</div>
                <div onClick={togglePopup} className="round-btn">
                  <span>{leftButton}</span>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="centerer">
                <div className="cost">{cost_second}</div>
                <div className="description">{rightInfo}</div>
                <div onClick={() => {
                  window.scrollTo(0, offset)
                }} className="round-btn">
                  <span>{rightButton}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}