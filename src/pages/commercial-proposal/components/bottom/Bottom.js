import React from 'react'
import './styles.scss'

export class Bottom extends React.Component {
  render() {
    const {
      bottomText,
      bottomButton
    } = this.props.texts
    return (
      <div className="bottom">
        <div className="container">
          <div className="desc">{bottomText}</div>
          <button onClick={this.props.togglePopup} className="round-btn filled">
            <span>{bottomButton}</span>
          </button>
        </div>
      </div>
    )
  }
}