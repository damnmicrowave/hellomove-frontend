import React from 'react'
import './styles.scss'

export class TopSection extends React.Component {
  render() {
    return (
      <div className="top-section">
        <div className="purpler"/>
        <div className="container">
          <h4>{this.props.texts.title}</h4>
          <div className="name">{this.props.proposal.name}</div>
        </div>
      </div>
    )
  }
}