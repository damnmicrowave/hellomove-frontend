import React from 'react'
import './styles.scss'
import globeSVG from './media/globe.svg'
import halfSVG from './media/half.svg'
import radarSVG from './media/radar.svg'
import pcPNG from './media/pc.png'


export class Reasons extends React.Component {
  render() {
    const {
      advantagesTitle,
      advantagesHeadings,
      advantagesDescriptions
    } = this.props.texts
    const pics = [ globeSVG, halfSVG, radarSVG ]
    return (
      <div className="reasons">
        <div className="container">
          <h4>{advantagesTitle}</h4>
          <div className="group">
            <div className="left">
              {pics.map((pic, index) => (
                <div key={index} className="row">
                  <img src={pic} alt="icon"/>
                  <div className="desc">
                    <div className="heading">{advantagesHeadings.split('; ')[index]}</div>
                    <div className="text">{advantagesDescriptions.split('; ')[index]}</div>
                  </div>
                </div>
              ))}
            </div>
            <div className="right">
              <img src={pcPNG} alt="pc"/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}