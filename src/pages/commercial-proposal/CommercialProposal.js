import React from 'react'
import { FooterUpper } from '../../includes/footer'
import { apiRequest } from '../../utils'
import { ContactForm, Loading } from '../../includes'
import {
  TopSection,
  Reasons,
  Possibilities,
  Stages,
  Bottom
} from './components'

export class CommercialProposal extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      showPopup: false,
      loaded: false,
      agency: {},
      stagesOffset: 0
    }
  }

  componentDidMount() {
    const { loaded } = this.state
    if ( !loaded ) {
      apiRequest(({ response }) => {
        this.setState({
          proposal: response,
          loaded: true
        })
      }, `get_proposal/${this.props.match.params.id}`)
    }
  }

  togglePopup() {
    const { showPopup } = this.state
    this.setState({ showPopup: !showPopup })
  }

  render() {
    const {
      loaded,
      proposal,
      showPopup,
      stagesOffset
    } = this.state
    const { texts } = this.props
    return (
      <>
        {loaded ? <div className="wrapper-commercial-proposal">
          {showPopup &&
          <ContactForm toggle={this.togglePopup.bind(this)} texts={texts}/>
          }
          <TopSection proposal={proposal} texts={texts}/>
          <Reasons texts={texts}/>
          <Possibilities togglePopup={this.togglePopup.bind(this)} offset={stagesOffset} proposal={proposal}
                         texts={texts}/>
          <Stages onMount={offset => {
            this.setState({ stagesOffset: offset })
          }} proposal={proposal} texts={texts}/>
          <Bottom togglePopup={this.togglePopup.bind(this)} texts={texts}/>
        </div> : <Loading/>}
        <FooterUpper texts={texts}/>
      </>
    )
  }
}