import React from 'react'
import InputMask from 'react-input-mask'
import ym from 'react-yandex-metrika'
import { apiRequest, removeElement } from '../../utils'
import { CheckBox, FooterUpper, Loading, ThankYou } from '../../includes'
import { ProgramSelectionList } from './program-selection/ProgramSelectionList'
import './styles.scss'
import termsPDF from './media/terms.pdf'


export class AddAgency extends React.Component {
  constructor(props) {
    super(props)

    this.initialState = JSON.stringify({
      thanksPopup: false,
      loaded: false,
      loading: false,
      imageSet: false,

      agencyName: '',
      officialName: '',
      inn: '',
      email: '',
      phone: '',
      website: '',

      newAdvantage: '',
      advantages: [],
      advantagesRemovable: [],

      timeExisting: '',

      allPrograms: [],
      programs: [],
      selectedPrograms: [],
      programsListExpanded: false,

      agreement: false
    })
    this.state = JSON.parse(this.initialState)
    this.reader = new FileReader()
    this.pic = React.createRef()
    this.picInput = React.createRef()
  }

  componentDidMount() {
    this.reader.onload = () => {
      this.pic.current.src = this.reader.result
    }
    apiRequest(({ programs }) => {
      this.setState({
        loaded: true,
        allPrograms: programs,
        programs: programs
      })
    }, `get_all_programs/${this.props.lang}`)
  }

  createAgency() {
    const {
      image,
      allPrograms,
      programs
    } = this.state
    const formData = new FormData()
    formData.append('data', JSON.stringify(this.state))
    if ( image ) {
      formData.append('photo', image, image.name)
    }
    this.setState({
      ...JSON.parse(this.initialState),
      allPrograms: allPrograms,
      loaded: true,
      loading: true
    })
    apiRequest(() => {
      ym('reachGoal', 'agencyApplication')
      this.setState({
        loading: false,
        thanksPopup: true
      })
      setTimeout(() => {
        this.setState({ thanksPopup: false })
      }, 5000)
    }, 'add_agency', { data: false, method: 'POST', formData: formData })
  }

  validate() {
    const {
      agencyName,
      officialName,
      inn,
      email,
      phone,
      website,
      timeExisting,
      advantages,
      agreement
    } = this.state
    return agencyName &&
      officialName &&
      inn &&
      /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/.test(email.trim()) &&
      advantages.length &&
      timeExisting &&
      agreement &&
      website &&
      phone.length > 9
  }

  addAdvantage() {
    const {
      advantages,
      newAdvantage
    } = this.state
    if ( newAdvantage && !advantages.includes(newAdvantage) ) {
      this.setState({
        advantages: [ ...advantages, newAdvantage ],
        newAdvantage: ''
      })
    }
  }

  toggleCloseIcon(advantage) {
    const { advantagesRemovable } = this.state
    this.setState({
      advantagesRemovable: advantagesRemovable.includes(advantage) ?
        removeElement(advantagesRemovable, advantage) :
        [ ...advantagesRemovable, advantage ]
    })
  }

  suggestProgram(programName) {
    const { programs, selectedPrograms } = this.state
    this.setState({
      selectedPrograms: [ ...selectedPrograms, programName ],
      programs: [ programName, ...programs ]
    })
  }

  selectProgram(programName) {
    const { selectedPrograms } = this.state
    this.setState({
      selectedPrograms: !selectedPrograms.includes(programName) ?
        [ ...selectedPrograms, programName ] :
        [ ...removeElement(selectedPrograms, programName) ]
    })
  }

  setProgramsList(expanded) {
    this.setState({
      programsListExpanded: expanded
    })
  }

  render() {
    const { texts } = this.props
    const {
      title,
      companyInfo,
      avatar,
      agencyInfoName,
      agencyInfoOfficialName,
      agencyInfoTIN,
      agencyInfoEmail,
      agencyInfoPhone,
      agencyInfoWebsite,
      agencyAdvantages,
      agencyTimeExisting,
      programsYouUse,
      advantagesPlaceholder,
      timePlaceholder,
      termsTitle,
      mainTermsStatements,
      getFamiliarWithTerms,
      acceptTerms,
      sendButton,
      applicationSentTitle,
      applicationSentMessage
    } = texts
    const {
      thanksPopup,
      loaded,
      loading,
      imageSet,
      agencyName,
      officialName,
      inn,
      email,
      phone,
      website,
      newAdvantage,
      advantages,
      advantagesRemovable,
      timeExisting,
      programs,
      selectedPrograms,
      programsListExpanded,
      agreement
    } = this.state
    const inputs = [
      { placeholder: agencyInfoName, value: agencyName, state: 'agencyName' },
      { placeholder: agencyInfoOfficialName, value: officialName, state: 'officialName' },
      { placeholder: agencyInfoTIN, value: inn, state: 'inn' },
      { placeholder: agencyInfoEmail, value: email, state: 'email' },
      { placeholder: agencyInfoPhone, value: phone, phone: true },
      { placeholder: agencyInfoWebsite, value: website, state: 'website' }
    ]
    return (
      <>
        {thanksPopup && <ThankYou title={applicationSentTitle} message={applicationSentMessage}/>}
        <div className="wrapper-add-agency">
          {loaded ?
            <div className="container">
              <h3>{title}</h3>
              <div className="info-block">
                <div className="left">
                  <label className="avatar-box">
                    <input ref={this.picInput} type="file" accept="image/*" onChange={e => {
                      this.reader.readAsDataURL(e.target.files[0])
                      this.setState({ imageSet: true })
                    }}/>
                    {imageSet ?
                      <img ref={this.pic} alt="agency avatar"/> :
                      <>
                        <svg width="35" height="35" viewBox="0 0 35 35" fill="#999999"
                             xmlns="http://www.w3.org/2000/svg">
                          <path
                            d="M29.1665 7.29169H25.3953L21.4475 3.34398C21.3124 3.20829 21.1517 3.10069 20.9747 3.02735C20.7978 2.95402 20.608 2.91641 20.4165 2.91669H14.5832C14.3916 2.91641 14.2019 2.95402 14.025 3.02735C13.848 3.10069 13.6873 3.20829 13.5521 3.34398L9.60442 7.29169H5.83317C4.22463 7.29169 2.9165 8.59981 2.9165 10.2084V26.25C2.9165 27.8586 4.22463 29.1667 5.83317 29.1667H29.1665C30.775 29.1667 32.0832 27.8586 32.0832 26.25V10.2084C32.0832 8.59981 30.775 7.29169 29.1665 7.29169ZM17.4998 24.7917C13.5478 24.7917 10.2082 21.4521 10.2082 17.5C10.2082 13.5465 13.5478 10.2084 17.4998 10.2084C21.4519 10.2084 24.7915 13.5465 24.7915 17.5C24.7915 21.4521 21.4519 24.7917 17.4998 24.7917Z"/>
                          <path
                            d="M18.9583 13.125H16.0417V16.0417H13.125V18.9583H16.0417V21.875H18.9583V18.9583H21.875V16.0417H18.9583V13.125Z"/>
                        </svg>
                        <span>{avatar}</span>
                      </>
                    }
                  </label>
                </div>
                <div className="right col">
                  <h4>{companyInfo}</h4>
                  {inputs.map(({ placeholder, value, state, phone }, index) => (
                    !phone ?
                      <input
                        key={index}
                        type="text"
                        placeholder={placeholder}
                        value={value}
                        onChange={e => {
                          this.setState({ [state]: e.target.value })
                        }}
                      /> :
                      <InputMask key={index} mask="+99999999999999" maskChar=" " {...{
                        placeholder: placeholder,
                        value: value,
                        autoComplete: 'new-tel',
                        onChange: e => {
                          this.setState({ phone: e.target.value })
                        }
                      }}/>
                  ))}
                </div>
              </div>
              <div className="divider"/>
              <div className="info-block">
                <div className="left">
                  <h4>{agencyAdvantages}</h4>
                </div>
                <div className="right col">
                  {advantages.map((item, index) => (
                    <div key={index}
                         className={`advantage-box${advantagesRemovable.includes(item) ? ' removable' : ''}`}>
                      <div onTouchEnd={() => {
                        this.toggleCloseIcon.bind(this)(item)
                      }} className="advantage">
                        <span>{item}</span>
                      </div>
                      <div className="minus-box">
                        <div onClick={() => {
                          advantages.splice(index, 1)
                          this.setState({
                            advantages: [ ...advantages ]
                          })
                        }} className="minus-icon">
                          <div className="minus"/>
                        </div>
                      </div>
                    </div>
                  ))}
                  <div className="row">
                    <input
                      type="text"
                      placeholder={advantagesPlaceholder}
                      value={newAdvantage}
                      onChange={e => {
                        this.setState({ newAdvantage: e.target.value })
                      }}
                      onKeyUp={e => {
                        if ( e.key === 'Enter' ) {
                          this.addAdvantage.bind(this)()
                        }
                      }}
                    />
                    <svg onClick={this.addAdvantage.bind(this)} xmlns="http://www.w3.org/2000/svg"
                         xmlnsXlink="http://www.w3.org/1999/xlink" width="48px" height="48px"
                         viewBox="0 0 48 48" version="1.1" fill="#cecece">
                      <g id="surface12076162">
                        <path
                          d="M 24 4 C 12.953125 4 4 12.953125 4 24 C 4 35.046875 12.953125 44 24 44 C 35.046875 44 44 35.046875 44 24 C 44 12.953125 35.046875 4 24 4 Z M 32 26 L 26 26 L 26 32 C 26 33.105469 25.105469 34 24 34 C 22.894531 34 22 33.105469 22 32 L 22 26 L 16 26 C 14.894531 26 14 25.105469 14 24 C 14 22.894531 14.894531 22 16 22 L 22 22 L 22 16 C 22 14.894531 22.894531 14 24 14 C 25.105469 14 26 14.894531 26 16 L 26 22 L 32 22 C 33.105469 22 34 22.894531 34 24 C 34 25.105469 33.105469 26 32 26 Z M 32 26 "/>
                      </g>
                    </svg>
                  </div>
                </div>
              </div>
              <div className="divider"/>
              <div className="info-block">
                <div className="left">
                  <h4>{agencyTimeExisting}</h4>
                </div>
                <div className="right">
                  <input
                    style={{ width: '12rem' }}
                    type="text"
                    placeholder={timePlaceholder}
                    value={timeExisting}
                    onChange={e => {
                      this.setState({ timeExisting: e.target.value })
                    }}
                  />
                </div>
              </div>
              <div className="divider"/>
              <div className="info-block">
                <div className="left">
                  <h4>{programsYouUse}</h4>
                </div>
                <div className="right">
                  <ProgramSelectionList
                    expanded={programsListExpanded}
                    toggle={this.setProgramsList.bind(this)}
                    suggest={this.suggestProgram.bind(this)}
                    select={this.selectProgram.bind(this)}
                    programs={programs}
                    selected={selectedPrograms}
                    texts={texts}
                  />
                </div>
              </div>
              <div className="divider"/>
              <div className="terms-block">
                <h4>{termsTitle}</h4>
                <div className="terms" dangerouslySetInnerHTML={{ __html: mainTermsStatements }}/>
                <a href={termsPDF} target="_blank" rel="noopener noreferrer">{getFamiliarWithTerms}</a>
                <label className="terms-agreement">
                  <CheckBox checked={agreement} toggle={() => {
                    this.setState({ agreement: !agreement })
                  }}/>
                  {acceptTerms}
                </label>
                <button onClick={this.createAgency.bind(this)}
                        className={`round-btn${!this.validate() ? ' disabled' : ''}`}>
                  <span>{loading ? <Loading color="white"/> : sendButton}</span>
                </button>
              </div>
            </div> :
            <Loading/>
          }
        </div>
        <FooterUpper texts={texts}/>
      </>
    )
  }
}