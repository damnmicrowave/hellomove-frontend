import React from 'react'
import { CheckBox } from '../../../includes'
import { getParents } from '../../../utils'
import './styles.scss'


export class ProgramSelectionList extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      searchQuery: ''
    }

    this.programsList = React.createRef()
    this.suggestProgram = React.createRef()
  }

  componentDidMount() {
    window.onclick = e => {
      const { toggle, suggest } = this.props
      const nodeParents = getParents(e.target)
      if ( nodeParents.includes(this.suggestProgram.current) ) {
        suggest(this.state.searchQuery)
        this.setState({ searchQuery: '' })
      } else if ( !nodeParents.includes(this.programsList.current) ) {
        toggle(false)
      }
    }
  }

  render() {
    const {
      expanded,
      toggle,
      programs,
      texts,
      select,
      selected
    } = this.props
    const { searchQuery } = this.state
    const {
      searchPlaceholder,
      suggestProgram
    } = texts
    return (
      <>
        <div ref={this.programsList} className={`list-box${expanded ? ' expanded' : ''}`}>
          <div className="list-input">
            <input onFocus={() => {
              toggle(true)
            }} onChange={e => {
              const { value } = e.target
              this.setState({
                searchQuery: value
              })
            }} value={searchQuery} type="text" placeholder={searchPlaceholder}/>
          </div>
          <div className="items">
            {programs.map((name, index) => (
              name.toLowerCase().includes(searchQuery.toLowerCase()) &&
              <label onClick={e => {
                // noinspection JSUnresolvedVariable
                if ( e.target.nodeName === 'INPUT' ) select(name)
              }} key={index} className="elem">
                <CheckBox checked={selected.includes(name)}/>
                <span>{name}</span>
              </label>
            ))}
            {searchQuery &&
            <label ref={this.suggestProgram} className="elem">
              <span>{suggestProgram}: {searchQuery}</span>
            </label>
            }
          </div>
        </div>
      </>
    )
  }
}