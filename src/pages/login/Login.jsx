import React, { useState } from 'react'
import { apiRequest } from '../../utils'
import { Loading } from '../../includes'

import styles from './Login.module.scss'

export const Login = ({ texts }) => {

  const [ email, setEmail ] = useState('')
  const [ password, setPassword ] = useState('')
  const [ loading, setLoading ] = useState(false)
  const memberLogin = () => {
    setLoading(true)
    apiRequest(({ status, reason }) => {
      if ( status === 'ok' ) {
        window.location.replace('/personal_area/')
      } else {
        const reasons = {
          notFound: 'Пользователь с данным e-mail не найден :/',
          passwordIncorrect: 'Неверный пароль'
        }
        alert(reasons[reason])
        setLoading(false)
      }
    }, 'member_login', {
      method: 'POST',
      data: { email, password }
    })
  }

  const validate = () => /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/.test(email) && password.length > 7

  const { title, passwordPlaceholder, button } = texts

  return (
    <div className={styles.wrapper}>
      <div className={styles.card}>
        <h3>{title}</h3>
        <input
          value={email}
          onChange={e => setEmail(e.target.value)}
          type='text'
          placeholder='E-mail'
        />
        <input
          value={password}
          onChange={e => setPassword(e.target.value)}
          type='password'
          placeholder={passwordPlaceholder}
        />
        <button onClick={memberLogin} className={`${styles.button} ${!validate() || loading ? 'disabled' : ''}`}>
          {loading ? <Loading/> : <span>{button}</span>}
        </button>
      </div>
    </div>
  )
}
